#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 20:17:00 2022

@author: maxime
"""
import numpy as np
import utils
from scipy.constants import R 
from physics.constants import BSE
 
def S_solubility_G22(p_S2,T,fO2,X_FeO=BSE['FeO']/100):
    fS2   = p_S2*1e-5
    X_FeO*=100
    return np.exp(13.8426-26.476e3/T+0.124*X_FeO+0.5*np.log(fS2/fO2))*1e-6

def N_solubility_D22(p_N2,p,T,DeltaIW,X_SiO2=BSE['SiO2']/100,X_Al2O3=BSE['Al2O3']/100,X_TiO2=BSE['TiO2']/100):
    return (np.sqrt(p_N2*1e-9)*np.exp(5908*np.sqrt(p*1e-9)/T-1.60*DeltaIW)+p_N2*1e-9*np.exp(4.67+7.11*X_SiO2-13.06*X_Al2O3-120.67*X_TiO2))*1e-6

def psat(T, gaz):
    return gaz.TriplePointP*np.exp(-gaz.L_mol/R*(1./T-1./gaz.TriplePointT)) 

def T_dew(p,gas):
    return gas.TriplePointT/(1-R*gas.TriplePointT/gas.L_mol*np.log(p/gas.TriplePointP))

def CO2_saturation_Duncan_2017(p,T,fO2,compo):
    N_O_compo   = (2*compo['SiO2']+2*compo['TiO2']+3*compo['Al2O3']+3*compo['Cr2O3']+compo['MnO']+compo['FeO']+compo['NiO']+compo['MgO']+compo['CaO']+compo['Na2O']+compo['K2O']+5*compo['P2O5'])/100
    FW_one      = compo['average molecular mass']/N_O_compo
    log_Kstar   = 7.87+2.049*10**4/T-836*(p*1e-9)/T-4.467*10**4/T**2\
                -15.53*compo['SiO2']/100-20.61*compo['Al2O3']/100-19.74*compo['FeO']/100-11.59*compo['MgO']/100-10.31*compo['CaO']/100
    X_carbonate = 10**(log_Kstar+np.log10(fO2))/(1+10**(log_Kstar+np.log10(fO2)))
    CO2_molecular_mass = 44
    return CO2_molecular_mass/FW_one*X_carbonate/(1-(1-CO2_molecular_mass/FW_one)*X_carbonate)

def CO2_sat_Eguchi_Dasgupta_2017(p,T,fO2,compo):
    N_O_compo   = (2*compo['SiO2']+2*compo['TiO2']+3*compo['Al2O3']+3*compo['Cr2O3']+compo['MnO']+compo['FeO']+compo['NiO']+compo['MgO']+compo['CaO']+compo['Na2O']+compo['K2O']+5*compo['P2O5'])/100
    FW_one      = compo['average molecular mass']/N_O_compo
    K10         = 10**(40.07639-2.53932e-2*(T-273.15)+5.27096e-6*(T-273.15)**2+0.0267*(p*1e-9-1)/(T-273.15)) # Holloway et al., 1992
    # Values for CO2 dissolution for andesite melt
    lnK0        = -21.79      # 
    DeltaV0     = 32.91*1e-6  # [m^3/mol]
    DeltaH0     = 107e3       # [J/mol]
    p0          = 3e9         # [Pa]
    T0          = 1773.15     # [K]
    K6          = np.exp(lnK0-DeltaV0/R/T*(p-p0)-DeltaH0/R*(1/T-1/T0))
    X_CO2_molar = K6*K10*fO2
    CO2_molecular_mass = 44
    return X_CO2_molar*CO2_molecular_mass/(FW_one*(1-X_CO2_molar)+CO2_molecular_mass*X_CO2_molar)

from chemistry.thermochemistry import JANAF
def CO_sat_Yoshioka_2019(p,T,fO2,compo):
    C_atomic_mass = 12
    V_graphite  = C_atomic_mass/5.3*1e-6*2 # [m^3/mol] graphite molar volume (graphite density ~2.2 g/cm^3)
    DeltafG0_CO = JANAF['CO'].to_xarray().interp({'T(K)':utils.float2array(T)})['delta-f G'].data*1000 # [J/mol]
    K_CCO       = np.exp(V_graphite*p/R/T-DeltafG0_CO/R/T)
    fCO         = K_CCO*np.sqrt(fO2)
    return 10**(-5.20)*fCO**0.80*1e-2