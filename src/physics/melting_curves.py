#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 15:20:19 2022

@author: maxime
"""
import numpy as np
from tools import melting_curves
from datapath import datapath

#### PHYSICAL QUANTITIES

# General
heat_capacity       = 1200. 	# J/K/kg
thermal_diffusivity = 1e-6	# m^2/s
thermal_expansivity = 3.0e-5   # K^-1
gravity             = 9.8      # m/s^2
R_out               = 6400000. # m
T0                  = 274.15  # [K]
p0                  = 1e5     # [Pa]
EO_mass             = 1.4e21  # [kg]

###############################

# KLB-1 peridotite
Mars_mc_Maurice2017 = melting_curves(np.linspace(0.,24e9,100),\
                                     1400 + 149.5*np.linspace(0.,24,100) - 9.64*np.linspace(0.,24,100)**2 + 0.313*np.linspace(0.,24,100)**3 - 0.0039*np.linspace(0.,24,100)**4,\
                                     1977 + 64.1*np.linspace(0.,24,100)  - 3.92*np.linspace(0.,24,100)**2 + 0.141*np.linspace(0.,24,100)**3 - 0.0015*np.linspace(0.,24,100)**4,\
                                     5e5)


Earth_mc_Fiquet2010 = melting_curves(np.linspace(0.,135e9,100),\
                                     1362+5.58e1*np.linspace(0.,135,100)-5.58e-1*np.linspace(0.,135,100)**2+3.95e-3*np.linspace(0.,135,100)**3-1.11e-5*np.linspace(0.,135,100)**4,\
                                     1750+7.32e1*np.linspace(0.,135,100)-7.78e-1*np.linspace(0.,135,100)**2+4.88e-3*np.linspace(0.,135,100)**3-1.24e-5*np.linspace(0.,135,100)**4,\
                                     5e5)

Chondritic_mc_Andrault2011 = melting_curves(np.linspace(0.,135e9,100),\
                                            2045.*(np.linspace(0.,135e9,100)/92e9+1.)**(1./1.3),\
                                            1940.*(np.linspace(0.,135e9,100)/29e9+1.)**(1./1.9),\
                                            5e5)
    
Monteux2016_Fperid = melting_curves(np.append(np.linspace(0,20e9),np.linspace(20e9,135e9)),
                                    np.append(1661.2*(np.linspace(0,20e9)/(1.336e9)+1)**(1/7.437),2081.8*(np.linspace(20e9,135e9)/(101.69e9)+1)**(1/1.226)),
                                    np.append(1982.1*(np.linspace(0,20e9)/(6.594e9)+1)**(1/5.374),78.74*(np.linspace(20e9,135e9)/(4.054e6)+1)**(1/2.44)),
                                    4e5)
    
Monteux2016_Achond = melting_curves(np.append(np.linspace(0,20e9),np.linspace(20e9,135e9)),
                                    np.append(1661.2*(np.linspace(0,20e9)/(1.336e9)+1)**(1/7.437),2081.8*(np.linspace(20e9,135e9)/(101.69e9)+1)**(1/1.226)),
                                    np.append(1982.1*(np.linspace(0,20e9)/(6.594e9)+1)**(1/5.374),2006.8*(np.linspace(20e9,135e9)/(34.65e9)+1)**(1/1.844)),
                                    4e5)
  
Stixrude2014_SuperEarth = melting_curves(np.linspace(0,600e9),
                                        5400*(np.linspace(0,600e9)/(140e9))**0.48,
                                        5400*(np.linspace(0,600e9)/(140e9))**0.48+1,
                                        5e5)

Alfe1999_iron = melting_curves(np.genfromtxt(datapath+'/core_melting_curve.dat',skip_header=1)[:,0]*1e9,
                               np.genfromtxt(datapath+'/core_melting_curve.dat',skip_header=1)[:,1],
                               np.genfromtxt(datapath+'/core_melting_curve.dat',skip_header=1)[:,1],
                               13.8e5/55.8473*1000)

#### BOOK

mc_book               = {}
mc_book['Mars']       = Mars_mc_Maurice2017
mc_book['Earth']      = Earth_mc_Fiquet2010        
mc_book['SuperEarth'] = Stixrude2014_SuperEarth  
mc_book['iron']       = Alfe1999_iron     