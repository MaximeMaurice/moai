#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 15:16:38 2022

@author: maxime
"""

import numpy as np
import pandas as pd
import utils
from scipy.optimize import root_scalar
from scipy.integrate import quad
from scipy.constants import R
from datapath import datapath

#### PHYSICAL QUANTITIES

# General
heat_capacity       = 1200. 	# J/K/kg
thermal_diffusivity = 1e-6	# m^2/s
thermal_expansivity = 3.0e-5   # K^-1
gravity             = 9.8      # m/s^2
R_out               = 6400000. # m
T0                  = 274.15  # [K]
p0                  = 1e5     # [Pa]
EO_mass             = 1.4e21  # [kg]

###############################

#### THERMAL EXPANSIVITY

def alpha_Nikolaou2019(p,T):
    # given by eq. 3 in Nikolaou et al., 2019
    m = 0.         # mysterious parameter which is zero anyway [1]
    alpha_0 = 3e-5 # reference thermal expansivity             [1/K]
    K_0 = 200e9    # room-pressure bulk modulus                [Pa]
    K_prime = 4.   # pressure derivative of the bulk modulus   [1]
    return alpha_0 * (p * K_prime / K_0 + 1.) ** (-(m-1.+K_prime)/K_prime)

#### DENSITY

def rho_MyiazakiKorenaga2019(p,T,phase):
    # visually obtain for MgSiO3 in Figure 6.d) of Myiazaki and Korenaga 2019.
    rho_top  = {'liq':3600.,'sol':4200.} # surface density [kg/m^3]
    rho_CMB  = {'liq':5100.,'sol':5250.} # CMB density     [kg/m^3]
    p_top    = 25e9
    p_CMB    = 140e9 # CMB pressure    [Pa]
    return (p_CMB*rho_top[phase]-p_top*rho_CMB[phase])/(p_CMB-p_top)\
          +p*(rho_top[phase]-rho_CMB[phase])/(p_top-p_CMB)

def rho_PREM(p,T):
    # Defined in Monteux et al., EPSL 2016
    A         = 4.0074e11            # [Pa]
    B         = -91862.              # [Pa/m]
    C         = 0.0045483            # [Pa/m^2]
    A_prime   = A+B*R_out+C*R_out**2 # [Pa]
    B_prime   = -B-2*C*R_out        # [Pa/m]
    C_prime   = C                   # [Pa/m^2]
    rho_ref   = np.sqrt(B_prime**2-4*A_prime*C_prime)/gravity    # [kg/m^3]
    p_ref     = (B_prime**2-4*A_prime*C_prime)/(4*C_prime) # [Pa]

    return rho_ref*np.sqrt(1+p/p_ref)

EOS_coefs = pd.read_excel(datapath+'/thermodynamics_coefs.ods')

def rho_perfect_gas(p,T,sp):
    # perfect gas law for water
    return p/(R/sp.molecular_mass*T)*1000

def Birch_Murnaghan(V, mineral):
    
    return 3./2*EOS_coefs['kappa'][mineral]*((V/EOS_coefs['V0'][mineral])**(-7./3)-(V/EOS_coefs['V0'][mineral])**(-5./3))\
                                     *(1+3./4*(EOS_coefs['kappa prime'][mineral]-4)*((V/EOS_coefs['V0'][mineral])**(-2./3)-1.))

def Birch_Murnaghan_inv(p, mineral):
    
    return root_scalar(lambda p: Birch_Murnaghan(p,mineral)-p, method='Newton', x0=None)

def Birch_Murnaghan_T_inv(p, T, mineral):
    
    return Birch_Murnaghan_inv(p,mineral) * (1.+quad(lambda T_: thermal_expansivity(T_,mineral),T0,T))
    
def thermal_expansivity(T, mineral):
    
    return EOS_coefs['alpha0'][mineral]*T+EOS_coefs['alpha1'][mineral]*T**2+EOS_coefs['alpha2'][mineral]/T

def rho_BMT(p,T,mineral):
    
    return EOS_coefs['rho0'][mineral]*EOS_coefs['V0'][mineral]/Birch_Murnaghan_T_inv(p, T, mineral)

#### HEAT CAPACITY

# NATAF tables
JANAF        = {}
for sp in ['H2','H2O','CO','CO2','N2','NH3','HCN','CH4','O2','SH2','S2','SO2','HCl']:
    JANAF[sp] = pd.read_excel(datapath+'/JANAF.ods',sheet_name=sp,index_col=0,skiprows=1)

cp_NATAF = {'H2O':lambda T:utils.array2float(JANAF['H2O'].to_xarray().interp({'T(K)':utils.float2array(T)})['Cp'].data),\
            'CO' :lambda T:utils.array2float(JANAF['CO'].to_xarray().interp({'T(K)':utils.float2array(T)})['Cp'].data),\
            'CO2':lambda T:utils.array2float(JANAF['CO2'].to_xarray().interp({'T(K)':utils.float2array(T)})['Cp'].data)}

# HEAT CAPACITY
# Shomate cefficients: cp(T) = A+B*(T/1000)+C*(T/1000)**2+D*(T/1000)**3+E*(T/1000)**-2
# T_Shomate is the transition temperature when multiple parametrizations exist
# An upper bound at 1e10 is set for the loop.
T_Shomate = {}
A_Shomate = {}
B_Shomate = {}
C_Shomate = {}
D_Shomate = {}
E_Shomate = {}

# O2
A_Shomate['O2'] = 31.32234	
B_Shomate['O2'] = -20.23531	
C_Shomate['O2'] = 57.86644	
D_Shomate['O2'] = -36.50624
E_Shomate['O2'] = -0.007374	


# N2
A_Shomate['N2'] = 931.857
B_Shomate['N2'] = 293.529
C_Shomate['N2'] = -70.576
D_Shomate['N2'] = 5.688
E_Shomate['N2'] = 1.587

# CO2
A_Shomate['CO2'] = 568.122
B_Shomate['CO2'] = 1254.249
C_Shomate['CO2'] = -765.713
D_Shomate['CO2'] = 180.645
E_Shomate['CO2'] = -3.105

# NH3
A_Shomate['NH3'] = 1176.213
B_Shomate['NH3'] = 2927.717
C_Shomate['NH3'] = -904.470
D_Shomate['NH3'] = 113.009
E_Shomate['NH3'] = 11.128

## From NIST tables (numerical coefficients are in J/mol/K)

# NH3
T_Shomate['NH3'] = np.array([1400.,1e10])
A_Shomate['NH3'] = np.array([19.99563,52.02427])
B_Shomate['NH3'] = np.array([49.77119,18.48801])
C_Shomate['NH3'] = np.array([-15.37599,-3.765128])
D_Shomate['NH3'] = np.array([1.921168,0.248541])
E_Shomate['NH3'] = np.array([0.189174,-12.45799])

# N2
T_Shomate['N2'] = np.array([500.,2000.,1e10])
A_Shomate['N2'] = np.array([28.98641,19.50583,35.51872])
B_Shomate['N2'] = np.array([1.853978,19.88705,1.128728])
C_Shomate['N2'] = np.array([-9.647459,-8.598535,-0.196103])
D_Shomate['N2'] = np.array([16.63537,1.369784,0.014662])
E_Shomate['N2'] = np.array([0.000117,0.527601,-4.553760])

# O2
T_Shomate['O2'] = np.array([700.,2000.,1e10])
A_Shomate['O2'] = np.array([31.,30.03235,20.91111])
B_Shomate['O2'] = np.array([-20.23531,8.,10.72071])
C_Shomate['O2'] = np.array([57.86644,-3.988133,-2.020498])
D_Shomate['O2'] = np.array([-36.50624,0.788313,0.146449])
E_Shomate['O2'] = np.array([-0.007374,-0.741599,9.245722])

# CH4
T_Shomate['CH4'] = np.array([1300.,1e10])
A_Shomate['CH4'] = np.array([-0.703029,85.81217])
B_Shomate['CH4'] = np.array([108.4773,11.26467])
C_Shomate['CH4'] = np.array([-42.52157,-2.114146])
D_Shomate['CH4'] = np.array([5.862788,0.138190])
E_Shomate['CH4'] = np.array([0.678565,-26.42221])

# CO2
T_Shomate['CO2'] = np.array([1200.,1e10])
A_Shomate['CO2'] = np.array([24.99735,58.16639])
B_Shomate['CO2'] = np.array([55.18696,2.720074])
C_Shomate['CO2'] = np.array([-33.69137,-0.492289])
D_Shomate['CO2'] = np.array([7.948387,0.038844])
E_Shomate['CO2'] = np.array([-0.136638,-6.447293])

# CO2 liquid
T_Shomate['CO2c'] = np.array([1e10])
A_Shomate['CO2c'] = np.array([-1.34865150e+01])
B_Shomate['CO2c'] = np.array([1.08322368e+03])
C_Shomate['CO2c'] = np.array([-7.25969954e+03])
D_Shomate['CO2c'] = np.array([1.82414316e+04])
E_Shomate['CO2c'] = np.array([1.32633972e-04])

# CO
T_Shomate['CO'] = np.array([1300.,1e10])
A_Shomate['CO'] = np.array([25.56759,35.15070])
B_Shomate['CO'] = np.array([6.096130,1.300095])
C_Shomate['CO'] = np.array([4.054656,-0.205921])
D_Shomate['CO'] = np.array([-2.671301,0.013550])
E_Shomate['CO'] = np.array([0.131021,-3.282780])

# H2O
T_Shomate['H2O'] = np.array([1700.,1e10])
A_Shomate['H2O'] = np.array([30.09200,41.96426])
B_Shomate['H2O'] = np.array([6.832514,8.622053])
C_Shomate['H2O'] = np.array([6.793435,-1.499780])
D_Shomate['H2O'] = np.array([-2.534480,0.098119])
E_Shomate['H2O'] = np.array([0.082139,-11.15764])

# H2O liquid
T_Shomate['H2Ol'] = np.array([1e10])
A_Shomate['H2Ol'] = np.array([-203.6060])
B_Shomate['H2Ol'] = np.array([1523.290])
C_Shomate['H2Ol'] = np.array([-3196.413])
D_Shomate['H2Ol'] = np.array([2474.455])
E_Shomate['H2Ol'] = np.array([3.855326])

# H2
T_Shomate['H2'] = np.array([1000.,2500.,1e10])
A_Shomate['H2'] = np.array([33.066178,18.563083,43.413560])
B_Shomate['H2'] = np.array([-11.363417,12.257357,-4.293079])
C_Shomate['H2'] = np.array([11.432816,-2.859786,1.272428])
D_Shomate['H2'] = np.array([-2.772874,0.268238,-0.096876])
E_Shomate['H2'] = np.array([-0.158558,1.977990,-20.533862])

# HCN
T_Shomate['HCN'] = np.array([1200,1e10])
A_Shomate['HCN'] = np.array([32.,52.36527])
B_Shomate['HCN'] = np.array([22.59205,5.563298])
C_Shomate['HCN'] = np.array([-4.369142,-0.953224])
D_Shomate['HCN'] = np.array([-0.407697,0.056711])
E_Shomate['HCN'] = np.array([-0.282399,-7.564086])

# SH2
T_Shomate['SH2'] = np.array([1400,1e10])
A_Shomate['SH2'] = np.array([26.88412,51.22136])
B_Shomate['SH2'] = np.array([18.67809,4.147486])
C_Shomate['SH2'] = np.array([3.434203,-0.643566])
D_Shomate['SH2'] = np.array([-3.378702,0.041621])
E_Shomate['SH2'] = np.array([0.135882,-10.46385])

# S2
T_Shomate['S2'] = np.array([1e10])
A_Shomate['S2'] = np.array([33.51313])
B_Shomate['S2'] = np.array([5.065360])
C_Shomate['S2'] = np.array([-1.059670])
D_Shomate['S2'] = np.array([0.089905])
E_Shomate['S2'] = np.array([-0.211911])

# SO2
T_Shomate['SO2'] = np.array([1200,1e10])
A_Shomate['SO2'] = np.array([21.43049,57.48188])
B_Shomate['SO2'] = np.array([74.35094,1.009328])
C_Shomate['SO2'] = np.array([-57.75217,-0.076290])
D_Shomate['SO2'] = np.array([16.35534,0.005174])
E_Shomate['SO2'] = np.array([0.086731,-4.045401])

def cp_Shomate_single_val(T,**kw):
    
    for i,T_transition in enumerate(T_Shomate[kw['species']]):
        if T<T_transition:
            return A_Shomate[kw['species']][i]+B_Shomate[kw['species']][i]*(T/1000)+C_Shomate[kw['species']][i]*(T/1000)**2+D_Shomate[kw['species']][i]*(T/1000)**3+E_Shomate[kw['species']][i]*(T/1000)**-2

cp_Shomate = np.vectorize(cp_Shomate_single_val,excluded=['species'])

def cp_Wagner_Pruss_single_val(T):
    # For water
    # From Table 6.1 in Wagner and Pruss 2002
    n     = np.array([28.32044648201,
                      6.6832105268,
                      3.00632,
                      0.012436,
                      0.97315,
                      1.27950,
                      0.96956,
                      0.24873])
    gamma = np.array([1.,
                      1.,
                      1.,
                      1.28728967,
                      3.53734222,
                      7.74073708,
                      9.24437796,
                      27.5075105])
    Tc    = 647.
    tau   = T/Tc
    R     = 8.314
    return (1.+n[2]+sum((n*(gamma*tau)**2*np.exp(-gamma*tau)/(1.-np.exp(-gamma*tau))**2)[3:]))*R/(18/1000)

cp_Wagner_Pruss = np.vectorize(cp_Wagner_Pruss_single_val)

#### BOOK

eos_book = {}

eos_book['alpha'] = {}
eos_book['alpha']['cst'] = lambda p,T: np.ones_like(p)*3e-5
eos_book['alpha']['N19'] = alpha_Nikolaou2019

eos_book['rho'] = {}
eos_book['rho']['cst']  = lambda p,T: np.ones_like(p)*4000.
eos_book['rho']['cstMercury']  = lambda p,T: np.ones_like(p)*3000.
eos_book['rho']['MK19s'] = lambda p,T: rho_MyiazakiKorenaga2019(p,T,'sol')
eos_book['rho']['MK19l'] = lambda p,T: rho_MyiazakiKorenaga2019(p,T,'liq')
eos_book['rho']['PREM'] = rho_PREM

eos_book['cp'] = {}
eos_book['cp']['cst'] = lambda p,T: np.ones_like(p)*1000.