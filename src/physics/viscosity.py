#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 15:25:09 2022

@author: maxime
"""
import numpy as np
import pandas as pd
from scipy.constants import R
from datapath import datapath
from chemistry.molecules import H2O

#### PHYSICAL QUANTITIES

# General
heat_capacity       = 1200. 	# J/K/kg
thermal_diffusivity = 1e-6	# m^2/s
thermal_expansivity = 3.0e-5   # K^-1
gravity             = 9.8      # m/s^2
R_out               = 6400000. # m
T0                  = 274.15  # [K]
p0                  = 1e5     # [Pa]
EO_mass             = 1.4e21  # [kg]
BSE                 = pd.read_excel(datapath+'/basanite_composition.ods',index_col=0,sheet_name='molar').T['Giordanno 2008']
#BSE                 = pd.read_excel(datapath+'/BSE_composition.ods',index_col=0,sheet_name='molar').T['Palme and Oneill 2003']

###############################

def eta_VFT(T):
    # Vogel-Fulcher-Tammann law for liquid silicate, as defined by Nikolaou et al., 2019
    Ak = 0.00024
    Bk = 4600.
    Ck = 1000.
    return Ak*np.exp(Bk/(T-Ck))

def eta_Arrhenius(p,T):
    # Arrhenius law for diffusion creep in dry olivine
    A = 4.2e10 # [Pa s]
    E = 240e3  # [J/mol]
    V = 5e-6   # [m^3/mol]
    return A*np.exp((E-p*V)/R/T)

def eta_Giordanno2008(T,X_H2O):
    X_H2O /= H2O.molecular_mass/BSE['average molecular mass']*100
    A_G = -3.9759 # -4.55
    V = X_H2O
    TA = BSE['TiO2']+BSE['Al2O3']
    FM = BSE['FeO']+BSE['MnO']+BSE['MgO']
    NK = BSE['Na2O']+BSE['K2O']
    B_G = 159.6*(BSE['SiO2']+BSE['TiO2'])\
         -173.3*BSE['Al2O3']\
         +72.1*(BSE['FeO']+BSE['MnO']+BSE['P2O5'])\
         +75.7*BSE['MgO']\
         -39.0*BSE['CaO']\
         -84.1*(BSE['Na2O']+V)\
         +141.5*(V+np.log(1+X_H2O))\
         -2.43*(BSE['TiO2']+BSE['SiO2'])*FM\
         -0.91*(BSE['SiO2']+TA+BSE['P2O5'])*(NK+X_H2O)\
         +17.6*(BSE['Al2O3']*NK)
    C_G = 2.75*BSE['SiO2']\
         +15.7*TA\
         +8.3*FM\
         +10.2*BSE['CaO']\
         -12.3*NK\
         -99.5*np.log(1+V)\
         +0.3*(BSE['Al2O3']+FM+BSE['CaO']-BSE['P2O5'])*(NK+V)
    return 10**(A_G+B_G/(T-C_G))

#### BOOK

viscosity_book              = {}
viscosity_book['cst']       = lambda p,T: np.ones_like(p)*0.1
viscosity_book['VFT']       = eta_VFT
viscosity_book['Arrhenius'] = eta_Arrhenius
viscosity_book['Giordanno2008'] = eta_Giordanno2008