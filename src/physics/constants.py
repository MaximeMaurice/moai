#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 16:18:40 2022

@author: maxime
"""

import pandas as pd
from datapath import datapath

#### PHYSICAL QUANTITIES

# General
heat_capacity        = 1200.   # [J/K/kg] general default value
thermal_diffusivity  = 1e-6	  # [m^2/s] general default value
thermal_expansivity  = 3.0e-5  # [K^-1] general default value
T0                   = 274.15  # [K]
p0                   = 1e5     # [Pa]
EO_mass              = 1.4e21  # [kg] Mass of terrestrial water ocean

# Earth values
Earth_surface_radius = 6371e3   # [m] planetary radius
Earth_IBC_radius     = 1221.5e3 # [m] inner-core boundary
Earth_CMB_radius     = 3480e3   # [m] core-mantle boundary
Earth_gravity        = 9.798    # [m/s^2]
BSE_mass             = 4e24     # [kg]
Earth_mass           = 4./3*BSE_mass
BSE                  = pd.read_excel(datapath+'/BSE_composition.ods',index_col=0,sheet_name='molar').T['Palme and Oneill 2003'] # molar fractions