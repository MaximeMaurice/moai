#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 18:58:00 2022

@author: maxime
"""

import numpy as np
from scipy.constants import R
from physics import constants
from chemistry.molecules import molecules_book

class equilibrium:
    
    def __init__(self,expression):
        
        self.expression = expression
        self.constant   = 1.
        self.coefs      = {}
        
        stoichiometric_sgn  = -1
        stoichiometric_coef = ''
        formula   = ''
        # add EOL term at the end
        expression += ' '
        for char in expression:
            if char in ['=','+',' ']:
                self.coefs[molecules_book[formula]] = stoichiometric_sgn*stoichiometric_coef
                formula               = ''
                stoichiometric_coef             = ''
                if char == '=': stoichiometric_sgn *= -1
            elif char.isdigit() and formula == '':
                stoichiometric_coef += char
            elif char.isalpha() and formula == '':
                if stoichiometric_coef == '':
                    stoichiometric_coef = 1
                else:
                    stoichiometric_coef = int(stoichiometric_coef)
                formula = char
            elif char.isalpha() or char.isdigit():
                formula += char
            else:
                raise Exception('Error while reading equilibrium:', expression)
                
        for sp in molecules_book:
            if not molecules_book[sp] in self.coefs:
                self.coefs[molecules_book[sp]] = 0.
                                
        self.constant_fun = lambda T: np.exp(-sum([self.coefs[sp]*sp.Delta_fG0(T) for sp in self.coefs])/R/T)
        self.constant_val = self.constant_fun(300) # by default evaluated at 300 K
        
eq_H  = equilibrium('2H2+O2=2H2O')
eq_C  = equilibrium('2CO+O2=2CO2')
eq_CH = equilibrium('CO+3H2=CH4+H2O')
eq_N1 = equilibrium('N2+3H2=2NH3')
eq_N2 = equilibrium('H2O+HCN=CO+NH3')
eq_S1 = equilibrium('2SH2=S2+2H2')
eq_S2 = equilibrium('2SO2=S2+2O2')

equilibria_book = {eq_H.expression:eq_H,
                   eq_C.expression:eq_C,
                   eq_CH.expression:eq_CH,
                   eq_N1.expression:eq_N1,
                   eq_N2.expression:eq_N2,
                   eq_S1.expression:eq_S1,
                   eq_S2.expression:eq_S2}
