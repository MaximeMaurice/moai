#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 16:41:55 2022

@author: maxime
"""

from chemistry.elements import *
from scipy.constants import R
from physics import eos
from physics.phase_change.volatiles import psat,T_dew
from chemistry.thermochemistry import JANAF
import utils
import numpy as np

class molecule:
    
    def __init__(self,formula,name='NA'):
        
        self.formula = formula
        self.name    = formula
        self.coefs   = {}
        try: exec('self.gas=phys.'+formula)
        except: self.gas=None
            
        idx = 0
        # add implicit unity coefficient at the end
        if formula[-1].isalpha(): formula = formula+'1'
        while idx < len(formula):
            symb = formula[idx]
            if not symb.isupper():
                raise Exception('Error while reading formula:', formula[:-1])
            next_char = formula[idx+1] # always exists because we added the implicit last coefficient
            if next_char.islower():
                symb      = symb+next_char
                idx      += 1
                next_char = formula[idx+1]
            if next_char.isupper():
                self.coefs[elems_book[symb]] = 1
                idx                        += 1
            elif next_char.isdigit():
                self.coefs[elems_book[symb]] = int(next_char)
                idx                        += 2
            else:
                raise Exception('Error while reading formula:', formula[:-1])
        
        self.molecular_mass = 0.        
        for el in elems_book:
            if not elems_book[el] in self.coefs:
                self.coefs[elems_book[el]] = 0.
            else:
                self.molecular_mass += self.coefs[elems_book[el]]*elems_book[el].atomic_mass

        self.Delta_fG0 = lambda T: utils.array2float(JANAF[self.formula].to_xarray().interp({'T(K)':utils.float2array(T)})['delta-f G'].data*1000)
      
H2                 = molecule('H2', 'molecular hydrogen')
H2.molecular_mass  = 2. # [Da] i.e. [g/mol]
H2.alpha           = 5e-12 * H2.molecular_mass/H2.coefs[H]/H.atomic_mass # Hirschmann 2016 gives values for element solubility
H2.beta            = 1.
H2.Henry_law       = lambda pp,p: H2.alpha*pp**H2.beta
H2.dHenry_law_dpp  = lambda pp,p: H2.alpha*H2.beta*pp**(H2.beta-1)
H2.dHenry_law_dp   = lambda pp,p: 0
H2.TriplePointT    = 13.95 # [K]
H2.TriplePointP    = 7200e5 # [Pa]
H2.psat            = lambda T:psat(T,H2)
H2.T_dew           = lambda p: T_dew(p,H2)
H2.cp              = lambda T:utils.array2float(JANAF['H2'].to_xarray().interp({'T(K)':utils.float2array(T)})['Cp'].data)
H2.cp_mass         = lambda T:eos.cp_Shomate(T,species='H2')/H2.molecular_mass*1000
H2.cp_mol          = lambda T:eos.cp_Shomate(T,species='H2')
H2.cp_cond         = lambda T:0
H2.L_mass          = 454000 # [J/kg]
H2.L_mol           = H2.L_mass*H2.molecular_mass/1000
H2.part_coef       = 1e-6
H2.k0              = 5e-5 # Rosseland mean opacity
H2.p0              = 101325. # [Pa]
H2.fastchemfor     = 'H2'

O2                 = molecule('O2', 'molecular oxygen')
O2.TriplePointT    = 54.3 # [K]
O2.TriplePointP    = 150e5 # [Pa]
O2.psat            = lambda T: psat(T,O2)
O2.T_dew           = lambda p: T_dew(p,O2)
O2.molecular_mass  = 32. # [Dau] i.e. [g/mol]
O2.cp              = lambda T:utils.array2float(JANAF['O2'].to_xarray().interp({'T(K)':utils.float2array(T)})['Cp'].data)
O2.cp_mass         = lambda T:eos.cp_Shomate(T,species='O2')/O2.molecular_mass*1000
O2.cp_mol          = lambda T:eos.cp_Shomate(T,species='O2')
O2.L_mass         = 242000. # [J/kg]
O2.L_mol          = O2.L_mass*O2.molecular_mass/1000
O2.fastchemfor     = 'O2'

H2O                = molecule('H2O','water')
H2O.molecular_mass = 18. # [amu] i.e. [g/mol]
H2O.alpha          = 534e-6*(1e-5)**(1./2)
H2O.beta           = 1./2
H2O.Henry_law      = lambda pp,p: H2O.alpha*pp**H2O.beta
H2O.dHenry_law_dpp = lambda pp,p: H2O.alpha*H2O.beta*pp**(H2O.beta-1)
H2O.dHenry_law_dp  = lambda pp,p: 0
H2O.TriplePointT   = 273.15 # [K]
H2O.TriplePointP   = 611. # [Pa]
H2O.psat           = lambda T:psat(T,H2O)
H2O.T_dew          = lambda p: T_dew(p,H2O)
H2O.cp             = lambda T:utils.array2float(JANAF['H2O'].to_xarray().interp({'T(K)':utils.float2array(T)})['Cp'].data)
H2O.cp_mass        = lambda T:eos.cp_Shomate(T,species='H2O')/H2O.molecular_mass*1000
H2O.cp_mol         = lambda T:eos.cp_Shomate(T,species='H2O')
H2O.cp_cond        = lambda T:eos.cp_Shomate(T,species='H2Ol')
H2O.L_mass         = 2493000. # [J/kg]
H2O.L_mol          = H2O.L_mass*H2O.molecular_mass/1000 # [J/mol]
H2O.part_coef      = 2.5e-4
H2O.k0             = 0.01                                    # Rosseland mean opacity [m^2/kg]
H2O.p0             = 101325.
H2O.fastchemfor     = 'H2O1'

CH4                = molecule('CH4', 'methane')
CH4.molecular_mass = 16. # [amu] i.e. [g/mol]
CH4.alpha          = 0.22e-12 * CH4.molecular_mass/CH4.coefs[C]/C.atomic_mass # Hirschmann 2016 gives values for element solubility
CH4.beta           = 1.
CH4.Henry_law      = lambda pp,p: CH4.alpha*pp**CH4.beta
CH4.dHenry_law_dpp = lambda pp,p: CH4.alpha*CH4.beta**pp**(CH4.beta-1)
CH4.dHenry_law_dp  = lambda pp,p: 0
CH4.TriplePointT   = 90.67 # [K]
CH4.TriplePointP   = 11700. # [Pa]
CH4.psat           = lambda T:psat(T,CH4)
CH4.T_dew          = lambda p: T_dew(p,CH4)
#CH4.cp_mol         = lambda T:utils.array2float(JANAF['CH4'].to_xarray().interp({'T(K)':utils.float2array(T)})['Cp'].data)
CH4.cp             = lambda T:utils.array2float(JANAF['CH4'].to_xarray().interp({'T(K)':utils.float2array(T)})['Cp'].data)
CH4.cp_mass        = lambda T:eos.cp_Shomate(T,species='CH4')/CH4.molecular_mass*1000
CH4.cp_mol         = lambda T:eos.cp_Shomate(T,species='CH4')
CH4.L_mass         = 536000. # [J/kg]
CH4.L_mol          = CH4.L_mass*CH4.molecular_mass/1000 # [J/mol]
CH4.part_coef      = 2.5e-4 #1e-3
CH4.k0             = 1.2e-5   # Rosseland mean opacity [m^2/kg]
CH4.p0             = 101325.  # reference pressure  [Pa]
CH4.fastchemfor     = 'C1H4'

CO                = molecule('CO', 'carbon monoxide')
CO.molecular_mass = 28.0097 # [amu] i.e. [g/mol]
CO.alpha          = 0.55e-12 * CO.molecular_mass/CO.coefs[C]/C.atomic_mass # Hirschmann 2016 gives values for element solubility
CO.beta           = 1.
CO.Henry_law      = lambda pp,p: CO.alpha*pp**CO.beta
CO.dHenry_law_dpp = lambda pp,p: CO.alpha*CO.beta*pp**(CO.beta-1)
CO.dHenry_law_dp  = lambda pp,p: 0
CO.TriplePointT   = 67.95 # [K]
CO.TriplePointP   = 15300. # [Pa]
CO.psat           = lambda T:psat(T,CO)
CO.T_dew          = lambda p: T_dew(p,CO)
CO.cp             = lambda T:utils.array2float(JANAF['CO'].to_xarray().interp({'T(K)':utils.float2array(T)})['Cp'].data)
CO.cp_mass        = lambda T:eos.cp_Shomate(T,species='CO')/CO.molecular_mass*1000
CO.cp_mol         = lambda T:eos.cp_Shomate(T,species='CO')
CO.cp_cond        = lambda T:0
CO.L_mass         = 214285.7 # [J/kg]
CO.L_mol          = CO.L_mass*CO.molecular_mass/1000 # [J/mol]
CO.part_coef      = 2.5e-4#1e-3
CO.k0             = 1.2e-5   # Rosseland mean opacity [m^2/kg]
CO.p0             = 101325.  # reference pressure  [Pa]
CO.fastchemfor     = 'C1O1'

CO2 = molecule('CO2','carbon dioxide')
CO2.molecular_mass = 44. # [amu] i.e. [g/mol]
CO2.alpha     = 1.6e-12 * CO2.molecular_mass/CO2.coefs[C]/C.atomic_mass # Hirschmann 2016 gives values for element solubility
CO2.beta      = 1.
CO2.Henry_law      = lambda pp,p: CO2.alpha*pp**CO2.beta
CO2.dHenry_law_dpp = lambda pp,p: CO2.alpha*CO2.beta*pp**(CO2.beta-1)
CO2.dHenry_law_dp  = lambda pp,p: 0
CO2.TriplePointT = 216.54 # [K]
CO2.TriplePointP = 518500. # [Pa]
CO2.psat      = lambda T:psat(T,CO2)
CO2.T_dew     = lambda p: T_dew(p,CO2)
CO2.cp        = lambda T:utils.array2float(JANAF['CO2'].to_xarray().interp({'T(K)':utils.float2array(T)})['Cp'].data)
CO2.cp_mass   = lambda T:eos.cp_Shomate(T,species='CO2')/CO2.molecular_mass*1000
CO2.cp_mol    = lambda T:eos.cp_Shomate(T,species='CO2')
CO2.cp_cond   = lambda T:eos.cp_Shomate(T,species='CO2c')
CO2.L_mass    = 397000. # [J/kg]
CO2.L_mol     = CO2.L_mass*CO2.molecular_mass/1000 # [J/mol]
CO2.part_coef = 5e-3
CO2.k0        = 0.0001   # Rosseland mean opacity [m^2/kg]
CO2.p0        = 101325.  # reference pressure  [Pa]
CO2.fastchemfor     = 'C1O2'

N2  = molecule('N2', 'molecular nitrogen')
N2.alpha     = 1e-12 * N2.molecular_mass/N2.coefs[N]/N.atomic_mass
N2.beta      = 1.
N2.Henry_law      = lambda pp,p: N2.alpha*pp**N2.beta
N2.dHenry_law_dpp = lambda pp,p: N2.alpha*N2.beta*pp**(N2.beta-1)
N2.dHenry_law_dp  = lambda pp,p: 0
N2.TriplePointT = 63.14 # [K]
N2.TriplePointP = 12530 # [Pa]
N2.psat      = lambda T:psat(T,N2)
N2.T_dew     = lambda p: T_dew(p,N2)
N2.molecular_mass = 28. # [amu] i.e. [g/mol]
N2.cp        = lambda T:utils.array2float(JANAF['N2'].to_xarray().interp({'T(K)':utils.float2array(T)})['Cp'].data)
N2.cp_mass   = lambda T:eos.cp_Shomate(T,species='N2')/N2.molecular_mass*1000
N2.cp_mol    = lambda T:eos.cp_Shomate(T,species='N2')
N2.cp_cond   = lambda T:0
N2.L_mass    = 218000. # [J/kg]
N2.L_mol     = N2.L_mass*N2.molecular_mass/1000 # [J/mol]
N2.part_coef = 5e-3
N2.k0        = 0.   # Rosseland mean opacity [m^2/kg]
N2.p0        = 101325.  # reference pressure  [Pa]
N2.fastchemfor     = 'N2'

NH3 = molecule('NH3','ammonia')
NH3.alpha     = 5e-12 * NH3.molecular_mass/NH3.coefs[N]/N.atomic_mass # Hirschmann 2016 gives values for element solubility
NH3.beta      = 1.
NH3.Henry_law      = lambda pp,p: NH3.alpha*pp**NH3.beta
NH3.dHenry_law_dpp = lambda pp,p: NH3.alpha*NH3.beta*pp**(NH3.beta-1)
NH3.dHenry_law_dp  = lambda pp,p: 0
NH3.TriplePointT = 195.4 # [K]
NH3.TriplePointP = 6100. # [Pa]
NH3.psat      = lambda T:psat(T,NH3)
NH3.T_dew     = lambda p: T_dew(p,NH3)
NH3.molecular_mass = 17. # [amu] i.e. [g/mol]
NH3.cp        = lambda T:utils.array2float(JANAF['NH3'].to_xarray().interp({'T(K)':utils.float2array(T)})['Cp'].data)
NH3.cp_mass   = lambda T:eos.cp_Shomate(T,species='NH3')/NH3.molecular_mass*1000
NH3.cp_mol    = lambda T:eos.cp_Shomate(T,species='NH3')
NH3.cp_cond   = lambda T:0
NH3.L_mass    = 1658000. # [J/kg]
NH3.L_mol     = NH3.L_mass*NH3.molecular_mass/1000
NH3.part_coef = 5e-3
NH3.k0        = 0.   # Rosseland mean opacity [m^2/kg]
NH3.p0        = 101325.  # reference pressure  [Pa]
NH3.fastchemfor     = 'N1H3'

HCN = molecule('HCN','hydrogen cyanide')
HCN.Henry_law = lambda p: 50e-12*p # Hirschmann 2016
HCN.alpha     = 50e-12
HCN.beta      = 1.
HCN.Henry_law      = lambda pp,p: HCN.alpha*pp**HCN.beta
HCN.dHenry_law_dpp = lambda pp,p: HCN.alpha*HCN.beta*pp**(HCN.beta-1)
HCN.dHenry_law_dp  = lambda pp,p: 0
#HCN.psat      = lambda T:1e10
#HCN.psat      = lambda T:psat(T,HCN.gas)
HCN.cp        = lambda T:utils.array2float(JANAF['HCN'].to_xarray().interp({'T(K)':utils.float2array(T)})['Cp'].data)/HCN.molecular_mass*1000
HCN.cp_mass   = lambda T:eos.cp_Shomate(T,species='HCN')/HCN.molecular_mass/1000
HCN.cp_mol    = lambda T:eos.cp_Shomate(T,species='HCN')
HCN.cp_cond   = lambda T:0
HCN.molecular_mass = 27.
HCN.part_coef = 5e-3
HCN.k0        = 0.   # Rosseland mean opacity [m^2/kg]
HCN.p0        = 101325.  # reference pressure  [Pa]
HCN.fastchemfor = 'H1C1N1'

SH2 = molecule('SH2','sulfur hydroxide')
SH2.alpha     = 50e-12
SH2.beta      = 1.
SH2.Henry_law      = lambda pp,p: SH2.alpha*pp**SH2.beta
SH2.dHenry_law_dpp = lambda pp,p: SH2.alpha*SH2.beta*pp**(SH2.beta-1)
SH2.dHenry_law_dp  = lambda pp,p: 0
SH2.cp        = lambda T:utils.array2float(JANAF['SH2'].to_xarray().interp({'T(K)':utils.float2array(T)})['Cp'].data)/SH2.molecular_mass*1000
SH2.cp_mass   = lambda T:eos.cp_Shomate(T,species='SH2')/SH2.molecular_mass/1000
SH2.cp_mol    = lambda T:eos.cp_Shomate(T,species='SH2')
SH2.part_coef = 0

S2  = molecule('S2','sulfur')
S2.alpha     = 50e-12
S2.beta      = 1.
S2.Henry_law      = lambda pp,p: S2.alpha*pp**S2.beta
S2.dHenry_law_dpp = lambda pp,p: S2.alpha*S2.beta*pp**(S2.beta-1)
S2.dHenry_law_dp  = lambda pp,p: 0
S2.cp        = lambda T:utils.array2float(JANAF['S2'].to_xarray().interp({'T(K)':utils.float2array(T)})['Cp'].data)/S2.molecular_mass*1000
S2.cp_mass   = lambda T:eos.cp_Shomate(T,species='S2')/S2.molecular_mass/1000
S2.cp_mol    = lambda T:eos.cp_Shomate(T,species='SH2')
S2.part_coef = 0

SO2 = molecule('SO2','sulfur oxide')
SO2.alpha     = 50e-12
SO2.beta      = 1.
SO2.Henry_law      = lambda pp,p: SO2.alpha*pp**SO2.beta
SO2.dHenry_law_dpp = lambda pp,p: SO2.alpha*SO2.beta*pp**(SO2.beta-1)
SO2.dHenry_law_dp  = lambda pp,p: 0
SO2.cp        = lambda T:utils.array2float(JANAF['SO2'].to_xarray().interp({'T(K)':utils.float2array(T)})['Cp'].data)/SO2.molecular_mass*1000
SO2.cp_mass   = lambda T:eos.cp_Shomate(T,species='SO2')/SO2.molecular_mass/1000
SO2.cp_mol    = lambda T:eos.cp_Shomate(T,species='SO2')
SO2.part_coef = 0

HCl = molecule('HCl','hydrogen chloride')

molecules_book = {H2.formula:H2,  
                  O2.formula:O2,  
                  H2O.formula:H2O,
                  CO.formula:CO,  
                  CO2.formula:CO2,
                  N2.formula:N2,  
                  NH3.formula:NH3,
                  HCN.formula:HCN,
                  CH4.formula:CH4,
                  HCl.formula:HCl,
                  SH2.formula:SH2,
                  S2.formula:S2,
                  SO2.formula:SO2}