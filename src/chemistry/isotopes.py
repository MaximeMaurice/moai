#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 18:59:58 2022

@author: maxime
"""
import numpy as np
import utils
from chemistry.elements import element

#### CLASSES ####

class isotopes_system:
    def __init__(self, dec_cst, prt_to_dtr, dtr_to_dtr, time):
        self.decay_constant = dec_cst
        self.parent_to_stable_daughter_ratio = prt_to_dtr
        self.radiogenic_daughter_to_stable_daughter_ratio = dtr_to_dtr
        self.current_time = time
        
    def clone(self):
        return isotopes_system(self.decay_constant, self.parent_to_stable_daughter_ratio, self.radiogenic_daughter_to_stable_daughter_ratio, self.current_time)

    def getDecayConstant(self):
        return self.decay_constant

    def getTime(self):
        return self.current_time

    def getDaughterRatio(self, **kwargs):

        if 't' in kwargs:
            time = kwargs['t']
        elif  'time' in kwargs:
            time = kwargs['time']
        else:
            time = self.current_time

        return self.radiogenic_daughter_to_stable_daughter_ratio + self.parent_to_stable_daughter_ratio * (1.-np.exp(-self.decay_constant*(time-self.current_time)))

    def getParentRatio(self, **kwargs):

        if 't' in kwargs:
            time = kwargs['t']
        elif  'time' in kwargs:
            time = kwargs['time']
        else:
            time = self.current_time

        return self.parent_to_stable_daughter_ratio * np.exp(-self.decay_constant*(time-self.current_time))
 
    def epsilon(self, ref, **kwargs):

        if 't' in kwargs:
            time = kwargs['t']
        elif  'time' in kwargs:
            time = kwargs['time']
        else:
            time = self.current_time

        return (self.getDaughterRatio(time=time)/ref.getDaughterRatio(time=time)-1.)*1e4
    
    def mu(self, ref, **kwargs):

        if 't' in kwargs:
            time = kwargs['t']
        elif  'time' in kwargs:
            time = kwargs['time']
        else:
            time = self.current_time

        return (self.getDaughterRatio(time=time)/ref.getDaughterRatio(time=time)-1.)*1e6
    
    def setTime(self, new_time):
        self.current_time = new_time
    
    def setDaughterRatio(self, dtr_to_dtr):
        self.radiogenic_daughter_to_stable_daughter_ratio = dtr_to_dtr

    def setParentRatio(self, prt_to_dtr):
        self.parent_to_stable_daughter_ratio = prt_to_dtr
    
    def decay(self, time):
        self.radiogenic_daughter_to_stable_daughter_ratio = self.getDaughterRatio(time=time)
        self.parent_to_stable_daughter_ratio = self.getParentRatio(time=time)
        self.setTime(time)

class isotope(element):
    
    def __init__(self,symbol,atomic_mass,half_life):
        element.__init__(self,symbol,atomic_mass)
        self.half_life = half_life
    
#### HEAT-PRODUCING ELEMENTS

# U 235
half_life_U235 = 0.7041e9 # [year]
heat_U235      = 5.69e-4  # [W/kg]
conc_U235      = 0.22e-9  # [kg/kg], Turcotte-Schubert 2003
U235 = isotope('235U',235.0439299,half_life_U235*utils.y2s)
U235.volatile = False
U235.formula = '235U'
U235.part_coef = 0.01
U235.radio = isotopes_system(np.log(2)/(half_life_U235*utils.y2s),conc_U235,0.,4.567e9*utils.y2s)
U235.heat = heat_U235

# U 238
half_life_U238 = 4.47e9   # [year]
heat_U238      = 9.46e-5  # [W/kg]
conc_U238      = 30.8e-9  # [kg/kg], Turcotte-Schubert 2003
U238 = isotope('238U',238.05078826,half_life_U238*utils.y2s)
U238.volatile = False
U238.formula = '238U'
U238.part_coef = 0.01
U238.radio = isotopes_system(np.log(2)/(half_life_U238*utils.y2s),conc_U238,0.,4.567e9*utils.y2s)
U238.heat = heat_U238

# Th 232
half_life_Th232 = 14.5e9   # [year]
heat_Th232      = 2.54e-5  # [W/kg]
conc_Th232      = 124e-9   # [kg/kg], Turcotte-Schubert 2003
Th232 = isotope('232Th',232.038053,half_life_Th232*utils.y2s)
Th232.volatile = False
Th232.formula = '232Th'
Th232.part_coef = 0.01
Th232.radio = isotopes_system(np.log(2)/(half_life_Th232*utils.y2s),conc_Th232,0.,4.567e9*utils.y2s)
Th232.heat = heat_Th232

# K 40
half_life_K40 = 1.25e9   # [year]
heat_K40      = 2.94e-5  # [W/kg]
conc_K40      = 36.9e-9  # [kg/kg], Turcotte-Schubert 2003
K40 = isotope('40K',39.96399848,half_life_K40*utils.y2s)
K40.volatile = False
K40.formula = '40K'
K40.part_coef = 0.01
K40.radio = isotopes_system(np.log(2)/(half_life_K40*utils.y2s),conc_K40,0.,4.567e9*utils.y2s)
K40.heat = heat_K40

# Short-lived

# Al 26
half_life_Al26 = 0.717e6   # [year]
heat_Al26      = 0.455     # [W/kg]
conc_Al26      = 1.23e-6   # [kg/kg] average SSI value? (ref?)
Al26 = isotope('26Al',26,half_life_Al26*utils.y2s)
Al26.volatile = False
Al26.formula = '26Al'
Al26.part_coef = 0.01
Al26.radio = isotopes_system(np.log(2)/(half_life_Al26*utils.y2s),conc_Al26,0.,0.)
Al26.heat = heat_Al26

# Fe 60
half_life_Fe60 = 2.62e6   # [year]
heat_Fe60      = 0.0412   # [W/kg]
conc_Fe60      = 7.2e-10  # [kg/kg] average SSI value? (ref?)
Fe60 = isotope('60Fe',60,half_life_Fe60*utils.y2s)
Fe60.volatile = False
Fe60.formula = '60Fe'
Fe60.part_coef = 0.01
Fe60.radio = isotopes_system(np.log(2)/(half_life_Fe60*utils.y2s),conc_Fe60,0.,0.)
Fe60.heat = heat_Fe60

#### CHRONO-ISOTOPES ####
today = 4.567

# REFERENCE isotopes_systemS #####################################

# 176Lu-176Hf ########
CHUR_176Lu176Hf  = isotopes_system(0.01867, 0.0336, 0.282785, today) # Bouvier et al., 2008
SCHEM_176Lu176Hf = isotopes_system(0.01867, 0.0375, 0.282785, 0.)

# 147Sm-143Nd ########
CHUR_147Sm143Nd  = isotopes_system(0.00654, 0.1960, 0.512630, today) # Bouvier et al., 2008
SCHEM_147Sm143Nd = isotopes_system(0.00654, 0.2082, 0.512630, 0.)

SSI_143Nd_144Nd  = 0.506674 # Borg et al., 2019

# 146Sm-142Nd ########
# LaJolla:                142Nd/144Nd = 1.141840(6) (Boyet and Carlson 2005, supp. mat.)
# mean chondrite value: epsilon 142Nd = -0.20(14) (Boyet and Carlson 2005)
# => mean chondrite:      142Nd/144Nd = 1.141817(17)
 
#SSini_146Sm142Nd_old = isotopes_system(6.72958, 0.0084, 0., 0.)       # Boyet et al., 2010
#SSini_146Sm142Nd_new = isotopes_system(10.2,    0.0094, 0., 0.)       # Kinoshita et al., 2012
lambda_146Sm_old = 6.72958 # t_1/2 = 103 Myr, Someone et al., sometime
lambda_146Sm_new = 10.2    # t_1/2 = 65? Myr, Kinoshita et al., 2012

SSI_146Sm_144Sm = 0.00828   # pm 0.0044 (obviousy SSI value)
Std_144Sm_147Sm = 0.202419 # present day value
Std_144Sm_144Nd = Std_144Sm_147Sm * CHUR_147Sm143Nd.getParentRatio() # time-independent
Std_142Nd_144Nd = 1.1418366 # Borg et al., 2019 (supplementary material), present-day value

# Std = Standard Earth, OCh = Ordinary Chondrites, ECh = Enstatite Chondrites
Std_146Sm142Nd  = isotopes_system(lambda_146Sm_old, SSI_146Sm_144Sm * Std_144Sm_144Nd, Std_142Nd_144Nd-((SSI_146Sm_144Sm*Std_144Sm_144Nd)*(1.-np.exp(-6.72958*today))), 0.)
OCh_146Sm142Nd  = isotopes_system(lambda_146Sm_old, SSI_146Sm_144Sm * Std_144Sm_144Nd, Std_142Nd_144Nd-((SSI_146Sm_144Sm*Std_144Sm_144Nd)*(1.-np.exp(-6.72958*today)))-0.000018, 0.)
ECh_146Sm142Nd  = isotopes_system(lambda_146Sm_old, SSI_146Sm_144Sm * Std_144Sm_144Nd, Std_142Nd_144Nd-((SSI_146Sm_144Sm*Std_144Sm_144Nd)*(1.-np.exp(-6.72958*today)))-0.000010, 0.)

# 182Hf-182W #########
Mars_180Hf_184W = 4.         # Kleine and Walker 2017
SSI_182Hf_180Hf = 1.018e-4   # Kleine and Walker 2017
Std_182W_184W   = 0.864900   # Kleine and Walker 2017
Ch_180Hf_184W   = 1.35       # Kleine and Walker 2017
lambda_182Hf = np.log(2)/8.9 # 1/Myr