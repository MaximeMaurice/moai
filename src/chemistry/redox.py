import numpy as np
import pandas as pd
import utils

from scipy.integrate import quad
from scipy.optimize import root_scalar
from datapath import datapath

from chemistry.thermochemistry import JANAF

# SPECIATION MODEL AND OXYGEN FUGACITY #

GPa2Pa = 1e9 # [Pa/GPa]
bar2Pa = 1e5 # [Pa/bar]

R_gas      = 8.314 # [J/mol/K]
N_Avogadro = 6.02214076e23

eq_C_exp  = '2CO+O2=2CO2'
eq_H_exp  = '2H2+O2=2H2O'
eq_N1_exp = 'N2+3H2=2NH3'
eq_N2_exp = 'H2O+HCN=CO+NH3'
eq_CH_exp = 'CO+3H2=CH4+H2O'

#### fO2 BUFFERS

def fO2_buffer(p,T,buffer='IW',study='A19'):

    if study == 'A19':
        return fO2_buffers_A19(p,T,buffer)
    elif study == 'K19':
        return fO2_buffers_K19(p,T,buffer)
    elif study == 'B21':
        return fO2_buffers_B21(p,T,buffer)
    elif study == 'D20':
        return fO2_buffers_D20(p,T,buffer)
    
# Hirschmann GCA 2021 parametrization
def fO2_buffers_H22(p,T,buffer='IW'):
    
    p_trans = -18.64+0.04359*T-5.069e-6*T**2
    
    #fO2 = np.zeros_like(T)
    
    a =  6.844864    + 1.175691e-1*(p*1e-9) + 1.143873e-3*(p*1e-9)**2
    b =  5.791364e-4 - 2.891434e-4*(p*1e-9) - 2.737171e-7*(p*1e-9)**2
    c = -7.971469e-5 + 3.198005e-5*(p*1e-9) + 1.059554e-10*(p*1e-9)**3 + 2.014461e-7*np.sqrt((p*1e-9))
    d = -2.769002e4  + 5.285977e2*(p*1e-9)  - 2.919275*(p*1e-9)**2
    
    e =  8.463095    - 3.000307e-3*(p*1e-9) + 7.213445e-5*(p*1e-9)**2
    f =  1.148738e-3 - 9.352312e-5*(p*1e-9) + 5.161592e-7*(p*1e-9)**2
    g = -7.448624e-4 - 6.329325e-6*(p*1e-9) - 1.407339e-10*(p*1e-9)**3 + 1.830014e-4*np.sqrt((p*1e-9))
    h = -2.782082e4  + 5.285977e2*(p*1e-9)  - 8.473231e-1*(p*1e-9)**2

    if (p*1e-9)<p_trans:
        return a+b*T+c*T*np.log(T)+d/T
    else:
        return e+f*T+g*T*np.log(T)+h/T
    
    #return fO2
    
# Katyal at al., ApJ 2020 parametrization        
def fO2_buffers_K19(p,T,buffer):
    
    # table 1 in Katyal et al., 2020
    A_fO2 = {}
    B_fO2 = {}
    C_fO2 = {}

    A_fO2['QIF']   = 7.679       # [-]
    B_fO2['QIF']   = 29673.      # [K]
    C_fO2['QIF']   = 0.05/bar2Pa # [K/bar]

    A_fO2['IW']    = 6.899       # [-]
    B_fO2['IW']    = 27714.      # [K]
    C_fO2['IW']    = 0.05/bar2Pa # [K/bar]

    A_fO2['QFM']   = 8.555        # [-]
    B_fO2['QFM']   = 24014.       # [K]
    C_fO2['QFM']   = 0.092/bar2Pa # [K/bar]

    A_fO2['NiNiO'] = 8.951        # [-]
    B_fO2['NiNiO'] = 24556.       # [K]
    C_fO2['NiNiO'] = 0.046/bar2Pa # [K/bar]
    
    # eq. 5 in Katyal et al., 2020
    return A_fO2[buffer]-B_fO2[buffer]/T+C_fO2[buffer]*(p-1*bar2Pa)/T
    
    
# Armstrong et a., Science 2019 parametrization
def fO2_buffers_A19(p,T,buffer='IW'):
    
    if buffer=='IW':
        # eq. S15 in Armstrong et al., 2019
        A = 6.541
        B = 0.001
        C = -28163.6
        D = 546.32
        E = 1.1341
        F = 0.001927
        
        p = p*1e-9
        
        return A+B*p+(C+D*p+E*p**2+F*p**3)/T
    
    elif buffer=='RuRuO2':
        # eq. S3 in Armstrong et al., 2019
        # Useful only for experimental conditions
        A = 7.782
        B = -0.00996
        C = 0.001932
        D = 3.76
        E = 13763.
        F = 592.
        G = 3.955
        H = -1.05e6
        I = -4622.
        
        p = p*1e-9
        
        return A+B*p+C*p**2+D*p**3+(E+F*p+G*p**2)/T+(H+I*p)/T**2
    
# Bower et al., PSJ 2021 parametrization        
def fO2_buffers_B21(p,T,buffer='IW'):
    
    if buffer != 'IW':
        raise Exception('Only IW provided in Bower et al., 2021')
    
    A = -244118.
    B = 115.559
    C = -8.474
    
    return 2*(A+B*T+C*T*np.log(T))/np.log(10)/R_gas/T

# Deng et al.., Nat. Comm. 2020 parametrization    
def fO2_buffers_D20(p,T,buffer='IW'):
    
    if buffer != 'IW':
        raise Exception('Only IW provided in Deng et al., 2020')
    
    a0 = 6.54106
    a1 = 0.0012324
    b0 = -28163.6
    b1 = 546.32
    b2 = -1.13412
    b3 = 0.0019274
    
    p = p*1e-9
    
    return (a0+a1*p)+(b0+b1*p+b2*p**2+b3*p**3)/T # [log10(bar)]

###############################################################################
    
#### FeO/Fe2O3 equations of state
    
# Armstrong et al., Science 2019 parametrization
def V_0T_A19(T,species):
    # V_0,T = V_0,T_0 + V_0,T_T*(T-1673 [K])
    V_0T_0          = {}
    V_0T_T          = {}
    V_0T_0['FeO']   = 13650./GPa2Pa # [J/Pa]
    V_0T_0['Fe2O3'] = 21070./GPa2Pa # [J/Pa]
    V_0T_T['FeO']   = 2.92/GPa2Pa   # [J/Pa/K]
    V_0T_T['Fe2O3'] = 4.54/GPa2Pa   # [J/Pa/K]
    return V_0T_0[species]+V_0T_T[species]*(T-1673.) # [J/Pa]

def TEOS_A19(p,species):
    # Fitted values from Armstrong et al., 2019
    kappa0                = {}
    kappa0_prime          = {}
    kappa0_sec            = {}
    kappa0['FeO']         = 37.*GPa2Pa                             # [Pa]
    kappa0['Fe2O3']       = 12.6*GPa2Pa                            # [Pa]
    kappa0_prime['FeO']   = 8.                                     # [-]
    kappa0_prime['Fe2O3'] = 1.3                                    # [-]
    kappa0_sec['FeO']     = -kappa0_prime['FeO']/kappa0['FeO']     # [1/Pa]
    kappa0_sec['Fe2O3']   = -kappa0_prime['Fe2O3']/kappa0['Fe2O3'] # [1/Pa]
         
def TEOS_A19_int(p,species):
    # Fitted values from Armstrong et al., 2019
    kappa0                = {}
    kappa0_prime          = {}
    kappa0_sec            = {}
    kappa0['FeO']         = 37.*GPa2Pa                             # [Pa]
    kappa0['Fe2O3']       = 12.6*GPa2Pa                            # [Pa]
    kappa0_prime['FeO']   = 8.                                     # [-]
    kappa0_prime['Fe2O3'] = 1.3                                    # [-]
    kappa0_sec['FeO']     = -kappa0_prime['FeO']/kappa0['FeO']     # [1/Pa]
    kappa0_sec['Fe2O3']   = -kappa0_prime['Fe2O3']/kappa0['Fe2O3'] # [1/Pa]

    # Margules coefficients
    a = {} # [-]
    b = {} # [Pa]
    c = {} # [-]
    for species in ['FeO','Fe2O3']:
        a[species] = (1.+kappa0_prime[species])/(1.+kappa0_prime[species]+kappa0[species]*kappa0_sec[species])
        b[species] = kappa0_prime[species]/kappa0[species]-kappa0_sec[species]/(1.+kappa0_prime[species])
        c[species] = (1.+kappa0_prime[species]+kappa0[species]*kappa0_sec[species])\
                    /(kappa0_prime[species]**2+kappa0_prime[species]+kappa0[species]*kappa0_sec[species])
        
    # Armstrong et al., eq S10
    return 1.-a[species]+a[species]*(1-(1.+b[species]*p)**(1.-c[species]))/(b[species]*(c[species]-1.)*p) # [-]

# Deng et al., 2020
def EOS_D2020(p,T,params):
    V0      = params['V0']      # Angstroem^3
    K0      = params['K0']      # GPa
    K_prime = params['K_prime'] # non-dim
    K_sec   = params['K_sec']   # 1/GPa
    T0      = 3000.             # K
    p       = p*1e-9            # GPa
    return root_scalar(lambda V:p-(Birch_Murnaghan_4th_order(V,V0,K0,K_prime,K_sec)+B_TH(V,params)*(T-T0)),bracket=(0.3*V0,1.1*V0)).root*(1e-30*N_Avogadro) # [m^3/mol]

def B_TH(V,params):
    V0 = params['V0'] # Angstroem
    a  = params['a']
    b  = params['b']
    c  = params['c']
    return (a-b*(V/V0)+c*(V/V0)**2)/1000 # GPa/K

# Kuwahara et al., 2023

def TEOS_K23(p,species):
    # Fitted values from Armstrong et al., 2019
    kappa0                = {}
    kappa0_prime          = {}
    kappa0_sec            = {}
    kappa0['FeO']         = 37.*GPa2Pa                             # [Pa]
    kappa0['Fe2O3']       = 12.6*GPa2Pa                            # [Pa]
    kappa0_prime['FeO']   = 4.          # here are the differences # [-]
    kappa0_prime['Fe2O3'] = 1.4         # with Armstrong 2023      # [-]
    kappa0_sec['FeO']     = -kappa0_prime['FeO']/kappa0['FeO']     # [1/Pa]
    kappa0_sec['Fe2O3']   = -kappa0_prime['Fe2O3']/kappa0['Fe2O3'] # [1/Pa]

    # Margules coefficients
    a = {} # [-]
    b = {} # [Pa]
    c = {} # [-]
    for species in ['FeO','Fe2O3']:
        a[species] = (1.+kappa0_prime[species])/(1.+kappa0_prime[species]+kappa0[species]*kappa0_sec[species])
        b[species] = kappa0_prime[species]/kappa0[species]-kappa0_sec[species]/(1.+kappa0_prime[species])
        c[species] = (1.+kappa0_prime[species]+kappa0[species]*kappa0_sec[species])\
                    /(kappa0_prime[species]**2+kappa0_prime[species]+kappa0[species]*kappa0_sec[species])
    
    # Armstrong et al., eq S10
    return 1.-a[species]*(1.-(1.+b[species]*p)**(-c[species])) # [-]
         
def TEOS_K23_int(p,species):
    # Fitted values from Armstrong et al., 2019
    kappa0                = {}
    kappa0_prime          = {}
    kappa0_sec            = {}
    kappa0['FeO']         = 37.*GPa2Pa                             # [Pa]
    kappa0['Fe2O3']       = 12.6*GPa2Pa                            # [Pa]
    kappa0_prime['FeO']   = 4.          # here are the differences # [-]
    kappa0_prime['Fe2O3'] = 1.4         # with Armstrong 2023      # [-]
    kappa0_sec['FeO']     = -kappa0_prime['FeO']/kappa0['FeO']     # [1/Pa]
    kappa0_sec['Fe2O3']   = -kappa0_prime['Fe2O3']/kappa0['Fe2O3'] # [1/Pa]

    # Margules coefficients
    a = {} # [-]
    b = {} # [Pa]
    c = {} # [-]
    for species in ['FeO','Fe2O3']:
        a[species] = (1.+kappa0_prime[species])/(1.+kappa0_prime[species]+kappa0[species]*kappa0_sec[species])
        b[species] = kappa0_prime[species]/kappa0[species]-kappa0_sec[species]/(1.+kappa0_prime[species])
        c[species] = (1.+kappa0_prime[species]+kappa0[species]*kappa0_sec[species])\
                    /(kappa0_prime[species]**2+kappa0_prime[species]+kappa0[species]*kappa0_sec[species])
        
    # Armstrong et al., eq S10
    return 1.-a[species]+a[species]*(1-(1.+b[species]*p)**(1.-c[species]))/(b[species]*(c[species]-1.)*p) # [-]

# Birch-Murnaghan
def Birch_Murnaghan_2nd_order(V,V0,K0):
    return 3./2*K0*((V0/V)**(7./3)-(V0/V)**(5./3))
    
def Birch_Murnaghan_3rd_order(V,V0,K0,K_prime):
   return 3./2*K0*((V0/V)**(7./3)-(V0/V)**(5./3))*(1.+3./4*(K_prime-4.)*((V0/V)**(2./3)-1.))
    
def Birch_Murnaghan_4th_order(V,V0,K0,K_prime,K_sec):
    return 3./2*K0*((V0/V)**(7./3)-(V0/V)**(5./3))*(1.+3./4*(K_prime-4.)*((V0/V)**(2./3)-1.)+1./24*(9.*K_prime**2-63.*K_prime+9.*K0*K_sec+143.)*((V0/V)**(2./3)-1.)**2)
    
#def BM4_inv(p,params):
#    V0      = params['V0']      # Angstroem
#    K0      = params['K0']      # Pa
#    K_prime = params['K_prime'] # non-dim
#    K_sec   = params['K_sec']   # 1/Pa
#    return root_scalar(lambda V:p-Birch_Murnaghan_4th_order(V,V0,K0,K_prime,K_sec),method='bisect',bracket=(0.3*V0,1.1*V0)).root*(1e-30*N_Avogadro)

### Delta_rG_0 [2FeO+=2Fe2O3]
    
# Armstrong et al., 2019 (eq S4)
def Delta_rG0_A19(T):
    return (-16201./T+8.031)*R_gas*T

# Deng et al., 2020
def Delta_rG0_D20(T):
    
    a = -331035.9211346371 # -3.310e5
    b = -190.3795512883899 # -190.379
    c = 14.785873706952849 # 14.785
    d = -0.0016487959655627517 # -1.649e-3
    e = 9348044.38934694 # 9.348e6
    f = 10773.299613088355 #1.077e4
    
    return a+b*T+c*T*np.log(T)+d*T**2+e*T**-1+f*T**(0.5) # J/mol

# Komabayashi 2014
def G_Fe_liquid(T):
    a = -9007.3402
    b = 290.29866
    c = -46.
    return a+b*T+c*T*np.log(T)

def G_FeO_liquid(T):
    a = -245310.
    b = 231.879
    c = -46.12826
    d = -0.0057402984
    return a+b*T+c*T*np.log(T)+d*T**2

def G_FeO_solid(T):
    a = -279318.
    b = 252.848
    c = -46.12826
    d = -0.0057402984
    return a+b*T+c*T*np.log(T)+d*T**2

#### Equilibrium constants volatiles
    
# Ortenzi et al., table 2
Delta_fG0_O20 = {'CO': lambda T: -214104.0+25.2183*T*np.log(T)-262.1545*T,
                 'CO2':lambda T: -392647.0 +4.5855*T*np.log(T) -16.9762*T,
                 'H2O':lambda T: -483095.0+25.3687*T*np.log(T) +21.9563*T}

equilibrium_constant_O20 = {eq_C_exp: lambda T: np.exp(-(2*Delta_fG0_O20['CO2'](T)-Delta_fG0_O20['CO'](T))/R_gas/T),
                            eq_H_exp: lambda T: np.exp(-Delta_fG0_O20['H2O'](T)/R_gas/T)}
    

Delta_fG0_NATAF = {'H2O':lambda T:utils.array2float(JANAF['H2O'].to_xarray().interp({'T(K)':utils.float2array(T)})['delta-f G'].data*1000),
                   'CO' :lambda T:utils.array2float(JANAF['CO'].to_xarray().interp({'T(K)':utils.float2array(T)})['delta-f G'].data*1000),
                   'CO2':lambda T:utils.array2float(JANAF['CO2'].to_xarray().interp({'T(K)':utils.float2array(T)})['delta-f G'].data*1000),
                   'CH4':lambda T:utils.array2float(JANAF['CH4'].to_xarray().interp({'T(K)':utils.float2array(T)})['delta-f G'].data*1000)}
                   
equilibrium_constant_NATAF = {eq_C_exp:lambda T: np.exp(-2*(Delta_fG0_NATAF['CO2'](T)-Delta_fG0_NATAF['CO'](T))/R_gas/T),\
                              eq_H_exp:lambda T: np.exp(-2*Delta_fG0_NATAF['H2O'](T)/R_gas/T),
                              eq_CH_exp:lambda T:np.exp(-(Delta_fG0_NATAF['H2O'](T)+Delta_fG0_NATAF['CH4'](T)-Delta_fG0_NATAF['CO'](T))/R_gas/T)}

# Bower et al., 2021
equilibrium_constant_Bower21 = {eq_C_exp: lambda T: 10**((14468./T-4.348)*2),
                                eq_H_exp: lambda T: 10**((13152./T-3.039)*2)}

# Zahnle et al., 2020
equilibrium_constant_Zahnle20 = {eq_H_exp:  lambda T:1.158e-6*np.exp(59911./T),
                                 eq_N1_exp: lambda T:5.90e-13*np.exp(13207./T),
                                 eq_N2_exp: lambda T:1.99*np.exp(-6339.4/T),
                                 eq_CH_exp: lambda T:5.239e-14*np.exp(27285./T)}

#### fO2 parametrizations

# Armstrong et al., 2019
def fO2_A19(p,T,ferric_to_total_iron,composition):
    # default composition: BSE (Mc Donnough & Sun 1995)
    # eq. S12 re-written to compute fO2 with:
    # R = XFe2O3 / (XFe2O3 + XFeO)
    return np.exp(4*( np.log(ferric_to_total_iron/(1.-ferric_to_total_iron)) \
                     +(Delta_rG0_A19(T)+p*V_0T_A19(T,'Fe2O3')*TEOS_A19_int(p,'Fe2O3')-p*V_0T_A19(T,'FeO')*TEOS_A19_int(p,'FeO'))/R_gas/T \
                     +2248*composition['MgO']/100/T-7690*composition['CaO']/100/T-8553*composition['Na2O']/100/T-5644*composition['K2O']/100/T+6278*composition['Al2O3']/100/T\
                     -6880*composition['FeO']/100*(1.-2*ferric_to_total_iron)/T))

def fO2_sfc_A19(T,ferric_to_total_iron,composition):
    # default composition: BSE (Mc Donnough & Sun 1995)
    # eq. S12 re-written to compute fO2 with:
    # R = XFe2O3 / (XFe2O3 + XFeO)
    # X_NaO_0.5 = 2 * X_Na2O (the coef is given for X_aO_0.5 in the paper, but X_Na2O is given in the BSE compo)
    # same for K2O and Al2O3
    return np.exp(4*( np.log(ferric_to_total_iron/(1.-ferric_to_total_iron))+(Delta_rG0_A19(T))/R_gas/T\
                     +2248*composition['MgO']/100/T-7690*composition['CaO']/100/T-8553*2*composition['Na2O']/100/T-5644*2*composition['K2O']/100/T+6278*2*composition['Al2O3']/100/T\
                     -6880*composition['FeO']/100*(1.-2*ferric_to_total_iron)/T))

# Deng et al., 2020    
def fO2_D20(p,T,ferric_to_total_iron,composition,fit=3):
    p0 = 1e5 # Pa
    
    if isinstance(p,np.ndarray):
        fO2 = np.array([])
        for pi,Ti in zip(p,T):
            fO2 = np.append(fO2,fO2_D20(pi,Ti,ferric_to_total_iron,composition,fit))
        return fO2
    else:
        return np.exp(4*( np.log(ferric_to_total_iron/(1.-ferric_to_total_iron)) \
                         +(Delta_rG0_D20(T)+quad(lambda p_: EOS_D2020(p_,T,coefs_Deng2020['Mg14Fe2Si16O49'])-EOS_D2020(p_,T,coefs_Deng2020['Mg14Fe2Si16O48']),p0,p)[0])/R_gas/T\
                         +sum([WFe2O3_WFeO['D20'+str(fit)][oxide]*composition[oxide]/100/R_gas/T for oxide in WFe2O3_WFeO['D20'+str(fit)].keys()])\
                         +WFeOFe2O3['D20'+str(fit)]*composition['FeO']/100*(1.-2*ferric_to_total_iron)/R_gas/T))
        
def fO2_sfc_D20(T,ferric_to_total_iron,composition,fit=3):
    
    return np.exp(4*( np.log(ferric_to_total_iron/(1.-ferric_to_total_iron)) \
                     +Delta_rG0_D20(T)/R_gas/T\
                     +sum([WFe2O3_WFeO['D20'+str(fit)][oxide]*composition[oxide]/100/R_gas/T for oxide in WFe2O3_WFeO['D20'+str(fit)].keys()])\
                     +WFeOFe2O3['D20'+str(fit)]*composition['FeO']/100*(1.-2*ferric_to_total_iron)/R_gas/T))

# Hirschmann 2022        
def fO2_H22(p, T, ferric_to_total_iron , composition, p_dep='D20'):
    # Hirschmann 2022's equation 22 is for composition in single-cation formula (e.g. AlO_1.5 not Al2O3)
    p0 = 1e5 # Pa
    R  = 8.314
    
    a       = 0.1917
    b       = -1.961
    c       = 4158.1
    DeltaCp = 33.25
    T0      = 1673.15
    gamma1  = -520.46
    gamma2  = -185.37
    gamma3  = 494.39
    gamma4  = 1838.34
    gamma5  = 2888.48
    gamma6  = 3473.68
    gamma7  = -4473.6
    gamma8  = -1245.09
    gamma9  = -1156.86
    
    FeO15_FeO = ferric_to_total_iron / (1. - ferric_to_total_iron)
    
    if p_dep == 'D20':
        p_term = quad(lambda p_: EOS_D2020(p_,T,coefs_Deng2020['Mg14Fe2Si16O49'])/2-EOS_D2020(p_,T,coefs_Deng2020['Mg14Fe2Si16O48'])/2,p0,p)[0]
    elif p_dep == 'A19':
        p_term = p*V_0T_A19(T,'Fe2O3')*TEOS_A19_int(p,'Fe2O3')-p*V_0T_A19(T,'FeO')*TEOS_A19_int(p,'FeO')
    elif p_dep == 'K23':
        p_term = p*V_0T_A19(T,'Fe2O3')*TEOS_K23_int(p,'Fe2O3')-p*V_0T_A19(T,'FeO')*TEOS_K23_int(p,'FeO')
    
    return 10**(np.log10(FeO15_FeO)/a \
                -(b+c/T-DeltaCp/R/np.log(10)*(1.-T0/T-np.log(T/T0)))/a \
                +p_term/R/T/np.log(10)/a \
                -1./T/a*( gamma1*composition['SiO2']/100+gamma2*composition['TiO2']/100\
                         +gamma3*composition['MgO']/100+gamma4*composition['CaO']/100\
                         +gamma5*2*composition['Na2O']/100+gamma6*2*composition['K2O']/100\
                         +gamma7*2*composition['P2O5']/100\
                         +gamma8*composition['SiO2']/100*2*composition['Al2O3']/100\
                         +gamma9*composition['SiO2']/100*composition['MgO']/100))
        
def fO2_sfc_H22(T, ferric_to_ferrous_iron , composition, p_dep='D20'):# Hirschmann 2022's equation 22 is for composition in single-cation formula (e.g. AlO_1.5 not Al2O3)

    R  = 8.314
    
    a       = 0.1917
    b       = -1.961
    c       = 4158.1
    DeltaCp = 33.25
    T0      = 1673.15
    gamma1  = -520.46
    gamma2  = -185.37
    gamma3  = 494.39
    gamma4  = 1838.34
    gamma5  = 2888.48
    gamma6  = 3473.68
    gamma7  = -4473.6
    gamma8  = -1245.09
    gamma9  = -1156.86
    
    return 10**(np.log10(ferric_to_ferrous_iron)/a \
                -(b+c/T-DeltaCp/R/np.log(10)*(1.-T0/T-np.log(T/T0)))/a \
                -1./T/a*( gamma1*composition['SiO2']/100+gamma2*composition['TiO2']/100\
                         +gamma3*composition['MgO']/100+gamma4*composition['CaO']/100\
                         +gamma5*2*composition['Na2O']/100+gamma6*2*composition['K2O']/100\
                         +gamma7*2*composition['P2O5']/100\
                         +gamma8*composition['SiO2']/100*2*composition['Al2O3']/100\
                         +gamma9*composition['SiO2']/100*composition['MgO']/100))
    
    
def ferric_to_ferrous_iron_H22(p, T, fO2, composition,p_dep='D20'):
    # Hirschmann 2022's equation 22 is for composition in single-cation formula (e.g. AlO1.5 not Al2O3)
    p0 = 1e5 # Pa
    R  = 8.314
    
    a       = 0.1917
    b       = -1.961
    c       = 4158.1
    DeltaCp = 33.25
    T0      = 1673.15
    gamma1  = -520.46
    gamma2  = -185.37
    gamma3  = 494.39
    gamma4  = 1838.34
    gamma5  = 2888.48
    gamma6  = 3473.68
    gamma7  = -4473.6
    gamma8  = -1245.09
    gamma9  = -1156.86
    
    if p_dep == 'D20':
        p_term = quad(lambda p_: EOS_D2020(p_,T,coefs_Deng2020['Mg14Fe2Si16O49'])/2-EOS_D2020(p_,T,coefs_Deng2020['Mg14Fe2Si16O48'])/2,p0,p)[0]
    elif p_dep == 'A19':
        p_term = p*V_0T_A19(T,'Fe2O3')*TEOS_A19_int(p,'Fe2O3')-p*V_0T_A19(T,'FeO')*TEOS_A19_int(p,'FeO')
        
    return 10**(a*np.log10(fO2)+b+c/T \
                -DeltaCp/R/np.log(10)*(1.-T0/T-np.log(T/T0)) \
                -p_term/R/T/np.log(10) \
                +1./T*( gamma1*composition['SiO2']/100+gamma2*composition['TiO2']/100\
                       +gamma3*composition['MgO']/100+gamma4*composition['CaO']/100\
                       +gamma5*2*composition['Na2O']/100+gamma6*2*composition['K2O']/100\
                       +gamma7*2*composition['P2O5']/100\
                       +gamma8*composition['SiO2']/100*2*composition['Al2O3']/100\
                       +gamma9*composition['SiO2']/100*composition['MgO']/100))
            
#### Data and values
        
# Activity coefficients
WFe2O3_WFeO                       = {}
WFeOFe2O3                         = {}
# Armostrong et al., 2019
WFe2O3_WFeO['A19']                = {}
WFe2O3_WFeO['A19']['MgO']         =  2248.
WFe2O3_WFeO['A19']['Al2O3']       =  6278.
WFe2O3_WFeO['A19']['CaO']         = -7690.
WFe2O3_WFeO['A19']['Na2O']        = -8553.
WFe2O3_WFeO['A19']['K2O']         = -5644.
WFeOFe2O3['A19']                  = -6880.
# Deng et al., 2020 Fit 1
WFe2O3_WFeO['D201']               = {}
WFe2O3_WFeO['D201']['MgO']        =  75011.
WFe2O3_WFeO['D201']['SiO2']       =  2029.
WFe2O3_WFeO['D201']['Al2O3']      =  30836.
WFe2O3_WFeO['D201']['CaO']        = -65959.
WFe2O3_WFeO['D201']['Na2O']       =  0.
WFe2O3_WFeO['D201']['K2O']        = -49254.
WFe2O3_WFeO['D201']['P2O5']       =  1.
WFe2O3_WFeO['D201']['TiO2']       = -9843.
WFeOFe2O3['D201']                 = -11552.
# Deng et al., 2020 Fit 2
WFe2O3_WFeO['D202']               = {}
WFe2O3_WFeO['D202']['MgO']        =  37352.
WFe2O3_WFeO['D202']['SiO2']       =  0.
WFe2O3_WFeO['D202']['Al2O3']      =  42858.
WFe2O3_WFeO['D202']['CaO']        = -42218.
WFe2O3_WFeO['D202']['Na2O']       =  -21803.
WFe2O3_WFeO['D202']['K2O']        = -34275.
WFe2O3_WFeO['D202']['P2O5']       =  119741.
WFe2O3_WFeO['D202']['TiO2']       = -105079. 
WFeOFe2O3['D202']                 = -2.
# Deng et al., 2020 Fit 3
WFe2O3_WFeO['D203']               = {}
WFe2O3_WFeO['D203']['MgO']        =  68629.
WFe2O3_WFeO['D203']['SiO2']       =  4601.
WFe2O3_WFeO['D203']['Al2O3']      =  40923.
WFe2O3_WFeO['D203']['CaO']        = -58109.
WFe2O3_WFeO['D203']['Na2O']       =  0.
WFe2O3_WFeO['D203']['K2O']        = -59584.
WFe2O3_WFeO['D203']['P2O5']       =  0.
WFe2O3_WFeO['D203']['TiO2']       =  0 
WFeOFe2O3['D203']                 = -14210.
# Deng et al., 2020 Fit 4
WFe2O3_WFeO['D204']               = {}
WFe2O3_WFeO['D204']['MgO']        =  35409.
WFe2O3_WFeO['D204']['SiO2']       =  4101.
WFe2O3_WFeO['D204']['Al2O3']      =  45924.
WFe2O3_WFeO['D204']['CaO']        = -50920.
WFe2O3_WFeO['D204']['Na2O']       =  0.
WFe2O3_WFeO['D204']['K2O']        = -62943.
WFe2O3_WFeO['D204']['P2O5']       =  0.
WFe2O3_WFeO['D204']['TiO2']       =  0. 
WFeOFe2O3['D204']                 = -8735.
    
coefs_Deng2020 = pd.read_excel(datapath+'/Deng2020_fit.ods',index_col=0).T

    
### References
    
# Armstrong et al., Science, 2019
# Deng et al., Nat. Comm., 2020
# Katyal et al., ApJ, 2020
# Ortenzi et al., Sci. Rep., 2020
# Bower et al., PSJ, 2021
