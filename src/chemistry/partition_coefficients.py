#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 16:39:39 2022

@author: maxime
"""

import numpy as np
from chemistry import elements

H_part_coef_ET2008 = 0.0062085 # computed from Elkins-Tanton 2008 supplementary materials
C_part_coef_ET2008 = 0.0009301 # computed from Elkins-Tanton 2008 supplementary materials

part_coefs = {}
part_coefs['2000km'] = {}
part_coefs['2000km'][22e9]  = {'Fe3+':0.7125, 'H':0.000495,'C':0.0005} # alternative value for Fe2O3: 38
part_coefs['2000km'][18e9]  = {'Fe3+':0.4365, 'H':0.01515, 'C':0.0005} # alternative value for Fe2O3: 2.5475
part_coefs['2000km'][15e9]  = {'Fe3+':0.3305, 'H':0.00605, 'C':0.001925} # alternative value for Fe2O3: 1.395
part_coefs['2000km'][2.5e9] = {'Fe3+':0.149,  'H':0.009,   'C':0.00173} # alternative value for Fe2O3: 0.173
part_coefs['2000km'][1e9]   = {'Fe3+':0.268,  'H':0.011,   'C':0.002105} # alternative value for Fe2O3: .3175
part_coefs['2000km'][0]     = {'Fe3+':0.171,  'H':0.01,    'C':0.00208} # alternative value for Fe2O3: 0.1965

def part_coefs_lookup(p,elem,seq):
    for p_ in part_coefs[seq]:
        if p<p_:
            continue
        else:
            return part_coefs[seq][p_][elem]
get_part_coef = np.vectorize(part_coefs_lookup,excluded=[1,2])