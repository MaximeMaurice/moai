#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 16:38:05 2022

@author: maxime
"""

from copy import deepcopy

class element:
    
    def __init__(self,symbol,atomic_mass):
        
        self.symbol     = symbol
        self.name       = symbol
        self.atomic_mass = atomic_mass
        
H  = element('H',1.)
C  = element('C',12.)
N  = element('N',14.)
O  = element('O',16.)
Cl = element('Cl',17.)
Fe = element('Fe',56.)
S = element('S',32.)
Mo = element('Mo',666)

H.part_coef = 0.01
C.part_coef = 0.001
N.part_coef = 0. # 0.0001

# Those below are not elements, but there are also involved in mass conservation
# TODO: ion class

Fe_ferric = deepcopy(Fe)
Fe_ferric.part_coef = 0.2 # not used
Fe_ferric.volatile  = False
Fe_ferric.molecular_mass = 56+1.5*16
Fe_ferric.symbol    = 'Fe3+'
Fe_ferric.name      = 'Fe3+'

Fe_ferrous = deepcopy(Fe)
Fe_ferrous.part_coef = 0.94
Fe_ferrous.volatile  = False
Fe_ferrous.molecular_mass = 56+16
Fe_ferrous.symbol    = 'Fe2+'
Fe_ferrous.name      = 'Fe2+'

elems_book = {H.symbol:H,
             N.symbol:N,
             O.symbol:O,
             C.symbol:C,
             Cl.symbol:Cl,
             S.symbol:S,
             Fe_ferrous.symbol:Fe_ferrous,
             Fe_ferric.symbol:Fe_ferric,
             Mo.symbol:Mo}