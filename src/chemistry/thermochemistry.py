#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  6 16:52:09 2022

@author: maxime
"""

from datapath import datapath
import pandas as pd

# NATAF tables
JANAF        = {}
for sp in ['H2','H2O','CO','CO2','N2','NH3','HCN','CH4','O2','SH2','S2','SO2','HCl']:
    JANAF[sp] = pd.read_excel(datapath+'/JANAF.ods',sheet_name=sp,index_col=0,skiprows=1)