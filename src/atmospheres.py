import numpy as np
import copy
import utils
from physics.constants import Earth_surface_radius,Earth_gravity
from scipy.integrate import quad,ode
from scipy.interpolate import interp1d
from numpy.linalg import norm
from scipy.constants import R, Stefan_Boltzmann
from tools import *

#### Atmosphere classes
        
class atmosphere:
    
    def __init__(self, species, pps, Ts, **kw):

        if 'g' in kw:
            self.gravity = kw['g']
        else:
            self.gravity = Earth_gravity
        if 'R' in kw:
            self.R_surf  = kw['R']
        else:
            self.R_surf  = Earth_surface_radius
        if 'T_eq' in kw:
            self.T_eq = kw['T_eq']
        else:
            self.T_eq = 256. # [K]
        if 'pToA' in kw:
            self.pToA = kw['pToA']
        else:
            self.pToA = 1.
            
        self.Ts        = Ts
        self.ps        = sum(pps)
        self.average_molecular_mass = 1.
        
        self.species             = {}
        self.partial_pressure    = {}
        self.volume_mixing_ratio = {}
        self.mass_mixing_ratio   = {}
        
        self.equilibria          = []
        
        for sp,pp in zip(species,pps):
            self.addSpecies(sp,pp)

    def addSpecies(self,species,pp):
        
        self.species[species.formula] = species
        self.updatePartialPressure(species.formula,pp)
        
    def updatePartialPressure(self, species, p):
        
        self.partial_pressure[species] = p
        self.ps                        = sum(self.partial_pressure.values())
        self.updateMixingRatios()
        
    def updateMixingRatios(self):
        for sp in self.species:
            self.volume_mixing_ratio[sp] = self.partial_pressure[sp]/self.ps
        self.updateAverageMolecularMass()
        for sp in self.species:
            self.mass_mixing_ratio[sp] = self.species[sp].molecular_mass/self.average_molecular_mass*self.partial_pressure[sp]/self.ps
        
    def updateAverageMolecularMass(self):
        
        self.average_molecular_mass = 0.
        for sp in self.species:
            self.average_molecular_mass += self.volume_mixing_ratio[sp]*self.species[sp].molecular_mass
            
    def updateTs(self, Ts):
        
         self.Ts = Ts

    def getMass(self, species='total'):
        
        if species == 'total':
            return 4*np.pi*self.R_surf**2/self.gravity*self.ps
        else:
            return 4.*np.pi*self.R_surf**2/self.gravity*self.mass_mixing_ratio[species]*self.ps
    
class black_body(atmosphere):
    
    def getOLR(self):
        return Stefan_Boltzmann*self.Ts**4
            
    def getASR(self):
        return Stefan_Boltzmann*self.T_eq**4
    
class multi_species_single_condensible_convective_atmosphere(atmosphere):
    
    def __init__(self,species, pps, Ts, **kw):
            
        atmosphere.__init__(self, species, pps, Ts, **kw)
        
        if 'pToA' in kw:
            self.pToA = kw['pToA']
        else:
            self.pToA = 1. # [Pa]
        if 'dlnpa' in kw:
            self.dlnpa = -kw['dlnpa']
        else:
            self.dlnpa = -0.1 # [Pa]
        if 'N' in kw:
            self.N = kw['N']
        else:
            self.N = 100
        if 'T_str' in kw:
            self.T_str=kw['T_str']
        else:
            self.T_str = 0.
        self.p_str = self.pToA
        if 'cond' in kw:
            self.i_cond = list(species).index(kw['cond'])
        else:
            self.i_cond = 0
        if 'rain_out' in kw:
            self.rain_out = kw['rain_out']
        else:
            self.rain_out = True
        self.compute_HE = False
            
        self.gases                        = species
        self.profiles                     = {}
        self.profiles['partial pressure'] = {}
        self.profiles['molar fraction']   = {}
        self.profiles['mass fraction']    = {}
        self.profiles['tot molar fraction']   = {}
        self.profiles['tot mass fraction']    = {}
        
        # Condensible and air
        self.cond = species[self.i_cond]
        self.air  = utils.dummy() # will be initialized later
        self.eta0 = pps[self.i_cond] / sum(np.append(pps[:self.i_cond],pps[self.i_cond+1:]))
        
        # lapse rates
        self.dry_lapse_rate   = lambda lnp,  lnT: R/self.air.cp_mol(np.exp(lnT))
        if self.rain_out:
            self.moist_lapse_rate = lambda lnpa, lnT: (R+self.cond.L_mol/np.exp(lnT)*self.cond.psat(np.exp(lnT))/np.exp(lnpa))\
                                                     /(self.air.cp_mol(np.exp(lnT))+(self.cond.cp_mol(np.exp(lnT))+self.cond.L_mol**2/R/np.exp(lnT)**2-self.cond.L_mol/np.exp(lnT))*self.cond.psat(np.exp(lnT))/np.exp(lnpa))
        else:
            self.moist_lapse_rate = lambda lnpa, lnT: (R+self.cond.L_mol/np.exp(lnT)*self.cond.psat(np.exp(lnT))/np.exp(lnpa))\
                                                     / (self.air.cp_mol(np.exp(lnT))\
                                                      +(self.cond.cp_mol(np.exp(lnT))+self.cond.L_mol**2/R/np.exp(lnT)**2-self.cond.L_mol/np.exp(lnT))*self.cond.psat(np.exp(lnT))/np.exp(lnpa)\
                                                      +(self.eta0-self.cond.psat(np.exp(lnT))/np.exp(lnpa))*self.cond.cp_cond(np.exp(lnT)))
        
        # Update
        self.update(Ts,pps)
        
        if 'compute_HE' in kw and kw['compute_HE'] == True:
            self.set_hydrostatic_equilibrium()
            self.update(Ts, pps)
            
    def set_hydrostatic_equilibrium(self):
        
        self.compute_HE = True
        dz_dp = lambda p,z: -utils.interp(p,self.profiles['pressure'],R*1000/self.profiles['average molecular mass'])\
                             *utils.interp(p,self.profiles['pressure'],self.profiles['temperature'])\
                             /p/(self.gravity*(self.R_surf/(self.R_surf+z))**2)
        self.hydrostatic_equilibrium = lithostatic_equilibrium(self.ps, self.pToA, dz_dp, log=True, N=1000)
        self.update(self.Ts,[self.partial_pressure[sp] for sp in self.partial_pressure])
        
    # Overload atmosphere functions:
    def updateTs(self, Ts):
        
        self.update(Ts,np.array([self.partial_pressure[sp] for sp in self.species]))
        
    def update_air(self,species,x):
        
        self.air  = utils.dummy()
        self.air.molecular_mass = sum([x_*sp.molecular_mass for x_,sp in zip(x,species)])
        self.air.cp_mol         = lambda T: sum([x_*sp.cp_mol(T) for x_,sp in zip(x,species)])
        
    def update(self, Ts, pps):
        
        self.Ts = Ts
        T,pp = self.solve(self.Ts,np.array(pps))
        #self.profiles['temperature'],self.profiles['partial pressure'] = self.solve(self.Ts,pps)
        
        # surface values
        for i,gas in enumerate(self.gases):
            #atmosphere.updatePartialPressure(self, gas.formula, self.profiles['partial pressure'][gas.formula][0])
            atmosphere.updatePartialPressure(self, gas.formula, pp[gas.formula][0])
        
        # profiles
        #self.profiles['pressure'] = np.zeros_like(self.profiles['temperature'])
        self.profiles['pressure'] = np.logspace(np.log10(self.ps),np.log10(self.pToA),self.N)
        p = np.zeros_like(T)
        for i in range(len(T)):
            for gas in self.gases:
                #self.profiles['pressure'][i] += self.profiles['partial pressure'][gas.formula][i]
                p[i] += pp[gas.formula][i]
                
        self.profiles['temperature'] = utils.interp(self.profiles['pressure'],p,T)
        self.profiles['average molecular mass']            = np.zeros_like(self.profiles['pressure'])
        for gas in self.gases:
            self.profiles['partial pressure'][gas.formula] = utils.interp(self.profiles['pressure'],p,pp[gas.formula])
            self.profiles['average molecular mass']       += self.profiles['partial pressure'][gas.formula]*gas.molecular_mass
        
        self.profiles['average molecular mass']           /= self.profiles['pressure']
        
        for gas in self.gases:
            self.profiles['molar fraction'][gas.formula] = self.profiles['partial pressure'][gas.formula] / self.profiles['pressure']
        for gas in self.gases:
            self.profiles['mass fraction'][gas.formula]  = self.profiles['molar fraction'][gas.formula]*gas.molecular_mass/self.profiles['average molecular mass']
           
        if not self.rain_out and self.cloud_deck < self.ps:
            gas2tot = (1.-self.profiles['molar fraction'][self.cond.formula][0])/(1.-self.profiles['molar fraction'][self.cond.formula])
            for gas in self.gases:
                self.profiles['tot molar fraction'][gas.formula] = self.profiles['molar fraction'][gas.formula]*gas2tot
            self.profiles['tot molar fraction'][self.cond.formula+'c'] = 1.-gas2tot
            
            for gas in self.gases:
                self.profiles['tot mass fraction'][gas.formula] = self.profiles['tot molar fraction'][gas.formula] * gas.molecular_mass / self.profiles['average molecular mass'][0]
            self.profiles['tot mass fraction'][self.cond.formula+'c'] = self.profiles['tot molar fraction'][self.cond.formula+'c'] * self.cond.molecular_mass / self.profiles['average molecular mass'][0]
            
        if self.compute_HE:
            self.hydrostatic_equilibrium.update_p_surf(self.ps)
            self.hydrostatic_equilibrium.update_lookup()
            self.profiles['height'] = self.hydrostatic_equilibrium.getz(self.profiles['pressure'])
            
    def solve(self,Ts,pps):
            
        # Ts is the surface temperature

        # Initialize
        
        if pps[self.i_cond] < self.cond.psat(Ts):
            # No condensation at the surface
            self.update_air(self.gases,pps/sum(pps))
            pp              = pps                 # partial pressure
            T               = np.array([Ts])      # temperature
            p               = np.array([sum(pp)]) # pressure
            condensing      = False
            self.cloud_deck = self.pToA
        else:
            # Condensation at the surface
            self.update_air(np.append(self.gases[:self.i_cond],self.gases[self.i_cond+1:]),\
                            np.append(pps[:self.i_cond],pps[self.i_cond+1:])/sum(np.append(pps[:self.i_cond],pps[self.i_cond+1:])))
            pp              = np.append(pps[:self.i_cond],np.append(self.cond.psat(self.Ts),pps[self.i_cond+1:]))
            pa              = sum(np.append(pps[:self.i_cond],pps[self.i_cond+1:]))
            T               = np.array([Ts])      # temperature
            p               = np.array([sum(pp)]) # total pressure
            condensing      = True
            self.cloud_deck = pp[self.i_cond]
            print(self.cond.formula,'condensing at the surface, ps corrected from ', sum(pps), 'to ', p)
            
        pp_dict = {}
        for i,sp in enumerate(self.species):
            pp_dict[sp] = np.array([pp[i]])
        
        # Solve
        while not condensing:
            # Dry adiabat
            
            # R-K 4th orer coefficients
            k1 = self.dry_lapse_rate(np.log(p),        np.log(T[-1]))
            k2 = self.dry_lapse_rate(np.log(p)+self.dlnpa/2,np.log(T[-1])+self.dlnpa*k1/2)
            k3 = self.dry_lapse_rate(np.log(p)+self.dlnpa/2,np.log(T[-1])+self.dlnpa*k2/2)
            k4 = self.dry_lapse_rate(np.log(p)+self.dlnpa,  np.log(T[-1])+self.dlnpa*k3)
            
            # Increment pressure and temperature
            p = np.append(p,np.exp(np.log(p[-1])+self.dlnpa))
            T = np.append(T,max(np.exp(np.log(T[-1])+(k1/6+k2/3+k3/3+k4/6)*self.dlnpa),self.T_str))
            
            # Calculate partial pressures
            pp = pps/sum(pps)*p[-1]
                
            # If condensation occurs
            if pp[self.i_cond] > self.cond.psat(T[-1]):
                self.update_air(np.append(self.gases[:self.i_cond],self.gases[self.i_cond+1:]),\
                                np.append(pps[:self.i_cond],pps[self.i_cond+1:])/sum(np.append(pps[:self.i_cond],pps[self.i_cond+1:])))
                # Doing so is resolution-dependent
                T[-1]           = self.cond.T_dew(pp[self.i_cond])
                self.cloud_deck = p[-1]
                pa              = sum(np.append(pp[:self.i_cond],pp[self.i_cond+1:]))
                condensing      = True
                
            if T[-1] == self.T_str and p[-1] > self.p_str:
                self.p_str = p[-1]
                
            # Write partial pressures
            for i,sp in enumerate(self.species):
                pp_dict[sp] = np.append(pp_dict[sp],pp[i])
                    
            # Reached top of atmosphere
            if p[-1] < self.pToA:
                return T,pp_dict
            
        while p[-1] > self.pToA:
            # Moist adiabat
            
            # R-K 4th orer coefficients
            k1 = self.moist_lapse_rate(np.log(pa),        np.log(T[-1]))
            k2 = self.moist_lapse_rate(np.log(pa)+self.dlnpa/2,np.log(T[-1])+self.dlnpa*k1/2)
            k3 = self.moist_lapse_rate(np.log(pa)+self.dlnpa/2,np.log(T[-1])+self.dlnpa*k2/2)
            k4 = self.moist_lapse_rate(np.log(pa)+self.dlnpa,  np.log(T[-1])+self.dlnpa*k3)
            
            pa = np.exp(np.log(pa)+self.dlnpa)
            T  = np.append(T,max(np.exp(np.log(T[-1])+(k1/6+k2/3+k3/3+k4/6)*self.dlnpa),self.T_str))
            
            if T[-1] <= self.T_str:
                T = np.append(T[:-1],[self.T_str,self.T_str])
                
                x = pp/p[-1]
                p = np.append(p,pa+self.cond.psat(T[-1]))
                self.p_str = p[-1]
                
                # Write partial pressures in the whole stratosphere
                for i,sp in enumerate(self.species):
                    pp_dict[sp] = np.append(pp_dict[sp],[x[i]*p[-1],x[i]*self.pToA])
                break
            
            # Partial and total pressures
            pp               = pps/sum(np.append(pps[:self.i_cond],pps[self.i_cond+1:]))*pa
            pp[self.i_cond]  = self.cond.psat(T[-1])
            p                = np.append(p,sum(pp))
            
            # Write partial pressures
            for i,sp in enumerate(self.species):
                pp_dict[sp] = np.append(pp_dict[sp],pp[i])
            
        return T,pp_dict
        
class single_species_condensible_convective_grey_atmosphere(atmosphere):
    
    def __init__(self, species, ps, Ts, **kw):
        
        if 'N' in kw:
            self.N = kw['N']
        else:
            self.N = 100
            
        atmosphere.__init__(self, [species], [ps], Ts, **kw)

        self.dlnT_dlnp   = lambda log_p,log_T: R/species.molecular_mass/species.cp(np.exp(log_T))
        self.dry_adiabat = adiabat(Ts, ps, self.pToA, self.dlnT_dlnp, log=True, N=self.N)
        self.profiles    = {}
        self.updateStructure()
        
    #### Saturation values ####

    def psat(self, T):
        sp = list(self.species.values())[0]
        return sp.CriticalPointP*np.exp(-sp.L_vaporization/(R/sp.molecular_mass)*(1./T-1./sp.CriticalPointT))

    def Tsat(self, p):
        sp = list(self.species.values())[0]
        return sp.CriticalPointT/(1-(R/sp.molecular_mass*sp.CriticalPointT/sp.L_vaporization)*np.log(p/sp.CriticalPointP))
    
    #### 2-D structure ####

    def getT(self,p):
        return utils.interp(p,self.profiles['pressures'],self.profiles['temperatures'])

    def getdTdp(self,p):
        return utils.interp(p,self.profiles['pressures'],self.dTdp_lookup)
    
    def updateStructure(self,temperature=True,pressure=True):
        if pressure:
            self.profiles['pressures'] = np.logspace(np.log10(self.ps),np.log10(self.pToA),self.N)
            self.dry_adiabat.updateP(self.ps,self.pToA)
        if temperature:
            self.dry_adiabat.updateT_pot(self.Ts)
            self.profiles['temperatures'] = np.maximum(self.dry_adiabat.getT(self.profiles['pressures']),\
                                                   self.Tsat(self.profiles['pressures']))
            
        self.dTdp_lookup  = np.gradient(self.profiles['temperatures'],self.profiles['pressures'])
        
        self.getT = interp1d(self.profiles['pressures'],self.profiles['temperatures'])
        self.getdTdp = interp1d(self.profiles['pressures'],self.dTdp_lookup)

    #### Radiative transfer functions (from Pierrehumber 2010) ####

    def getTau(self, p):
        # constant absorption coefficient
        return list(self.species.values())[0].k0/self.gravity*(self.ps-p)
        
    def transmission(self, p1, p2):
        return np.exp(-abs(self.getTau(p1)-self.getTau(p2)))

    def getUpwardFlux(self,p,F_bot,epsrel=1e-5,epsabs=1.,limit=10,show_err=False):
        if isinstance(p,np.ndarray):
            fluxes = np.zeros_like(p)
            for i,p_i in enumerate(p):
                fluxes[i] = self.getUpwardFlux(p_i,F_bot,epsrel,epsabs,limit=limit)
            return fluxes
        
        integrand = lambda p_: self.transmission(p_,p)*Stefan_Boltzmann*self.getT(p_)**4*list(self.species.values())[0].k0/self.gravity
        BddTerm   = F_bot*self.transmission(self.ps,p)

        return quad(integrand,p,self.ps,epsrel=epsrel,epsabs=epsabs,limit=limit)[0] + BddTerm

    def getDownwardFlux(self,p,F_top,epsrel=1e-5,epsabs=1.,limit=10,show_err=False):
        if isinstance(p,np.ndarray):
            fluxes = np.zeros_like(p)
            for i,p_i in enumerate(p):
                fluxes[i] = self.getDownwardFlux(p_i,F_top,epsrel,epsabs,limit=limit)
            return fluxes
        
        integrand = lambda p_: self.transmission(p_,p)*Stefan_Boltzmann*self.getT(p_)**4*list(self.species.values())[0].k0/self.gravity
        BddTerm   = F_top*self.transmission(self.pToA,p)

        return quad(integrand,self.pToA,p,epsrel=epsrel,epsabs=epsabs,limit=limit)[0] + BddTerm
    
    def getOLR(self,F_bot,epsrel=1e-5,epsabs=1.,limit=10,show_err=False):
        return self.getUpwardFlux(self.pToA,F_bot,epsrel,epsabs,limit,show_err)
    
    

class radiative_grey_atmosphere(atmosphere):
    
    def __init__(self, species, pps, Ts, **kw):
        
        atmosphere.__init__(self, species, pps, Ts, **kw)
        if len(species) != 0:
            self.update_opacity()
        else:
            self.opacity = 0.

    def update_opacity(self):

        optical_depths = np.zeros(len(self.species))
        for i,sp in enumerate(self.species):
            optical_depths[i] = (3.*self.getMass(sp)/(8*np.pi*self.R_surf**2)*np.sqrt(self.species[sp].k0*self.gravity/(3.*self.species[sp].p0)))
        self.opacity = 2./(2.+sum(optical_depths))
        
    def getOLR(self):
        return Stefan_Boltzmann*self.opacity*self.Ts**4
        
    def getASR(self):
        return Stefan_Boltzmann*self.opacity*self.T_eq**4
        
    def updateTs(self, Ts):
        
        atmosphere.updateTs(self,Ts)
        self.update_opacity()
        
class convective_atmosphere(atmosphere):
    
    def __init__(self, species, pps, Ts, **kw):
        
        atmosphere.__init__(self, species, pps, Ts, **kw)
        
        if 'pToA' in kw:
            self.pToA = kw['pToA']
        else:
            self.pToA = 1. # [Pa]
        if 'dlnp' in kw:
            self.dlnp = -kw['dlnp']
        else:
            self.dlnp = -0.1 # [Pa]
        if 'N' in kw:
            self.N = kw['N']
        else:
            self.N = 100
        if 'T_str' in kw:
            self.T_str=kw['T_str']
        else:
            self.T_str = 0.
        self.p_str = self.pToA
        if 'alpha' in kw:
            self.alpha = np.array([kw['alpha'][sp.formula] for sp in species])
        else:
            self.alpha = np.zeros_like(species)
        self.compute_HE = False
            
        self.gases = np.array(species)
        self.profiles = {}
        self.profiles['partial pressure'] = {}
        self.profiles['molar fraction']   = {}
        self.profiles['mass fraction']    = {}
        self.profiles['x_v'] = {}
        self.profiles['x_c'] = {}
        self.p_cloud = {}
        for gas in self.gases:
            self.p_cloud[gas.formula] = self.pToA
        
        self.update(Ts, pps)
        
        if 'compute_HE' in kw and kw['compute_HE'] == True:
            self.set_hydrostatic_equilibrium()
            self.update(Ts, pps)
            
    def set_hydrostatic_equilibrium(self):
        
        self.compute_HE = True
        dz_dp = lambda p,z: -utils.interp(p,self.profiles['pressure'],R*1000/self.profiles['average molecular mass'])\
                             *utils.interp(p,self.profiles['pressure'],self.profiles['temperature'])\
                             /p/self.gravity
        self.hydrostatic_equilibrium = lithostatic_equilibrium(self.ps, self.pToA, dz_dp, log=True, N=1000)
        
    # Overload atmosphere functions:
    def updateTs(self, Ts):
        
        self.update(Ts,np.array([self.partial_pressure[sp] for sp in self.species]))
        
    def update(self, Ts, pps):
        
        self.Ts = Ts
        p,T,pps,x_v,x_c = self.solve(self.Ts,pps)
        
        # surface values
        for gas in self.gases:
            atmosphere.updatePartialPressure(self, gas.formula, pps[gas.formula][0])
        
        # profiles
        self.profiles['pressure'] = np.logspace(np.log10(self.ps),np.log10(self.pToA),self.N)
                
        self.profiles['temperature']                       = utils.interp(self.profiles['pressure'],p,T)
        self.profiles['average molecular mass']            = np.zeros_like(self.profiles['pressure'])
        
        for gas in self.gases:
            self.profiles['partial pressure'][gas.formula] = utils.interp(self.profiles['pressure'],p,pps[gas.formula])
            self.profiles['x_v'][gas.formula]              = utils.interp(self.profiles['pressure'],p,x_v[gas.formula])
            self.profiles['x_c'][gas.formula]              = utils.interp(self.profiles['pressure'],p,x_c[gas.formula])
            self.profiles['molar fraction'][gas.formula]   = self.profiles['partial pressure'][gas.formula]/self.profiles['pressure']
            self.profiles['average molecular mass']       += self.profiles['partial pressure'][gas.formula]*gas.molecular_mass
        
        self.profiles['average molecular mass']           /= self.profiles['pressure']
        for gas in self.gases:
            self.profiles['mass fraction'][gas.formula]    = self.profiles['molar fraction'][gas.formula]*gas.molecular_mass/self.profiles['average molecular mass']
                     
        if self.compute_HE:
            self.hydrostatic_equilibrium.update_p_surf(self.ps)
            self.hydrostatic_equilibrium.update_lookup()
            self.profiles['altitude'] = self.hydrostatic_equilibrium.getz(self.profiles['pressure'])
            
    def solve(self,Ts,pps):
        
        # initialize ######################
        
        T   = np.array([Ts])       # temperature
        p   = np.array([sum(pps)]) # pressure

        # pressures and fractions
        pp  = pps                # partial pressures
        x_0 = pp/sum(pp)         # initial molar fraction
        x_v = x_0                # vapour molar fraction
        x_c = np.zeros_like(x_0) # condensed molar fraction
        pp_dict  = {}
        x_v_dict = {}
        x_c_dict = {}
        for i,gas in enumerate(self.gases):
            pp_dict[gas.formula] = np.array([pp[i]])
            x_v_dict[gas.formula] = np.array([x_v[i]])
            x_c_dict[gas.formula] = np.array([x_c[i]])
            
        # lapse rate
        dlnT_dlnp = dlnT_dlnp_factory(self.gases,pps/sum(pps),alpha=self.alpha)
        dlnx_v_dlnp = {}
        for i,sp in enumerate(self.gases):
            dlnx_v_dlnp[sp] = dlnx_v_dlnp_factory(sp,self.gases,dlnT_dlnp)
        
        lapse_rate = lambda lnp, lnargs: np.append(dlnT_dlnp(lnp, lnargs[0], lnargs[1:]),
                                                   [dlnx_v_dlnp[sp](lnp, lnargs[0], lnargs[1:]) for sp in self.gases])
            
        while  p[-1] > self.pToA:
                    
            lnargs = np.append(np.log(T[-1]),[np.log(x_v_) for x_v_ in x_v])
            k1 = lapse_rate(np.log(p[-1]),lnargs)
            k2 = lapse_rate(np.log(p[-1])+self.dlnp/2,lnargs+k1*self.dlnp/2)
            k3 = lapse_rate(np.log(p[-1])+self.dlnp/2,lnargs+k2*self.dlnp/2)
            k4 = lapse_rate(np.log(p[-1])+self.dlnp,lnargs+k3*self.dlnp)
            
            p   = np.append(p,np.exp(np.log(p[-1])+self.dlnp))
            T   = np.append(T,max(np.exp((np.log(T[-1])+(k1[0]/6+k2[0]/3+k3[0]/3+k4[0]/6)*self.dlnp)),self.T_str))
            x_v = np.exp(np.log(x_v)+(k1[1:]/6+k2[1:]/3+k3[1:]/3+k4[1:]/6)*self.dlnp)
            x_c = x_0-x_v    
            pp  = x_v/sum(x_v)*p[-1]
        
            for i,gas in enumerate(self.gases):
                pp_dict[gas.formula] = np.append(pp_dict[gas.formula],pp[i])
                x_v_dict[gas.formula] = np.append(x_v_dict[gas.formula],x_v[i])
                x_c_dict[gas.formula] = np.append(x_c_dict[gas.formula],x_c[i])
                if x_c[i] > 0 and p[-1] > self.p_cloud[gas.formula]:
                    self.p_cloud[gas.formula] = p[-1]
                
            if T[-1] == self.T_str and p[-1] > self.p_str:
                self.p_str = p[-1]
        
        return p,T,pp_dict,x_v_dict,x_c_dict
        
class Graham2021(atmosphere):
    
    def __init__(self, species, pps, Ts, **kw):
        
        atmosphere.__init__(self, species, pps, Ts, **kw)
        
        if 'pToA' in kw:
            self.pToA = kw['pToA']
        else:
            self.pToA = 1. # [Pa]
        if 'dlnp' in kw:
            self.dlnp = -kw['dlnp']
        else:
            self.dlnp = -0.1 # [Pa]
        if 'N' in kw:
            self.N = kw['N']
        else:
            self.N = 100
        if 'T_str' in kw:
            self.T_str=kw['T_str']
        else:
            self.T_str = None
        if 'alpha' in kw:
            self.alpha = np.array([kw['alpha'][sp.formula] for sp in species])
        else:
            self.alpha = np.zeros_like(species)
            
        self.gases = np.array(species)
        self.profiles = {}
        self.profiles['partial pressure'] = {}
        self.profiles['molar fraction']   = {}
        self.profiles['mass fraction']    = {}
        self.profiles['x_v'] = {}
        self.profiles['x_c'] = {}
        
        self.update(Ts, pps)
        
    # Overload atmosphere functions:
    def updateTs(self, Ts):
        
        self.update(Ts,np.array([self.partial_pressure[sp] for sp in self.species]))
        
    def update(self, Ts, pps):
        
        self.Ts = Ts
        T,pps,x_d,x_v,x_c,LR = self.solve(self.Ts,pps)
        
        # surface values
        for gas in self.gases:
            atmosphere.updatePartialPressure(self, gas.formula, pps[gas.formula][0])
        
        # profiles
        self.profiles['pressure'] = np.logspace(np.log10(self.ps),np.log10(self.pToA),self.N)
        p = np.zeros_like(T)
        for i in range(len(T)):
            for gas in self.gases:
                p[i] += pps[gas.formula][i]
                
        self.profiles['temperature']                       = utils.interp(self.profiles['pressure'],p,T)
        self.profiles['x_d']                               = utils.interp(self.profiles['pressure'],p,x_d)
        self.profiles['lapse rate']                        = utils.interp(self.profiles['pressure'],p,LR)
        self.profiles['average molecular mass']            = np.zeros_like(self.profiles['pressure'])
        for gas in self.gases:
            self.profiles['partial pressure'][gas.formula] = utils.interp(self.profiles['pressure'],p,pps[gas.formula])
            self.profiles['x_v'][gas.formula]              = utils.interp(self.profiles['pressure'],p,x_v[gas.formula])
            self.profiles['x_c'][gas.formula]              = utils.interp(self.profiles['pressure'],p,x_c[gas.formula])
            self.profiles['molar fraction'][gas.formula]   = self.profiles['partial pressure'][gas.formula]/self.profiles['pressure']
            self.profiles['average molecular mass']       += self.profiles['partial pressure'][gas.formula]*gas.molecular_mass
        
        self.profiles['average molecular mass']           /= self.profiles['pressure']
        for gas in self.gases:
            self.profiles['mass fraction'][gas.formula]    = self.profiles['molar fraction'][gas.formula]*gas.molecular_mass/self.profiles['average molecular mass']
                    
    def solve(self,Ts,pps):
        
        air         = []
        condensable = []
        
        cp  = np.array([])
        cpc = np.array([])
        M   = np.array([])
        L   = np.array([])

        for gas in self.gases:
            cp = np.append(cp,gas.cp_mol)
            cpc= np.append(cpc,gas.cp_cond)
            M  = np.append(M,gas.molecular_mass)
            L  = np.append(L,gas.L_mol)

        # initialize ######################

        pp  = pps
        x_0  = pp/sum(pp)             # molar fraction
        x_v = np.zeros_like(x_0)      # molar fraction
        x_c = np.zeros_like(x_0)      # molar fraction
        p_d = 0.
        x_d = 0.
            
        # condensable / air
        for i,gas in enumerate(self.gases):
            if gas.psat(Ts) <= x_0[i]*sum(pps):
            #if Ts < gas.T_dew(pps[i]):
                # condensable
                air.append(False)
                condensable.append(True)
                pp[i]  = gas.psat(Ts)
                x_v[i] = pp[i]/sum(pps) # pps gives us the initial condition in terms of quantity of matter
                x_c[i] = x_0[i] - x_v[i]
                #raise Warning(gas.formula, 'condenses at the surface.')
            else:
                # air
                air.append(True)
                condensable.append(False)
                x_d   += x_0[i]
                x_v[i] = x_0[i]
                x_c[i] = 0.
        # air properties initialization
        x_d = sum(x_0[np.where(air)])
        c_d = lambda T: sum(x/x_d*cp_(T) for x,cp_ in zip(x_0[np.where(air)],cp[np.where(air)]))

        # lapse rate
        dlnT_dlnp = lambda lnp,lnT: (x_d+sum(x_v[np.where(condensable)]))\
                                   /(x_d*(c_d(np.exp(lnT))*x_d\
                                          +sum([x_v_*(cp_(np.exp(lnT))-R_gas*L_/R_gas/np.exp(lnT)+R_gas*(L_/R_gas/np.exp(lnT))**2) + x_c_*alpha_*cpc_(np.exp(lnT)) for x_v_,cp_,L_,x_c_,alpha_,cpc_ in zip(x_v[np.where(condensable)],\
                                                                                                                                                                                           cp[np.where(condensable)],\
                                                                                                                                                                                           L[np.where(condensable)],\
                                                                                                                                                                                           x_c[np.where(condensable)],\
                                                                                                                                                                                           self.alpha[np.where(condensable)],\
                                                                                                                                                                                           cpc)]))\
                                    /(R_gas*(x_d+sum([x_v_*L_/R_gas/np.exp(lnT) for x_v_,L_ in zip(x_v[np.where(condensable)],L[np.where(condensable)])])))\
                                    +sum((x_v*L/R_gas/np.exp(lnT))[np.where(condensable)]))

        T        = np.array([Ts])
        p        = np.array([sum(pp)])
        x_d_arr  = np.array([x_d])
        slope    = np.array([dlnT_dlnp(np.log(sum(pp)),np.log(Ts))])
        pp_dict  = {}
        x_v_dict = {}
        x_c_dict = {}
        for i,gas in enumerate(self.gases):
            pp_dict[gas.formula] = np.array([pp[i]])
            x_v_dict[gas.formula] = np.array([x_v[i]])
            x_c_dict[gas.formula] = np.array([x_c[i]])
            
        # iterate ######################
        while True:
            
            lnp = np.log(p[-1])
            lnT = np.log(T[-1])
            
            # R-K 4th orer coefficients
            k1 = dlnT_dlnp(lnp,            lnT)
            k2 = dlnT_dlnp(lnp+self.dlnp/2,lnT+self.dlnp*k1/2)
            k3 = dlnT_dlnp(lnp+self.dlnp/2,lnT+self.dlnp*k2/2)
            k4 = dlnT_dlnp(lnp+self.dlnp,  lnT+self.dlnp*k3)

            # R-K 4th order solution
            T = np.append(T, np.exp(np.log(T[-1])+(k1/6+k2/3+k3/3+k4/6)*self.dlnp))
            p = np.append(p, np.exp(lnp+self.dlnp))
            slope = np.append(slope,dlnT_dlnp(np.log(p[-1]),np.log(T[-1])))
            
            if self.T_str != None:
                T[-1] = max(T[-1],self.T_str)
            
            pp_prev = copy.copy(pp)
            repeat = True
            while repeat:
                qsat = np.ones_like(pp)
                for i,gas in enumerate(self.gases):
                    if condensable[i]:
                        pp[i] = gas.psat(T[-1])
                p_d = p[-1] - sum(pp[np.where(condensable)])
                #print('p','=',p[-1])
                #print('T','=',T[-1])
                for i,gas in enumerate(self.gases):
                    if air[i]:
                        pp[i] = x_0[i]/sum(x_0[np.where(air)])*p_d
                        qsat[i] = gas.psat(T[-1])/pp[i]
                        #print('qsat_'+gas.formula,'=',qsat[i])
                        #print('pp_'+gas.formula,'=',pp[i])
                        #print('p_d','=',p_d)
                        #print('psat_'+gas.formula,'=',gas.psat(T[-1]))
                    
                if min(qsat) < 1:
                    condensable[np.argmin(qsat)] = True
                    air[np.argmin(qsat)] = False
                    #print(self.gases[np.argmin(qsat)].formula,' is condensing,p=',p[-1]*1e-5,',saturation pressure is:',self.gases[np.argmin(qsat)].psat(T[-1])*1e-5,' and partial pressure is ',x_0[np.argmin(qsat)]*p[-1]*1e-5)
                else:
                    repeat = False
                    
            for i,gas in enumerate(self.gases):
                if condensable[i]:
                    #print(gas.formula)
                    x_v[i] *= sum(pp_prev[np.where(air)])/sum(pp[np.where(air)])*pp[i]/pp_prev[i]
                    x_c[i] = x_0[i] - x_v[i]
                else:
                    x_v[i] = x_0[i]
                    x_c[i] = 0.
    
            # air properties initialization
            x_d = sum(x_v[np.where(air)])
            c_d = lambda T: sum(x/x_d*cp_(T) for x,cp_ in zip(x_0[np.where(air)],cp[np.where(air)]))
            
            # partial pressures
            x_d_arr = np.append(x_d_arr,x_d)
            for i,gas in enumerate(self.gases):
                pp_dict[gas.formula] = np.append(pp_dict[gas.formula],pp[i])
                x_v_dict[gas.formula] = np.append(x_v_dict[gas.formula],x_v[i])
                x_c_dict[gas.formula] = np.append(x_c_dict[gas.formula],x_c[i])
                #print(gas.formula,':pp=',pp[i],'psat=',gas.psat(T[-1]),'x=',x[i],',eta=',eta[i])
                
            # termination test
            if p[-1]<self.pToA:
                break
        
        return T, pp_dict,x_d_arr,x_v_dict,x_c_dict,slope
    

class dlnT_dlnp_factory:
    
    def __init__(self,species,x_0,**kw):
        self.species   = np.array(species)
        self.air       = np.array(np.ones_like(species),dtype=bool)
        self.x_0       = x_0
        if 'alpha' in kw:
            self.alpha = kw['alpha']
        else:
            self.alpha = np.zeros_like(species)
        
    def __call__(self,lnp,lnT,lnx_v,**kw):
        
        R = 8.314
        
        # update air
        if 'air' in kw:
            self.air = kw['air']
        else:
            for i,sp_ in enumerate(self.species):
                if not self.air[i]:
                    continue
                elif sp_.psat(np.exp(lnT)) < np.exp(lnx_v[i])/sum(np.exp(lnx_v))*np.exp(lnp):
                    self.air[i] = False
                                    
        # equation 1 from Graham et al., 2021
        return sum(np.exp(lnx_v))\
              /(sum(np.exp(lnx_v[np.where(self.air)]))\
                *( sum([np.exp(lnx_v_)*sp_.cp_mol(np.exp(lnT)) for lnx_v_,sp_ in zip(lnx_v[np.where(self.air)],self.species[np.where(self.air)])])/sum(np.exp(lnx_v[np.where(self.air)]))\
                  +sum([np.exp(lnx_v_)*(sp_.cp_mol(np.exp(lnT))-R*sp_.L_mol/R/np.exp(lnT)+R*(sp_.L_mol/R/np.exp(lnT))**2)+alpha_*(x_0_-np.exp(lnx_v_))*sp_.cp_cond(np.exp(lnT)) for lnx_v_,sp_,alpha_,x_0_ in zip(lnx_v[np.where(~self.air)],self.species[np.where(~self.air)],self.alpha[np.where(~self.air)],self.x_0[np.where(~self.air)])]))\
                /(R*(sum(np.exp(lnx_v[np.where(self.air)]))+sum([np.exp(lnx_v_)*sp_.L_mol/R/np.exp(lnT) for lnx_v_,sp_ in zip(lnx_v[np.where(~self.air)],self.species[np.where(~self.air)])])))\
                +sum([np.exp(lnx_v_)*sp_.L_mol/R/np.exp(lnT) for lnx_v_,sp_ in zip(lnx_v[np.where(~self.air)],self.species[np.where(~self.air)])]))
                                               
class dlnx_v_dlnp_factory:
    
    def __init__(self,sp,species,dlnT_dlnp):
        self.sp        = sp
        self.species   = np.array(species)
        self.dlnT_dlnp = dlnT_dlnp
        self.air       = np.array(np.ones_like(species),dtype=bool)
        
    def __call__(self,lnp,lnT,lnx_v):
        
        R = 8.314
        
        # update air
        for i,sp_ in enumerate(self.species):
            if not self.air[i]:
                continue
            elif sp_.psat(np.exp(lnT)) < np.exp(lnx_v[i])/sum(np.exp(lnx_v))*np.exp(lnp):
                self.air[i] = False
            
        # if self species is air
        if self.air[self.species.tolist().index(self.sp)]:
            return 0.
        else:
            # equation 23 from Li et al., 2018
            return self.sp.L_mol/R/np.exp(lnT)*self.dlnT_dlnp(lnp,lnT,lnx_v) - 1. + sum([np.exp(lnx_v_)/sum(lnx_v[np.where(self.air)])*(sp_.L_mol/R/np.exp(lnT)*self.dlnT_dlnp(lnp,lnT,lnx_v) - 1.) for lnx_v_,sp_ in zip(lnx_v[np.where(~self.air)],self.species[np.where(~self.air)])])
