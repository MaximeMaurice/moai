import matplotlib.pyplot as plt
import numpy as np

def dicmul(dicA,dicB):
    return_dic = {}
    for key in dicA:
        if key in dicB:
            return_dic[key] = dicA[key]*dicB[key]
    return return_dic

def dicmulscal(dic,scal):
    for key in dic:
        dic[key] *= scal
    return dic

def dicdiv(dicA,dicB):
    return_dic = {}
    for key in dicA:
        if key in dicB:
            return_dic[key] = dicA[key]/dicB[key]
    return return_dic

def dicadd(dicA,dicB):
    return_dic = {}
    for key in dicA:
        if key in dicB:
            return_dic[key] = dicA[key]+dicB[key]
    return return_dic

def dicsub(dicA,dicB):
    return_dic = {}
    for key in dicA:
        if key in dicB:
            return_dic[key] = dicA[key]-dicB[key]
    return return_dic

def dicaddscal(dic,scal):
    for key in dic:
        dic[key] += scal
    return dic

def dicsum(dic):
    return sum([dic[key] for key in dic])

def dicprod(dic):
    return np.product([dic[key] for key in dic])

def plot_makeup(xname,yname,l=True,g=True):
    plt.xlabel(xname,fontsize=13)
    plt.xticks(fontsize=10)
    plt.ylabel(yname,fontsize=13)
    plt.yticks(fontsize=10)
    if l:
        plt.legend(fontsize=13)
    if g:
        plt.grid()

def interp(x,xp,fp,left=None,right=None,period=None):

    #if np.diff(xp)[0]<0:
    if xp[0]>xp[-1]:
        xp        = np.flipud(xp)
        fp        = np.flipud(fp)
        left_temp = left
        left      = right
        right     = left_temp

    return np.interp(x,xp,fp,left,right,period)

def float2array(var):
    
    if isinstance(var,(float,int)):
        return np.array([var])
    else:
        return var
    
def array2float(var):
    
    if len(var)==1:
        return var[0]
    else:
        return var
    
def NewtonRaphson(F,J,x_0,tol=1e-8,max_it=1000):
    
    x_n = x_0
    tol = np.ones_like(x_0) * tol
    
    for it in range(max_it):
        x_n -= np.dot(np.linalg.inv(J(x_n)),F(x_n))
        print(F(x_n))
        if (abs(F(x_n)) < tol).all():
            return x_n
            
    raise Exception('Newton-Raphson does not converge')

class dummy:
    pass

def set_panel_number(ax,label,**kw):
    try:
        logx=kw['logx']
    except KeyError:
        logx=False
    try:
        logy=kw['logy']
    except KeyError:
        logy=False
    try:
        posx=kw['posx']
    except KeyError:
        posx=0.9
    try:
        posy=kw['posy']
    except KeyError:
        posy=0.9
    try:
        fs=kw['fontsize']
    except KeyError:
        fs=13
    if not (logx or logy):
        plt.annotate(label,(ax.get_xlim()[0]+posx*(ax.get_xlim()[1]-ax.get_xlim()[0]),
                           ax.get_ylim()[0]+posy*(ax.get_ylim()[1]-ax.get_ylim()[0])),
                     fontsize=fs)
    elif logy and not logx:
        plt.annotate(label,(ax.get_xlim()[0]+posx*(ax.get_xlim()[1]-ax.get_xlim()[0]),
                            10**(np.log10(ax.get_ylim()[0])+posy*(np.log10(ax.get_ylim()[1])-np.log10(ax.get_ylim()[0])))),
                     fontsize=fs)
    elif logx and not logy:
        plt.annotate(label,(10**(np.log10(ax.get_xlim()[0])+posx*(np.log10(ax.get_xlim()[1])-np.log10(ax.get_xlim()[0]))),
                            ax.get_ylim()[0]+posy*(ax.get_ylim()[1]-ax.get_ylim()[0])),
                     fontsize=fs)
    else:
        plt.annotate(label,(10**(np.log10(ax.get_xlim()[0])+posx*(np.log10(ax.get_xlim()[1])-np.log10(ax.get_xlim()[0]))),
                            10**(np.log10(ax.get_ylim()[0])+posy*(np.log10(ax.get_ylim()[1])-np.log10(ax.get_ylim()[0])))),
                     fontsize=fs)

# Utility
y2s = 3600.*24.*365. # years to seconds conversion [yr/s]
s2y = 1./y2s # seconds to years conversion [s/yr]
T_offset = {} # offset from SI (K)
T_offset['C'] = -273.15
T_offset['K'] = 0.
d_factor = {}
d_factor['m'] = 1.
d_factor['km'] = 0.001
p_factor = {}
p_factor['Pa'] = 1.
p_factor['GPa'] = 1e-9
