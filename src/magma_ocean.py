import numpy as np
import matplotlib.pyplot as plt
import copy
import utils
import functools

from physics.eos import eos_book
from physics.constants import Earth_surface_radius,Earth_gravity
from chemistry import elements,molecules,equilibria
import time as clock
from scipy.optimize import root_scalar
from scipy.interpolate import interp1d
from speciation import spec_iter
from atmospheres import *
from tools import *


# super class magma ocean implements equilibrium crystallization
class magma_ocean:

    def timed(channel):
        def timed(fun):
            @functools.wraps(fun)
            def wrapper(*args,**kwargs):
                start = clock.time()
                val = fun(*args,**kwargs)
                args[0].stopwatch[channel] += clock.time()-start
                return val
            return wrapper
        return timed

    def __init__(self, T_pot, p_CMB, eos={'rho':'cst','alpha':'cst','cp':'cst'}, **kw):

        if 'N' in kw:
            self.N       = kw['N']
        else:
            self.N       = 100
        if 'g' in kw:
            self.gravity = kw['g']
        else:
            self.gravity = Earth_gravity
        if 'R' in kw:
            self.R_out   = kw['R']
        else:
            self.R_out   = Earth_surface_radius
        if 't' in kw:
            self.t = kw['t']
        else:
            self.t = 0.
        if 'p_surf' in kw:
            self.p_surf = kw['p_surf']
        else:
            self.p_surf = 1. # [Pa]
        if 'ConvCum' in kw:
            self.convecting_cumulates = kw['ConvCum']
        else:
            self.convecting_cumulates = True
            
        self.profiles        = {}
        self.averages        = {}
        self.scalar          = {}
        
        # Timing
        self.stopwatch = {'speciation':0,
                          'fractionation':0,
                          'flux minimization':0,
                          'integration':0,
                          'parametrization':0}
        
        # Tendencies
        self.tendencies = {'Tpot':0.}
        self.max_change = {'Tpot':1.}
        
        self.adaptive_timestep = True
        
        # variables to path dictionnary (get appened each time you set a new parametrization or add a new species
        self.var2path = {'temperatures':'self.profiles[\'temperatures\']',
                         'pressures'   :'self.profiles[\'pressures\']',
                         'rho'         :'self.profiles[\'rho\']',
                         'alpha'       :'self.profiles[\'alpha\']',
                         'cp'          :'self.profiles[\'cp\']',
                         'radius'      :'self.radii',
                         'depth'       :'self.depths',
                         'T_pot'       :'self.adiabat.T_pot',
                         'T_sfc'       :'self.atm.Ts',
                         'p_CMB'       :'self.p_CMB',
                         'p_bot'       :'self.p_bot',
                         'M_sys'       :'self.M_system',
                         'p_sfc'       :'self.p_surf',
                         'M_sys'       :'self.M_system',
                         'M_mantle'    :'self.M_mantle',
                         'H_int'       :'self.H_int',
                         't'           :'self.t',
                         'dt'          :'self.dt',
                         'atm'         :'self.atm',
                         'LE'          :'self.LE',
                         'BL'          :'self.BL',
                         'MO'          :'self'}
            
        # Parametrization
        self.param_channel               = {'eos':[],
                                            'pre-update':[],
                                            'pre-frac':[],
                                            'pre-spec':[],
                                            'post-spec':[],
                                            'time-dep':[],
                                            'internal-heating':[]}
        self.with_mc                     = False
        self.parametrizations            = {}
        self.parametrizations_variables  = {}
        self.parametrization_types       = {'profile':[],'scalar':[],'action':[]}
        
        # Equations of state
        self.setParametrization('alpha',
                                lambda var:eos['alpha'](var['pressures'],var['temperatures']),
                                ['pressures','temperatures'],
                                ptype='profile',
                                update_state=False)
        self.setParametrization('cp',
                                lambda var:eos['cp'](var['pressures'],var['temperatures']),
                                ['pressures','temperatures'],
                                ptype='profile',
                                update_state=False)
        self.setParametrization('rho',
                                lambda var:eos['rho'](var['pressures'],var['temperatures']),
                                ['pressures','temperatures'],
                                ptype='profile',
                                update_state=False)
        
        self.param_channel['eos'] = ['rho','cp','alpha']
        self.param_channel['pre-spec'] = [] # eos have been initialized in pr-spec channel to avoid calculating the thermal structure, need to override it
        
        # Trace elements
        self.species                = []
        self.to_frac                = []
        self.to_spec                = []
        self.speciate               = False
        self.content                = {}
        self.content['bulk']        = {}
        self.content['system']      = {}
        self.content['liquid']      = {}
        self.content['solid']       = {}
        self.content['precipitate'] = {}
        self.check_sat              = False
        
        # Boundary Layer
        self.BL           = None
        
        # Atmosphere
        self.atm          = None

        # Adiabat
        dT_dp            = lambda p,T: self.parametrizations['alpha']({'pressures':p,'temperatures':T})\
                                        /(self.parametrizations['rho']({'pressures':p,'temperatures':T})\
                                          *self.parametrizations['cp']({'pressures':p,'temperatures':T}))*T
        
        self.adiabat      = adiabat(T_pot, 0., 2*p_CMB, dT_dp, False, 2*self.N)
        
        # internal heating
        self.H_int = 0

        # Lithostatic Equilibrium
        dz_dp             = lambda p,z: 1./(self.parametrizations['rho']({'pressures':p,'temperatures':self.adiabat.getT(p)})*self.gravity)
        self.LE           = lithostatic_equilibrium(0., 2*p_CMB, dz_dp, log=False, N=2*self.N)

        # Pressures (and Geometry)
        self.setCMBpressure(p_CMB)
        self.p_bot = p_CMB
        self.R_int = self.LE.getz(p_CMB)

        # Profiles
        self.profiles['temperatures'] = self.adiabat.getT(self.profiles['pressures'])
        self.updateInterior(propagate=True,fractionate=[],speciate=[])
        
        # Mass
        self.M_mantle  = self.getIntegral(np.ones_like(self.radii),mass_weighted=True)
        self.M_system  = self.M_mantle
        self.dM_system = 0.
        
        self.max_dT = 1. # [K]
        self.dt     = 0. # [s]
        self.min_T_surf = 1000 # [K]

        # Numerical
        self.flux_minimization_method = 'brentq'

    def getRa_star(self,domain):
        # Ra_star is Ra/DeltaT, so
        # it only depends on values
        # defined in the magma ocean class.
        
        alpha = self.getAverage(self.profiles['alpha'],domain=domain,mean='arithmetic')
        rho   = self.getAverage(self.profiles['rho'],domain=domain,mean='arithmetic')
        k     = self.getAverage(self.profiles['k'],domain=domain,mean='arithmetic')
        cp    = self.getAverage(self.profiles['cp'],domain=domain,mean='arithmetic')
        eta   = self.getAverage(self.profiles['viscosity'],domain=domain,mean='harmonic')
        if domain == 'liquid':
            D = self.LE.getz(self.p_bot)
        elif domain == 'solid':
            D = self.LE.getz(self.p_CMB)-self.LE.getz(self.p_bot)
            
        return alpha*rho*self.gravity*D**3/(k/rho/cp)/eta

    def getRa(self,domain):
        
        if domain == 'liquid':
            Delta_T = self.BL.Delta_T
        elif domain == 'solid':
            Delta_T = self.adiabat.getT(self.p_CMB)-self.adiabat.getT(self.p_bot)
        
        return self.getRa_star('liquid')*Delta_T
        
    def getPr(self,domain):

        return self.averages['viscosity']/(self.averages['k']/(self.averages['rho']*self.averages['cp'])*self.averages['rho'])
    
    def setCMBdepth(self,z_CMB):
        
        self.p_CMB  = self.LE.getp(z_CMB)
        self.updatePressures()
    
    def setCMBpressure(self,p_CMB):
        
        self.p_CMB  = p_CMB
        self.adiabat.updateP(self.p_surf,self.p_CMB)
        self.updatePressures()
        
    def adjustPressures(self):
        # shouldn't be called in general
        self.profiles['pressures'] = self.LE.getp(self.depths)

    def updatePressures(self):
        self.profiles['pressures'] = np.linspace(self.p_CMB,self.p_surf,self.N) # set by lithostatic equilibrium
        self.updateDepths()
        
    def updateDepths(self,propagate=True):
        
        self.depths = self.LE.getz(self.profiles['pressures'])
        if propagate:
            self.updateRadii(False)
        
    def updateRadii(self,propagate=True):
        
        self.radii = self.R_out-self.depths
        self.dr    = np.diff(np.append(self.radii[0],np.append((self.radii[1:]+self.radii[:-1])*0.5,self.radii[-1]))) # useful for integration
        if propagate:
            self.updateDepths(False)
        
    def updateInterior(self, **kw):
        
        try: propagate      = kw['propagate']
        except: propagate   = True
        try: fractionate    = kw['fractionate']
        except: fractionate = self.to_frac
            
        # Update temperatures       
        if self.convecting_cumulates:
            # the temperature profiles follows an adiabat throughout the mantle
            self.profiles['temperatures'] = self.adiabat.getT(self.profiles['pressures'])
        else:
            # the temperature profile follows an adiabat throughout the magma ocean only
            self.profiles['temperatures'][np.where(self.profiles['pressures']<=self.p_bot)] = self.adiabat.getT(self.profiles['pressures'][np.where(self.profiles['pressures']<=self.p_bot)])
        # Update core parameters
        for param in self.param_channel['eos']:
            self.updateParam(param)
            
        if self.with_mc:
            
            # Update parametrizations in the pre-update channel
            for param in self.param_channel['pre-update']:
                self.updateParam(param)
            
            # update system mass (from mc lookup)
            self.dM_system = self.M_system - utils.interp(self.adiabat.T_pot,self.T_pot_lookup,self.M_sys_lookup)
            self.M_system -= self.dM_system

            # update each fractionated or speciated species mass in the system
            for sp_name in np.append(self.to_frac,self.to_spec):
                self.content['system'][sp_name] = ((self.M_system + self.dM_system) * self.content['system'][sp_name] - self.dM_system * (self.mc.RCMF * self.content['liquid'][sp_name] + (1.-self.mc.RCMF) * self.content['solid'][sp_name])) / self.M_system

            # Ideally M_solid and M_liquid would be computed similarly in both equilibrium and fractional crystallization cases.
            # However, due to interpolation of the melt fraction profiles, doing so gives non-zero M_solid in fractional crystallization
            self.M_liquid  = self.M_system * self.getAverage(self.profiles['phi'],mass_weighted=True,domain='liquid')      + self.mc.RCMF*self.dM_system
            self.M_solid   = self.M_system * self.getAverage((1.-self.profiles['phi']),mass_weighted=True,domain='liquid') + (1.-self.mc.RCMF)*self.dM_system
            
            # Update parametrizations in the pre-frac channel
            for param in self.param_channel['pre-frac']:
                self.updateParam(param)
                
            # Fractionation (requires updated M_liquid and M_solid)
            if self.with_mc and len(fractionate) != 0:
                self.fractionation(fractionate)
        
        # Update parametrizations in the pre-spec channel
        for param in self.param_channel['pre-spec']:
            self.updateParam(param)
            
        # Speciation (requires updated M_liquid, M_solid and parametrization: fO2)
        if self.with_mc and self.speciate: # len(speciate['equilibria']) != 0:
            self.speciationStep()
            #try:
            #    self.speciationStep()
            #except:
            #    self.speciationInit()
    
        # Update parametrizations in the post-spec channel
        for param in self.param_channel['post-spec']:
            self.updateParam(param)
                
        # Calculate averages
        if propagate:
            self.updateAverages()
            
    # All normal parametrizations are called only via updateParam
    @timed('parametrization')
    def updateParam(self,name='all'):
        
        
        if name=='all':
            for call_name in self.parametrizations:
                self.updateParam(call_name)
            return
                
        # scalar parametrizations
        if name in self.parametrization_types['scalar']:
            variables = {'MO':self}
            for var in self.parametrizations_variables[name]:
                exec('variables[var] = '+self.var2path[var])
            self.scalar[name] = self.parametrizations[name](variables)
            
        # profile parametrization
        elif name in self.parametrization_types['profile']:
            variables = {'MO':self}
            for var in self.parametrizations_variables[name]:
                exec('variables[var] = '+self.var2path[var])
            self.profiles[name] = self.parametrizations[name](variables)
            
        # action parametrization
        elif name in self.parametrization_types['action']:
            variables = {'MO':self}
            for var in self.parametrizations_variables[name]:
                exec('variables[var] = '+self.var2path[var])
            self.parametrizations[name](variables)
            
                
    def updateAverages(self):

        for param in self.profiles:
                
            # viscosity (requires harmonic mean for equilibrium crystallization)
            if param=='viscosity':
                self.averages[param] = self.getAverage(self.profiles[param],mean='harmonic')
                
            # other profiles
            else:
                self.averages[param] = self.getAverage(self.profiles[param],mean='arithmetic')
                            

    def updateT_pot(self, new_T_pot, **kw):
        
        self.adiabat.updateT_pot(new_T_pot)
        
        if self.with_mc:
            self.updatep_bot()
            
        self.updateInterior(**kw)

    def updatep_bot(self):
        
        self.p_bot = self.getp_bot(self.adiabat.T_pot)
        self.p_liq = self.getp_liq(self.adiabat.T_pot)

    def setParametrization(self, name, equation, variables, ptype, **kw):
        
        #try:
        #    self.parametrization_is_profile[name] = kw['is_profile']
        #except:
        #    self.parametrization_is_profile[name] = True
        try:
            self.param_channel[kw['channel']].append(name)
        except:
            self.param_channel['pre-spec'].append(name) # the default channel is pre-spec
        try:
            update_state = kw['update_state']
        except:
            update_state = True
            
        if not ptype in ['action','scalar','profile']:
            raise Exception('Param type',ptype,'not valid.')
        else:
            self.parametrization_types[ptype].append(name)
            
        self.parametrizations[name]           = equation
        self.parametrizations_variables[name] = variables
        
        if ptype == 'profile':
            self.var2path[name+'_avg'] = 'self.averages[\''+name+'\']'
            self.var2path[name]        = 'self.profiles[\''+name+'\']'
            self.var2path[name+'_sfc'] = 'self.profiles[\''+name+'\'][-1]'
        elif ptype == 'scalar':
            self.var2path[name]        = 'self.scalar[\''+name+'\']'
            
        #self.var2path[name+'_avg']            = 'self.averages[\''+name+'\']'
        #if self.parametrization_is_profile[name]:
        #    self.var2path[name]               = 'self.profiles[\''+name+'\']'
        #    self.var2path[name+'_sfc']        = 'self.profiles[\''+name+'\'][-1]'
        #else:
        #    self.var2path[name]               = 'self.averages[\''+name+'\']'
            
        if name in self.param_channel['eos']:
            if name == 'rho':
                # Update the lithostatic equilibrium when you change the density
                self.LE.dz_dp             = lambda p,z: 1./(self.parametrizations['rho']({'pressures':p,'temperatures':self.adiabat.getT(p)})*self.gravity)
                self.LE.update_lookup()
                self.updateInterior()
                self.M_mantle = self.getIntegral(np.ones_like(self.radii),mass_weighted=True)
                #if not self.mc == None:
                #    raise Warning('equation of state modified after setting melting curves')
            
            # Update the adiabat
            self.adiabat.dT_dp = lambda p,T: self.parametrizations['alpha']({'pressures':p,'temperatures':T})\
                                            /(self.parametrizations['rho']({'pressures':p,'temperatures':T})\
                                             *self.parametrizations['cp']({'pressures':p,'temperatures':T}))*T
            self.adiabat.updateLookup()
            
        if update_state:
            self.updateInterior()
            
    def setupChemistry(self,initial_values=None):
        for page in molecules.molecules_book:
            sp = molecules.molecules_book[page]
            if  sp in [molecules.O2,molecules.HCl]:
                continue
            else:
                self.addSpecies([sp],[0],True)
        if initial_values != None:
            for sp in initial_values:
                self.setContent(sp,initial_values[sp])
                
    def setContent(self,sp,new_value,reg_elems=True):
        
        old_value = self.content['system'][sp.formula]
        self.content['system'][sp.formula] = new_value
        self.content['bulk'][sp.formula]   = new_value
        if reg_elems:
            for el in sp.coefs:
                if sp.coefs[el] == 0 or el == elements.O:
                    continue
                else:
                    self.content['system'][el.symbol] += (new_value-old_value)*sp.coefs[el]*el.atomic_mass/sp.molecular_mass
                    self.content['bulk'][el.symbol]   += (new_value-old_value)*sp.coefs[el]*el.atomic_mass/sp.molecular_mass
        
    def addSpecies(self,species, bulk, volatile=False, reg_elems=True):
        
        if volatile:
            for i,sp in enumerate(species):
                if sp.name in self.species:
                    raise Exception(sp.name,' already registered')
                else:
                    self.atm.addSpecies(sp,1.)
            
        for i,sp in enumerate(species):
            sp.HPE      = False
            sp.volatile = volatile
            if not sp.name in self.species:
                self.species.append(sp.name)
            self.content['bulk'][sp.name]   = bulk[i]
            self.content['system'][sp.name] = bulk[i]
            self.content['liquid'][sp.name] = bulk[i]
            self.content['solid'][sp.name]  = bulk[i]
            
            self.var2path[sp.name+'_bulk']   = 'self.content[\'bulk\'][\''+sp.name+'\']'
            self.var2path[sp.name+'_system'] = 'self.content[\'system\'][\''+sp.name+'\']'
            self.var2path[sp.name+'_liquid'] = 'self.content[\'liquid\'][\''+sp.name+'\']'
            self.var2path[sp.name+'_solid']  = 'self.content[\'solid\'][\''+sp.name+'\']'
            if sp.volatile:
                self.var2path['p_'+sp.name] = 'self.atm.partial_pressure[\''+sp.name+'\']'
        
            if isinstance(sp,molecules.molecule) and reg_elems:
                for el in sp.coefs:
                    if sp.coefs[el] == 0 or el == elements.O:
                        continue
                    if not el.name in self.species:
                        self.addSpecies([el],[bulk[i]*sp.coefs[el]*el.atomic_mass/sp.molecular_mass])
                    else:
                        self.content['bulk'][el.name]   += self.content['bulk'][sp.name]*sp.coefs[el]*el.atomic_mass/sp.molecular_mass
                        self.content['system'][el.name] += self.content['system'][sp.name]*sp.coefs[el]*el.atomic_mass/sp.molecular_mass
                
        
    def getElementMass(self,name):
        
        elem = elements.elems_book[name]
        mass = 0
        for sp_name in self.species:
            if sp_name in elements.elems_book:
                continue
            else:
                sp = molecules.molecules_book[sp_name]
            if not elem in sp.coefs:
                continue
            else:
                mass += (self.content['liquid'][sp.name] * self.M_liquid + self.content['solid'][sp.name] * self.M_solid + self.atm.getMass(sp.name)) * sp.coefs[elem] * elem.atomic_mass / sp.molecular_mass
                
        return mass
        
    def getMass(self,name):
        
        if name in elements.elems_book:
            elem = elements.elems_book[name]
            bulk = 0. # self.M_liquid*self.content['liquid'][elem.symbol] + self.M_solid*self.content['solid'][elem.symbol]
            for sp in self.atm.species:
                sp = molecules.molecules_book[sp]
                if not elem in sp.coefs:
                    continue
                bulk += (self.content['liquid'][sp.formula]*self.M_liquid+self.content['solid'][sp.formula]*self.M_solid) * sp.coefs[elem] * elem.atomic_mass / sp.molecular_mass
                bulk += self.atm.getMass(sp.formula) * sp.coefs[elem] * elem.atomic_mass / sp.molecular_mass
            return bulk
        else:
            sp = molecules.molecules_book[name]
            return self.M_liquid*self.content['liquid'][sp.formula] + self.M_solid*self.content['solid'][sp.formula] + self.atm.getMass(sp.formula)
        
    def getDissolvedMass(self,elem):
        
        if isinstance(elem,elements.element):
            bulk = 0. # self.M_liquid*self.content['liquid'][elem.symbol] + self.M_solid*self.content['solid'][elem.symbol]
            for sp in self.atm.species:
                sp = molecules.molecules_book[sp]
                if not elem in sp.coefs:
                    continue
                bulk += (self.content['liquid'][sp.formula]*self.M_liquid+self.content['solid'][sp.formula]*self.M_solid) * sp.coefs[elem] * elem.atomic_mass / sp.molecular_mass
            return bulk
        else:
            return self.M_liquid*self.content['liquid'][elem.formula] + self.M_solid*self.content['solid'][elem.formula] + self.atm.getMass(elem.formula)
     
    @timed('fractionation')
    def fractionation(self, species_list):
        
        species = []
        for sp_name in species_list:
            try:
                species.append(elements.elems_book[sp_name])
            except:
                species.append(molecules.molecules_book[sp_name])
        
        # refractory ##########################################################
        for sp in species:
            if sp.volatile:
                continue
            else:
                self.content['liquid'][sp.name] = self.content['system'][sp.name] * self.M_system / (self.M_liquid + self.M_solid * sp.part_coef)
                self.content['solid'][sp.name]  = self.content['liquid'][sp.name] * sp.part_coef
                
        if self.atm == None:
            return
        
        # volatiles ###########################################################
        S_red  = 4. * np.pi * self.R_out**2 / self.gravity
        mu_res = 1
        while mu_res > 1e-10:
            mu_avg_prev = self.atm.average_molecular_mass
            ptot = self.atm.ps
            for sp in species:
                if not sp.volatile:
                    continue
                else:
                    #M_eff       = self.M_system * self.profiles['phi'][-1]
                    M_eff       = self.M_solid * sp.part_coef + self.M_liquid
                    p = root_scalar(lambda pp: M_eff*sp.Henry_law(pp,ptot) + S_red*sp.molecular_mass/self.atm.average_molecular_mass*pp - self.content['system'][sp.name] * self.M_system,\
                                    method='bisect',bracket=(1.,1e10),xtol=1e-15,rtol=1e-15).root
                    self.atm.updatePartialPressure(sp.name,p)    
                    self.content['liquid'][sp.name] = sp.Henry_law(p,ptot)
                    self.content['solid'][sp.name]  = self.content['liquid'][sp.name] * sp.part_coef
                    for param in self.parametrizations:
                        if 'p_'+sp.name in self.parametrizations_variables[param]:
                            self.updateParam(param)
                
            mu_res = abs(mu_avg_prev-self.atm.average_molecular_mass)
            
    def getSpecState(self):
        
        return {'M_bulk':{el:self.content['system'][el]*self.M_system for el in self.to_spec},
                'M_MO':self.M_liquid,
                'M_sol':self.M_solid,
                'R':self.R_out,
                'g':self.gravity,
                'T':self.adiabat.T_pot,
                'DeltaIW':self.scalar['DeltaIW_sfc']}
            
    def speciationInit(self):
        
        self.to_spec = ['H','C','N','S']    
        self.speciate = True
        
        pp = spec_iter(self.getSpecState())
        
        self.spec_state = self.getSpecState()
        self.spec_state['pp0'] = pp
                
        # TODO: check for saturation
        
        # Update
        for sp_name in pp:
            try:
                # We might want to not include some species in the atmosphere
                self.atm.updatePartialPressure(sp_name,pp[sp_name])
            except:
                pass
        #    self.content['precipitate'][sp.name] = Mp[sp]
        
        for el_name in self.to_spec:
            el = elements.elems_book[el_name]
            self.content['liquid'][el_name] = 0
            self.content['solid'][el_name]  = 0
            
            for sp_name in pp:
                sp = molecules.molecules_book[sp_name]
                
                if sp.coefs[el] != 0:
                        
                    self.content['liquid'][sp_name]  = sp.Henry_law(self.atm.partial_pressure[sp_name],self.atm.ps)
                    self.content['liquid'][el_name] += self.content['liquid'][sp_name] * abs(sp.coefs[el]) * el.atomic_mass / sp.molecular_mass
                    self.content['solid'][sp_name]   = self.content['liquid'][sp_name] * sp.part_coef
                    self.content['solid'][el_name]  += self.content['solid'][sp_name]  * abs(sp.coefs[el]) * el.atomic_mass /sp.molecular_mass
     
    @timed('speciation')
    def speciationStep(self,N_step=2):
        
        pp0           = self.atm.partial_pressure
        current_state = self.getSpecState()
        
        try:
            pp = spec_iter(current_state,self.spec_state,N_step)
        except SpeciationSolverFailed:
            try:
                pp = spec_iter(current_state,self.spec_state,10*N_step)
            except SpeciationSolverFailed:
                pp = spec_iter(current_state,self.spec_state,100*N_step)
                
        self.spec_state = current_state
        self.spec_state['pp0'] = pp
                
        # TODO: check for saturation
        
        # Update
        for sp_name in pp:
            self.atm.updatePartialPressure(sp_name,pp[sp_name])
        #    self.content['precipitate'][sp.name] = Mp[sp]
        
        for el_name in self.to_spec:
            el = elements.elems_book[el_name]
            self.content['liquid'][el_name] = 0
            self.content['solid'][el_name]  = 0
            
            for sp_name in pp:
                sp = molecules.molecules_book[sp_name]
                
                if sp.coefs[el] != 0:
                        
                    self.content['liquid'][sp_name]  = sp.Henry_law(self.atm.partial_pressure[sp_name],self.atm.ps)
                    self.content['liquid'][el_name] += self.content['liquid'][sp_name] * abs(sp.coefs[el]) * el.atomic_mass / sp.molecular_mass
                    self.content['solid'][sp_name]   = self.content['liquid'][sp_name] * sp.part_coef
                    self.content['solid'][el_name]  += self.content['solid'][sp_name]  * abs(sp.coefs[el]) * el.atomic_mass /sp.molecular_mass
         
    def speciation(self, eqs_list, elements_list, **kw):
        
        eqs= []
        for eq_exp in eqs_list:
            eqs.append(equilibria.equilibria_book[eq_exp])
        elems = []
        for el_sym in elements_list:
            elems.append(elements.elems_book[el_sym])
            
        try:
            pp = speciation({el:self.content['system'][el.symbol]*self.M_system for el in elems},
                            self.M_solid,
                            self.M_liquid,
                            4*np.pi*self.R_out**2/self.gravity,
                            eqs,
                            {molecules.molecules_book[sp]:self.atm.partial_pressure[sp] for sp in self.atm.species})
        except SpeciationSolverFailed:
            par = {el:self.content['system'][el.symbol]*self.M_system for el in elems}
            x0  = {molecules.molecules_book[sp]:self.atm.partial_pressure[sp] for sp in self.atm.species}
            par.update({'Msol':self.M_solid,'Mliq':self.M_liquid,'Sred':4*np.pi*self.R_out**2/self.gravity})
            par.update({eq:eq.constant for eq in eqs})
            try:
                pp = spec_iter(par,x0=x0)
            except SpeciationSolverFailed:
                pp = spec_iter(par,x0=x0,n_step=100)
        
        Mp = {sp:0 for sp in pp}
        if self.check_sat:
            # Check for saturation
            sat       = {}    # saturation levels
            saturated = False # saturation switch
            for sp in pp:
                if sp.formula+'_sat' in self.profiles:
                    X_sp = sp.alpha*pp[sp]**sp.beta # calculated dissolved content
                    if X_sp > min(self.profiles[sp.formula+'_sat']):
                        # sp saturates!
                        saturated = True
                        sat[sp] = min(self.profiles[sp.formula+'_sat'])  # saturation value
                        Mp[sp]  = (1-sat[sp]/X_sp)*self.M_system # estimate precipitated mass
                    else:
                        # sp under-satrated
                        sat[sp] = 1
                        Mp[sp]  = 0
                else:
                    Mp[sp] = 0
                    sat[sp] = 1
            if saturated:
                pp,Mp = speciation_saturation({el:self.content['system'][el.symbol]*self.M_system for el in elems},
                                              self.M_solid,
                                              self.M_liquid,
                                              4*np.pi*self.R_out**2/self.gravity,
                                              eqs,
                                              sat,
                                              pp,
                                              Mp)
        
        # Update
        for sp in pp:
            self.atm.updatePartialPressure(sp.name,abs(pp[sp]))
            self.content['precipitate'][sp.name] = Mp[sp]
        
        for el in elems:
            self.content['liquid'][el.name] = 0
            self.content['solid'][el.name]  = 0
            already_counted = []
            
            for eq in eqs:
                
                for sp in eq.coefs:
                    
                    if eq.coefs[sp] == 0:
                        continue
                    
                    elif sp.coefs[el] != 0:
                        
                        if sp.name in already_counted:
                            continue
                        else:
                            already_counted.append(sp.name)
                            
                        self.content['liquid'][sp.name]  = sp.Henry_law(self.atm.partial_pressure[sp.name],self.atm.ps)
                        self.content['liquid'][el.name] += self.content['liquid'][sp.name] * abs(sp.coefs[el]) * el.atomic_mass / sp.molecular_mass
                        self.content['solid'][sp.name]   = self.content['liquid'][sp.name] * sp.part_coef
                        self.content['solid'][el.name]  += self.content['solid'][sp.name]  * abs(sp.coefs[el]) * el.atomic_mass / sp.molecular_mass
                  
        
    def saveMeltingCurves(self,file_name):
        
        assert self.mc != None
        np.savetxt(file_name,np.transpose((self.T_pot_lookup,self.p_bot_lookup,self.M_sys_lookup,self.dM_sol_dT_pot,self.p_liq_lookup)))


    def setMeltingCurves(self,mc,**kw):
       
        try:
            p_bot0 = kw['p_bot']
            p_bot  = kw['p_bot']
        except:
            p_bot0 = self.p_CMB
            p_bot  = self.p_CMB
        try:
            RCMF = kw['RCMF']
        except:
            RCMF = 0.4 # [-] default value [Solomatov 2000,2007,2015]
        try:
            data_mc           = np.genfromtxt(kw['file'])
            self.T_pot_lookup = data_mc[:,0]
            self.p_bot_lookup = data_mc[:,1]
            self.M_sys_lookup = data_mc[:,2]
            self.dM_sol_dT_pot  = data_mc[:,3]
            self.p_liq_lookup = data_mc[:,4]
            self.mc      = mc
            self.mc.RCMF           = RCMF
        except:
            print('No file found: calculating lookups, be patient it can take some time!')
            self.mc      = mc
            self.mc.RCMF = RCMF
            T_pot        = self.adiabat.T_pot
            bottom_up_ad = adiabat(mc.RCMF*mc.getTliq(p_bot0)+(1-mc.RCMF)*mc.getTsol(p_bot0),p_bot0,self.p_surf,self.adiabat.dT_dp)
            self.updateT_pot(float(bottom_up_ad.getT(self.p_surf)),fractionate=[],speciate={'species':[],'equilibria':[]})
            # adiabat.getT uses np.interp1d, which returns a np.array even if the size is 1, and then updateT?pot is confused,
            # so enforcing the value to be a float is a quick and dirty hack.
        
            # Utility values
            # phi_rho is the integral of the mass melt fraction. It is used to compute the latent heat term.
            #         Notice that L is in J/[kg of crystallizing melt], so no conflict with solids being denser than melt.
            # phi_sfc is the surface volume melt fraction. It is used to end the scan (when it reaches the RCMF).
            phi_rho_int = np.array([self.getIntegral(mc.getMeltFraction(self.profiles['pressures'],self.profiles['temperatures']),mass_weighted=True,domain=[p_bot0,self.p_surf])])
            phi_sfc     = mc.getMeltFraction(self.profiles['pressures'][-1],self.profiles['temperatures'][-1])
             
            # Lookups
            self.T_pot_lookup = np.array([self.adiabat.T_pot])
            self.p_bot_lookup = np.array([p_bot0])
            self.p_liq_lookup = np.array([p_bot0])
            self.M_sys_lookup = np.array([self.getIntegral(np.ones_like(self.radii),mass_weighted=True,domain=[p_bot0,self.p_surf])])
        
            while phi_sfc > mc.RCMF:
                
                self.updateT_pot(self.adiabat.T_pot-1,fractionate=[],speciate={'species':[],'equilibria':[]})
                
                # Integral melt mass fraction
                phi_rho_int = np.append(phi_rho_int,self.getIntegral(mc.getMeltFraction(self.profiles['pressures'],self.profiles['temperatures']),mass_weighted=True,domain=[p_bot,self.p_surf]))
                  		        
                # Surface melt fraction
                phi_sfc   = mc.getMeltFraction(self.profiles['pressures'][-1],self.profiles['temperatures'][-1])
                  		
                # Write lookups
                self.T_pot_lookup = np.append(self.T_pot_lookup, self.adiabat.T_pot)
                self.M_sys_lookup = np.append(self.M_sys_lookup,self.getIntegral(np.ones_like(self.radii),mass_weighted=True,domain=[p_bot,self.p_surf]))
                self.p_liq_lookup = np.append(self.p_liq_lookup, utils.interp(0.,utils.interp(self.profiles['pressures'],mc.p_lookup,mc.liquidus)-self.profiles['temperatures'],self.profiles['pressures']))
                
                # MO bottom pressure
                p_bot = min(utils.interp(mc.RCMF,mc.getMeltFraction(self.profiles['pressures'],self.profiles['temperatures']),self.profiles['pressures']),p_bot0)
                self.p_bot_lookup = np.append(self.p_bot_lookup, p_bot)
            
            # Reset initial state
            self.updateT_pot(T_pot,fractionate=[],speciate={'species':[],'equilibria':[]})
            self.dM_sol_dT_pot = np.gradient(phi_rho_int,self.T_pot_lookup)
        
        self.getp_bot     = interp1d(self.T_pot_lookup,self.p_bot_lookup,bounds_error=False,fill_value=(0,self.p_CMB))
        self.getp_liq     = interp1d(self.T_pot_lookup,self.p_liq_lookup,bounds_error=False,fill_value=(0,self.p_CMB))
        
        self.updatep_bot()
        
        self.with_mc           = True
        self.var2path['p_liq'] = 'self.p_liq'
        self.var2path['M_liq'] = 'self.M_liquid'
        self.var2path['M_sol'] = 'self.M_solid'
            
        getMeltFrac = lambda var: self.mc.getMeltFraction(var['pressures'],var['temperatures'])
        self.setParametrization('phi', getMeltFrac, ['temperatures','pressures'], ptype='profile', channel='pre-update')
        self.setParametrization('MO_depth', lambda var: var['LE'].getz(var['p_bot']), ['LE','p_bot'], ptype='scalar')

    def addBoundaryLayer(self,DeltaT=None):

        if self.atm == None and DeltaT == None:
            raise Exception('addBoundaryLayer: No atmosphere and no delta T given')
        elif DeltaT == None:
            DeltaT = self.adiabat.T_pot - self.atm.Ts
            
        self.BL = boundary_layer(DeltaT,self.getRa_star('liquid')*DeltaT,self.LE.getz(self.p_bot),self.averages['k'],Pr=self.getPr('liquid'))
        self.setParametrization('Ra_star', lambda var: var['MO'].getRa_star('liquid'), ['MO'], ptype='scalar')
        self.setParametrization('Pr', lambda var: var['MO'].getPr('liquid'), ['MO'], ptype='scalar')
        self.setParametrization('updateBL', lambda var: var['BL'].update(Ra_star=var['Ra_star'],D=var['MO_depth']), ['BL','Ra_star','MO_depth'], ptype='action')
        
    @timed('integration')
    def getIntegral(self,profile,**kw):
            

        try: domain          = kw['domain']
        except: domain       = 'bulk'
        try: mass_weighted    = kw['mass_weighted']
        except: mass_weighted = False
        if domain == 'liquid':
            r       = np.linspace(self.R_out-self.LE.getz(self.p_bot),self.R_out)
            dr      = np.diff(np.append(self.R_out-self.LE.getz(self.p_bot),np.append(0.5*(r[1:]+r[:-1]),self.R_out)))
            profile = utils.interp(r,self.radii,profile)
            rho     = utils.interp(r,self.radii,self.profiles['rho'])
        elif domain == 'solid':
            r       = np.linspace(self.R_int,self.R_out-self.LE.getz(self.p_bot))
            dr      = np.diff(np.append(self.R_int,np.append(0.5*(r[1:]+r[:-1]),self.R_out-self.LE.getz(self.p_bot))))
            profile = utils.interp(r,self.radii,profile)
            rho     = utils.interp(r,self.radii,self.profiles['rho'])
        elif domain == 'bulk':
            r   = self.radii
            dr  = self.dr
            rho = self.profiles['rho']
        else:
            r       = np.linspace(self.R_out-self.LE.getz(domain[0]),self.R_out-self.LE.getz(domain[1]))
            dr      = np.diff(np.append(self.R_out-self.LE.getz(domain[0]),np.append(0.5*(r[1:]+r[:-1]),self.R_out-self.LE.getz(domain[1]))))
            profile = utils.interp(r,self.radii,profile)
            rho     = utils.interp(r,self.radii,self.profiles['rho'])
        
            
        return 4.*np.pi*sum(rho**mass_weighted*profile*r**2*dr)
        #return 4.*np.pi*np.trapz(rho**mass_weighted*profile*r**2,r)

    def getAverage(self,profile,**kw):
        
        try:    mean          = kw['mean']
        except: mean          = 'arithmetic'
        try: domain           = kw['domain']
        except: domain        = 'bulk'
        try: mass_weighted    = kw['mass_weighted']
        except: mass_weighted = False
            
        if mean == 'arithmetic':
            return self.getIntegral(profile,mass_weighted=mass_weighted,domain=domain) / self.getIntegral(np.ones_like(profile),mass_weighted=mass_weighted,domain=domain)
        elif mean == 'harmonic':
            return 1./self.getAverage(1./profile,mass_weighted=mass_weighted,domain=domain)
        
    def dTpot_dt(self, Q, H):
        
        # Effective heat capacity
        if self.convecting_cumulates:
            self.Cp_eff = self.getIntegral(self.profiles['cp']*self.profiles['temperatures']/self.adiabat.T_pot,mass_weighted=True,domain='bulk')
        else:
            self.Cp_eff = self.getIntegral(self.profiles['cp']*self.profiles['temperatures']/self.adiabat.T_pot,mass_weighted=True,domain='liquid')
        
        # latent heat term
        if self.with_mc:
            self.LH = self.mc.L*utils.interp(self.adiabat.T_pot,self.T_pot_lookup,self.dM_sol_dT_pot,right=0.,left=0.)
        else:
            self.LH = 0.
            
        self.H_int = 0
        for param in self.param_channel['internal-heating']:
            self.updateParam(param)
        
        # Heat balance
        #return (Q+self.H_int)/(self.Cp_eff + self.LH)
        return (self.H_int-Q)/(self.dEth_dTpot + self.LH)
        
    def updateBL(self, T_surf):
        
        self.BL.Delta_T = self.adiabat.T_pot-T_surf
        self.BL.D       = self.LE.getz(self.p_bot)
        self.BL.Ra      = self.getRa('liquid')
        self.BL.Pr      = self.getPr('liquid')
        self.BL.k       = self.profiles['k'][-1]
        
    def get_H_int(self):
        
        H_int = 0.
        for name in self.species:
            try:
                el = elements.elems_book[name]
            except:
                continue
            if not el.HPE:
                continue
            H_int += (self.content['liquid'][el.name]*self.getIntegral(self.profiles['phi'],mass_weighted=True,domain='liquid')\
                     +self.content['solid'][el.name]*self.getIntegral(1.-self.profiles['phi'],mass_weighted=True,domain='liquid'))*el.heat
        return H_int
    
    def decay(self,t):
        
        for name in self.species:
            try:
                el = elements.elems_book[name]
            except:
                continue
            if not el.HPE:
                continue
            self.content['bulk'][el.name]   *= el.radio.getParentRatio(t=t)/el.radio.getParentRatio(t=self.t)
            self.content['system'][el.name] *= el.radio.getParentRatio(t=t)/el.radio.getParentRatio(t=self.t)
            self.content['liquid'][el.name] *= el.radio.getParentRatio(t=t)/el.radio.getParentRatio(t=self.t)
            self.content['solid'][el.name]  *= el.radio.getParentRatio(t=t)/el.radio.getParentRatio(t=self.t)
        self.t = t
                
    def get_q_top(self):
        
        if self.BL != None:
            return self.BL.getFlux()
        else:
            return self.atm.getOLR()
        
    def get_q_bot(self,regime='zero'):
        
        if regime=='zero':
            return 0.
        elif regime == 'CMB':
            raise Exception('CMB heat flux not implemented yet')
        elif regime == 'RCMF':
            return utils.interp(self.p_bot,self.profiles["pressures"],-self.profiles['k'])*utils.interp(self.p_bot,self.mc.p_lookup,np.gradient(self.mc.solidus*(1.-self.mc.RCMF)+self.mc.liquidus*self.mc.RCMF,self.R_out-self.LE.getz(self.mc.p_lookup)))
        elif regime == 'Labrosse':
            # From Agrusta et al., 2020
            D       = self.LE.getz(self.p_CMB)-self.LE.getz(self.p_bot)
            Delta_T = self.adiabat.getT(self.p_CMB)-self.adiabat.getT(self.p_bot)
            k       = self.getAverage(self.profiles['k'],domain='solid',mean='arithmetic')
            if D!=0:
                q_ref   = k*Delta_T/D
            else:
                q_ref = 0.
            return 0.37*self.getRa('solid')**0.33*q_ref

    def RK4_step(self,dt,**kw):
            
        self.dt = dt

        T_surf = root_scalar(lambda T_surf:self.flux_residual(self,T_surf),
                             method=self.flux_minimization_method,
                             x0=self.atm.Ts,
                             bracket=[self.min_T_surf,self.adiabat.T_pot],
                             xtol=0.1).root
        
        if self.atm != None:
            self.atm.updateTs(T_surf)
        
        den  = np.array([1.,2.,2.,1.])
        k    = np.zeros(4)

        q_top = self.get_q_top()
        S_top = 4.*np.pi*self.R_out**2
        
        q_bot = self.get_q_bot('zero' if self.convecting_cumulates  else 'RCMF')
        S_bot = 4.*np.pi*(self.R_out-self.LE.getz(self.p_bot))**2
        k[0] = self.dTpot_dt(q_bot*S_bot+q_top*S_top,self.get_H_int())

        for i in range(1,4):
            MO_it    = copy.deepcopy(self)
            
            MO_it.dt = dt/den[i]
            for param in MO_it.param_channel['time-dep']:
                MO_it.updateParam(param)

            MO_it.updateT_pot(MO_it.adiabat.T_pot+k[i-1]*dt/den[i],**kw)
            #MO_it.decay(MO_it.t+dt/den[i])
         
            T_surf = root_scalar(lambda T_surf:MO_it.flux_residual(MO_it,T_surf),\
                                 method=self.flux_minimization_method,\
                                 x0=MO_it.atm.Ts,
                                 bracket=[self.min_T_surf,MO_it.adiabat.T_pot],\
                                 xtol=0.1).root
            
            #if MO_it.BL != None:
            #    MO_it.updateBL(T_surf)

            if MO_it.atm != None:
                MO_it.atm.updateTs(T_surf)

            q_top = MO_it.get_q_top()
            S_top = 4.*np.pi*MO_it.R_out**2

            q_bot = MO_it.get_q_bot('zero' if MO_it.convecting_cumulates  else 'RCMF')
            S_bot = 4.*np.pi*(MO_it.R_out-MO_it.LE.getz(MO_it.p_bot))**2
            k[i] = self.dTpot_dt(q_bot*S_bot+q_top*S_top,self.get_H_int())
            
            for channel in self.stopwatch:
                self.stopwatch[channel] += MO_it.stopwatch[channel]

        # update time-dependant parametrizations
        for param in self.param_channel['time-dep']:
            self.updateParam(param)
        self.dt = 0.
        
        self.tendencies['Tpot'] = 1./6*sum(k*den)
        self.updateT_pot(self.adiabat.T_pot + self.tendencies['Tpot']*dt,**kw)
        #self.decay(self.t+dt)
        self.t+=dt
        
        T_surf = root_scalar(lambda T_surf:self.flux_residual(self,T_surf),\
                             method=self.flux_minimization_method,\
                             x0=self.atm.Ts,
                             bracket=[self.min_T_surf,self.adiabat.T_pot],\
                             xtol=0.1).root

        if self.atm != None:
            self.atm.updateTs(T_surf)
        
        # adaptive time-stepping
        if self.adaptive_timestep:
            maximum_allowed_dt = min([abs(self.max_change[var]/self.tendencies[var]) for var in self.tendencies])
            if maximum_allowed_dt > dt:
                return min(2*dt,maximum_allowed_dt)
            else:
                return maximum_allowed_dt*0.9
        
        #if abs(self.tendencies['Tpot']*dt) < self.max_dT:
        #    return min(2*dt,abs(self.max_change['Topt']/self.tendencies['Tpot']))
        #else:
        #    return dt
        
    def init_ts(self,ts):
        
        for var in self.var2path:
            if var in self.parametrizations:
                #if not self.parametrization_is_profile[var]:
                if not (var in self.parametrization_types['profile']
                     or var in self.parametrization_types['action']):
                    ts.register(var)
            elif not var in ['adiabat','BL','LE','atm','mc','MO']:
                if var == 'T_sfc' and self.atm == None:
                    continue
                ts.register(var)
                
    def resetStopwatch(self,channel='all'):
        if channel == 'all':
            for channel in self.stopwatch:
                self.stopwatch[channel] = 0
        else:
            self.stopwatch[channel] = 0

# magma ocean undergoing fractional crystallization
class fractional_crystallization(magma_ocean):
    
    def updateInterior(self, **kw):
        
        try: propagate      = kw['propagate']
        except: propagate   = True
        try: fractionate    = kw['fractionate']
        except: fractionate = self.to_frac
        try: speciate = kw['speciate']
        except:
            if self.atm != None: speciate = {'equilibria':self.atm.equilibria,'elements':self.to_spec}
            else: speciate = {'equilibria':[],'elements':[]}
           
        # Update temperatures       
        if self.convecting_cumulates:
            self.profiles['temperatures'] = self.adiabat.getT(self.profiles['pressures'])
        else:
            self.profiles['temperatures'][np.where(self.profiles['pressures']<=self.p_bot)] = self.adiabat.getT(self.profiles['pressures'][np.where(self.profiles['pressures']<=self.p_bot)])
            
        # Update core parameters
        for param in self.param_channel['eos']:
            self.updateParam(param)
            
        if self.with_mc:
            
            # Update parametrizations in the pre-update channel
            for param in self.param_channel['pre-update']:
                self.updateParam(param)
            
            self.dM_system = self.M_system - utils.interp(self.adiabat.T_pot,self.T_pot_lookup,self.M_sys_lookup)
            
            self.M_system -= self.dM_system
            for sp_name in np.append(self.to_frac,self.to_spec):
                self.content['system'][sp_name] = ((self.M_system + self.dM_system) * self.content['system'][sp_name] - self.dM_system * (self.mc.RCMF * self.content['liquid'][sp_name] + (1.-self.mc.RCMF) * self.content['solid'][sp_name])) / self.M_system

            self.M_liquid = self.M_system
            self.M_solid  = self.dM_system
            
            # Update parametrizations in the pre-frac channel
            for param in self.param_channel['pre-frac']:
                self.updateParam(param)
                
            # Fractionation (requires updated M_liquid and M_solid)
            if self.with_mc and len(fractionate) != 0:
                self.fractionation(fractionate)
        
        # Update parametrizations in the pre-spec channel
        for param in self.param_channel['pre-spec']:
            self.updateParam(param)
            
        # Speciation (requires updated M_liquid, M_solid and parametrization: fO2)
        if self.with_mc and len(speciate['equilibria']) != 0:
            self.speciation(speciate['equilibria'],speciate['elements'])
    
        # Update parametrizations in the post-spec channel
        for param in self.param_channel['post-spec']:
            self.updateParam(param)
                
        if propagate:
            self.updateAverages()

    def updatep_bot(self):
        
        self.p_bot = self.getp_bot(self.adiabat.T_pot)
        
    def saveMeltingCurves(self,file_name):
        
        assert self.mc != None
        np.savetxt(file_name,np.transpose((self.T_pot_lookup,self.p_bot_lookup,self.M_sys_lookup,self.dM_sol_dT_pot)))

    def setMeltingCurves(self,mc,**kw):
        
        try:
            p_bot  = kw['p_bot']
            p_bot0 = kw['p_bot']
        except:
            p_bot  = self.p_CMB # default: whole-mantle MO
            p_bot0 = self.p_CMB
        try:
            data_mc           = np.genfromtxt(kw['file'])
            self.T_pot_lookup = data_mc[:,0]
            self.p_bot_lookup = data_mc[:,1]
            self.M_sys_lookup = data_mc[:,2]
            self.dM_sol_dT_pot  = data_mc[:,3]
            p_bot = max(self.p_bot_lookup)
            self.mc      = mc
            self.mc.RCMF = 0
        except:
            print('No file provided: calculating lookups, be patient!')
            self.mc      = mc
            self.mc.RCMF = 0
            T_pot        = self.adiabat.T_pot
            bottom_up_ad = adiabat(float(mc.getTliq(p_bot0)),p_bot0,self.p_surf,self.adiabat.dT_dp)
            self.updateT_pot(float(bottom_up_ad.getT(self.p_surf)),fractionate=[],speciate={'species':[],'equilibria':[]})
        
            # Utility values
            phi_int     = np.array([self.getIntegral(mc.getMeltFraction(self.profiles['pressures'],self.profiles['temperatures']),mass_weighted=False,domain=[p_bot0,self.p_surf])])
            phi_rho_int = np.array([self.getIntegral(mc.getMeltFraction(self.profiles['pressures'],self.profiles['temperatures']),mass_weighted=True,domain=[p_bot0,self.p_surf])])
            phi_sfc     = mc.getMeltFraction(self.profiles['pressures'][-1],self.profiles['temperatures'][-1])
            MO_depth    = self.LE.getz(p_bot) # depth of the MO once all crystals have settled
            MO_radius   = self.R_out - MO_depth # radius of the MO bottom once all crystals have settled
             
            # Lookups
            self.T_pot_lookup = np.array([self.adiabat.T_pot])
            self.p_bot_lookup = np.array([p_bot])
            self.M_sys_lookup = np.array([self.getIntegral(np.ones_like(self.radii),mass_weighted=True,domain=[p_bot,self.p_surf])])
        
            while phi_sfc > mc.RCMF:
                
                self.updateT_pot(self.adiabat.T_pot-1,fractionate=[],speciate={'species':[],'equilibria':[]})
                
                # Integral melt volume fraction
                phi_int   = np.append(phi_int,self.getIntegral(mc.getMeltFraction(self.profiles['pressures'],self.profiles['temperatures']),mass_weighted=False,domain=[p_bot0,self.p_surf]))
                d_phi_int = np.diff(phi_int)[-1]
            
                # Integral melt mass fraction
                phi_rho_int  = np.append(phi_rho_int,self.getIntegral(mc.getMeltFraction(self.profiles['pressures'],self.profiles['temperatures']),mass_weighted=True,domain=[p_bot0,self.p_surf]))
                  		        
                # Surface melt fraction
                phi_sfc   = mc.getMeltFraction(self.profiles['pressures'][-1],self.profiles['temperatures'][-1])
                  		
                # Write lookups
                self.T_pot_lookup = np.append(self.T_pot_lookup, self.adiabat.T_pot)
                self.M_sys_lookup = np.append(self.M_sys_lookup,self.getIntegral(np.ones_like(self.radii),mass_weighted=True,domain=[p_bot,self.p_surf]))
                
                # MO bottom pressure
                MO_radius = (MO_radius**3 - 3./4/np.pi*(d_phi_int))**(1./3)
                MO_depth  = self.R_out - MO_radius
        
                # MO bottom pressure
                p_bot =  self.LE.getp(MO_depth)
                self.p_bot_lookup = np.append(self.p_bot_lookup,p_bot)
                    
            self.dM_sol_dT_pot = np.gradient(phi_rho_int,self.T_pot_lookup)
                
            # Reset initial state
            self.updateT_pot(T_pot,fractionate=[],speciate={'species':[],'equilibria':[]})
            
        self.getp_bot = interp1d(self.T_pot_lookup,self.p_bot_lookup,bounds_error=False,fill_value=(0,p_bot0))
        self.updatep_bot()
            
        self.with_mc           = True
        self.var2path['M_liq'] = 'self.M_liquid'
        self.var2path['M_sol'] = 'self.M_solid'
            
        getMeltFrac = lambda var: utils.array2float(np.array([1. if p <= self.p_bot else 0. for p in var['pressures']]))
            
        self.setParametrization('phi',
                                getMeltFrac,
                                ['temperatures','pressures'],
                                channel='pre-update',
                                ptype='profile')
        
    
