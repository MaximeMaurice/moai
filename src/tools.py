import numpy as np
import matplotlib.pyplot as plt
import copy
import time as clock
import utils

from physics import eos # not needed, but avoids circular import
from chemistry import elements,molecules,equilibria

from scipy.integrate import ode
from scipy.optimize import root, minimize, least_squares
from scipy.interpolate import interp1d
from datapath import datapath

#### Classes
class SpeciationSolverFailed(Exception):
    pass

class time_series:
    def __init__(self,attribute=None):
        self.index  = 'time'
        self.paths  = {}
        self.values = {'time':np.array([])}
        if type(attribute)==str:
            self.load(attribute)
        elif attribute != None:
            self.attribute = attribute
            try: self.attribute.init_ts(self)
            except: pass

    def register(self,name,path='NA',from_attribute=True):
        self.values[name] = np.array([])
        if from_attribute:
            if path=='NA':
                # get rid of 'self.' in attribute's var2path
                path=self.attribute.var2path[name][5:]
            self.paths[name]  = 'attribute.'+path
        else:
            if path=='NA':
                raise Exception('Path needs to be given for non attribute registration.')
            self.paths[name]  = path
        
    def write(self,time):
        self.values['time'] = np.append(self.values['time'],time)
        for name in self.values:
            if name=='time':
                continue
            exec('self.values[name] = np.append(self.values[name],self.'+self.paths[name]+')')
            if not isinstance(self.values[name][-1],float):
                raise Warning('time series contains', name, 'of type', type(self.values[name][-1]))
            
    def plot(self,keys,**kw):
        for key in keys:
            plt.plot(self.values['time'],self.values[key],label=key)
            
    def logplot(self,keys,**kw):
        for key in keys:
            plt.semilogy(self.values['time'],self.values[key],label=key)
            
    def logtimeplot(self,keys,**kw):
        for key in keys:
            plt.semilogx(self.values['time'],self.values[key],label=key)
            
    def loglogplot(self,keys,**kw):
        for key in keys:
            plt.loglog(self.values['time'],self.values[key],label=key)
    
    def xyplot(self, x,ys,**kw):
        for y in ys:
            plt.plot(self.values[x],self.values[y],label=y)
            
    def reset(self):
        for name in self.values:
            self.values[name] = np.array([])
            
    def __call__(self,name='',t=None):
        
        if t==None:
            return self.values[name]
        else:
            if name=='':
                return_dict={}
                for name in self.values:
                    return_dict[name] = utils.interp(t,self.values['time'],self.values[name])
                return return_dict
            else:
                return utils.interp(t,self.values['time'],self.values[name])
            
    def __iter__(self):
        
        return {t:self.__call__(t=t) for t in self.values['time']}

    def save(self,file_name):
        to_save = []
        for name in self.values:
            if len(self.values[name]) == len(self.values['time']):
                to_save.append(name)
            
        np.savetxt(file_name,np.transpose([np.append(val,self.values[val]) for val in to_save]),fmt='%s')
        
    def load(self,file_name):
        
        data = np.genfromtxt(file_name,dtype=str)
        for i,var in enumerate(data[0,:]):
            self.values[var] = data[1:,i].astype(float)
            
class MO_time_series(time_series):
    
    def Mplot(self,keys,**kw):
        for key in keys:
            plt.plot(1-self.values['M_sys']/self.values['M_sys'][0],self.values[key],label=key)
            
    def Mlogplot(self,keys,**kw):
        for key in keys:
            plt.semilogy(1-self.values['M_sys']/self.values['M_sys'][0],self.values[key],label=key)
            
class melting_curves:
    
    def __init__(self,p,T_sol,T_liq,latent_heat):
        self.p_lookup = p
        self.solidus  = T_sol
        self.liquidus = T_liq
        self.L        = latent_heat
        self.RCMF     = None
        self.getTliq = interp1d(self.p_lookup,self.liquidus,fill_value='extrapolate')
        self.getTsol = interp1d(self.p_lookup,self.solidus,fill_value='extrapolate')
        
    def getMeltFraction(self,p,T):
        return np.minimum(np.maximum((T-self.getTsol(p))\
                                    /(self.getTliq(p)-self.getTsol(p)),0.),1.)
    
    def plot(self, p_units='GPa', T_units='K'):
        plt.plot(self.solidus+utils.T_offset[T_units],self.p_lookup*utils.p_factor[p_units],label='solidus')
        plt.plot(self.liquidus+utils.T_offset[T_units],self.p_lookup*utils.p_factor[p_units],label='liquidus')
        
    
class lithostatic_equilibrium:

    def __init__(self, p_surf, p_bound, dz_dp, log=False, N=100):
        self.p_surf = p_surf
        self.dz_dp  = dz_dp 
        self.isLog  = log

        if self.isLog:
            self.p_lookup = np.logspace(np.log10(self.p_surf), np.log10(p_bound),N)
        else:
            self.p_lookup = np.linspace(self.p_surf,p_bound,N)

        self.update_lookup()

    def update_lookup(self):
        self.z_lookup = np.array([0.])
        solver = ode(self.dz_dp)
        solver.set_initial_value(0.,self.p_surf)
        for p in self.p_lookup[1:]:
            self.z_lookup = np.append(self.z_lookup, solver.integrate(p))
            if not solver.successful(): raise Exception('Lithostatic equilibrium: integration failed.')
        if (self.p_lookup[-1]-self.p_lookup[0])*(self.z_lookup[-1]-self.z_lookup[0]) > 0:
            self.getp = interp1d(self.z_lookup, self.p_lookup,fill_value=(min(self.p_lookup),max(self.p_lookup)),bounds_error=False)
            self.getz = interp1d(self.p_lookup, self.z_lookup,fill_value=(min(self.z_lookup),max(self.z_lookup)),bounds_error=False)
        else:
            self.getp = interp1d(self.z_lookup, self.p_lookup,fill_value=(max(self.p_lookup),min(self.p_lookup)),bounds_error=False)
            self.getz = interp1d(self.p_lookup, self.z_lookup,fill_value=(max(self.z_lookup),min(self.z_lookup)),bounds_error=False)

    def update_p_surf(self, p_surf):
        self.p_surf = p_surf
        self.update_lookup()

class adiabat:

    def __init__(self, T_pot, p_ref, p_bound, dT_dp, log=False, N=100):

        self.isLog = log
        self.T_pot   = T_pot
        self.dT_dp   = dT_dp
        self.updateP(p_ref,p_bound,N)

    def updateLookup(self):

        self.T_lookup = np.array([self.T_pot])
        if self.isLog:
            solver = ode(self.dT_dp)
            solver.set_initial_value(np.log(self.T_pot),np.log(self.p_ref))
            for log_p in np.log(self.p_lookup[1:]):
                self.T_lookup = np.append(self.T_lookup, np.exp(solver.integrate(log_p)))
                if not solver.successful(): raise Exception('Adiabat: integration failed.')

        else:
            solver = ode(self.dT_dp)
            solver.set_initial_value(self.T_pot,self.p_ref)
            for p in self.p_lookup[1:]:
                self.T_lookup = np.append(self.T_lookup, solver.integrate(p))
                if not solver.successful(): raise Exception('Adiabat: integration failed.')
                
        self.getT = interp1d(self.p_lookup,self.T_lookup,fill_value=(min(self.T_lookup),max(self.T_lookup)),bounds_error=False)
        self.getp = interp1d(self.T_lookup,self.p_lookup,fill_value=(min(self.p_lookup),max(self.p_lookup)),bounds_error=False)

    def updateT_pot(self, T_pot):
        self.T_pot = T_pot
        self.updateLookup()

    def updateP(self,p_ref,p_bound,N=100):
        self.p_ref   = p_ref
        self.p_bound = p_bound
        if self.isLog:
            self.p_lookup = np.logspace(np.log(self.p_ref),np.log(p_bound),num=N,base=np.exp(1))
        else:
            self.p_lookup = np.linspace(self.p_ref,p_bound,N)

        self.updateLookup()
    

class boundary_layer:
    def __init__(self,DeltaT,Ra,D,k,Pr=1.,AR=1.):
        self.Ra          = Ra
        self.Pr          = Pr
        self.AR          = AR
        self.D           = D
        self.k           = k
        self.DeltaT      = DeltaT
        self.scaling     = 'soft'
        
    def update(self,**kw):
        if 'Ra' in kw:
            self.Ra = kw['Ra']
        if 'DeltaT' in kw:
            self.DeltaT = kw['DeltaT']
        if 'Ra_star' in kw:
            self.Ra = kw['Ra_star']*self.DeltaT
        if 'D' in kw:
            self.D = kw['D']
        if 'Pr' in kw:
            self.Pr = kw['Pr']
        
    def getFlux(self):
        if self.scaling == 'soft':
            return 0.089*self.k*(self.DeltaT)/self.D*self.Ra**(1./3)
        elif self.scaling == 'hard':
            return 0.22*self.k*(self.DeltaT)/self.D*self.Ra**(2./7)*self.Pr**(-1./7)*self.AR**(-3./7)
        elif self.scaling == 'mix':
            if np.log10(self.Ra)>19:
                return 0.22*self.k*(self.DeltaT)/self.D*self.Ra**(2./7)*self.Pr**(-1./7)*self.AR**(-3./7)
            else:
                return 0.089*self.k*(self.DeltaT)/self.D*self.Ra**(1./3)
        else:
            raise Exception('Unknown scaling: '+self.scaling)
            

    


#### Functions

def speciation_old(M_bulk, M_sol, M_liq, S_red, eqs, pp0):
    
    elems = list(M_bulk.keys())
    
    # Find species
    species = []
    for sp in pp0:
        species.append(sp)
    molecular_masses = np.array([sp.molecular_mass for sp in species])
    
    ## Solve

    # chemical equilibria
    f = {}
    for eq in eqs:
        f[eq]=chemical_equilibrium_res_factory(species,eq)
    
    # masses conservation
    g = {}
    for el in elems:
        g[el]=mass_conservation_res_factory(M_bulk[el],
                                            M_sol,M_liq,
                                            S_red,
                                            species,el)
    
    # average molecular mass 
    h=lambda pp:sum(molecular_masses*pp[:-1])/sum(pp[:-1])/pp[-1] - 1.
    
    ## derivatives
    
    # chemical equilibria
    df = {}
    for eq in eqs:
        df[eq]=[]
        for i,sp in enumerate(species):
            df[eq].append(d_chemical_equilibrium_res_dpi_factory(species,eq,i)) # i-th partial pressure-derivative
        df[eq].append(lambda pp:0.)                                             # average molecular mass-derivative
     
    # masses conservation
    dg={}
    for el in elems:
        dg[el]=[]
        for i,sp in enumerate(species):
            dg[el].append(d_mass_conservation_res_dpi_factory(M_bulk[el],
                                                              M_sol,M_liq,
                                                              S_red,
                                                              species,el,i))              # i-th partial pressure-derivative
        dg[el].append(d_mass_conservation_res_dmu_factory(M_bulk[el],S_red,species,el))   # average molecular mass-derivative
    
    # average molecular mass 
    dh=[]
    for i,sp in enumerate(species):
        dh.append(lambda pp:sp.molecular_mass/sum(pp[:-1])/pp[-1])              # i-th partial pressure-derivative  ########
    dh.append(lambda pp: -sum(molecular_masses*pp[:-1])/sum(pp[:-1])/pp[-1]**2) # average molecular mass-derivative ########
    
    ## System
    F=lambda pp: np.append(np.array([f[eq](pp) for eq in eqs]),
                                         #np.append(np.array([g[el](pp) for el in elems]),np.append(h(pp),k(pp)))) ########
                                         np.append(np.array([g[el](pp) for el in elems]),h(pp))) ########
    
    Fmin=lambda pp:sum(np.append(sum([f[eq](pp)**2 for eq in eqs]),np.append(sum([g[el](pp)**2 for el in elems]),h(pp)**2)))
    
    ## Jacobian
    J=lambda pp:np.vstack(([[df[eq][i](pp) for i in range(len(species)+1)] for eq in eqs],
                               [[dg[el][i](pp) for i in range(len(species)+1)] for el in elems],
                               [dh[i](pp)      for i in range(len(species)+1)]))
    
    Jmin=lambda pp:[  sum([2*f[eq](pp)*df[eq][i](pp) for eq in eqs]) \
                    + sum([2*g[el](pp)*dg[el][i](pp) for el in elems]) \
                    + 2*h(pp)*dh[i](pp) for i in range(len(species)+1)]
         
    # initial guess
    mu0 = sum([pp0[sp]*sp.molecular_mass for sp in pp0])/sum([pp0[sp] for sp in pp0])
    # Solve
    sol_root=root(F,np.append([pp0[sp] for sp in pp0],mu0),jac=J,tol=1e-10)
    
    #bounds0 = [(0,np.inf) for i in range(len(species))]
    #bounds0.append((2,44))
    #
    #sol_min=minimize(Fmin,np.append([pp0[sp] for sp in pp0],mu0),
    #                 jac=Jmin,method='trust-constr',bounds=bounds0,tol=1e-10)
    #                 
    #sol_min=minimize(Fmin,np.append([pp0[sp] for sp in pp0],mu0),
    #                 method='Nelder_Mead',bounds=bounds0,tol=1e-5)
    #
    #sol_ls = least_squares(F,np.append([pp0[sp] for sp in pp0],mu0),jac=J)
    #
    #print('root:',Fmin(sol_root.x),sum(F(sol_root.x)**2))
    #print('min:',Fmin(sol_min.x),sum(F(sol_min.x)**2))
    #print('least-squares',Fmin(sol_ls.x),sum(F(sol_ls.x)**2))
    #
    #print(sol_min.x)
    #print(sol_root.x)
    #print(sol_ls.x)
    
    # Sanity check
    if not sol_root.success:
        raise SpeciationSolverFailed
     
    return {sp:sol_root.x[i] for i,sp in enumerate(pp0)}

# Robust speciation solver: iterates from a known state
p0s = {# 2H2+O2=2H2O    2CO+O2=2CO2
       frozenset([equilibria.eq_H.expression,equilibria.eq_C.expression]):\
       {elements.H:    2.6809428863754843e+20, # H content
        elements.C:    2.6809428863754843e+20, # C content
        'Msol':         0,                      # Msol
        'Mliq':         2.6809428863754845e+24, # Mliq
        'Sred':         52058019178382.14,      # Sred
        equilibria.eq_H: 0.29017611471080934,    # eq_H cst
        equilibria.eq_C: 0.007392229885041656}, # eq_C cst
       
       # 2H2+O2=2H2O    2CO+O2=2CO2    CO+3H2=CH4+H2O
       frozenset([equilibria.eq_H.expression,equilibria.eq_C.expression,equilibria.eq_CH.expression]):\
       {elements.H:    2.6809428863754843e+20, # H content
        elements.C:    2.6809428863754843e+20, # C content
        'Msol':         0,                      # Msol
        'Mliq':         2.6809428863754845e+24, # Mliq
        'Sred':         52058019178382.14,      # Sred
        equilibria.eq_H: 0.29017611471080934,    # eq_H cst
        equilibria.eq_C: 0.007392229885041656,   # eq_C cst
        equilibria.eq_CH:4.406673438424642e-08}, # eq_CH cst
        
       # 2H2+O2=2H2O    2CO+O2=2CO2    N2+3H2=2NH3
       frozenset([equilibria.eq_H.expression,equilibria.eq_C.expression,equilibria.eq_N1.expression]):\
       {elements.H:     2.6809428863754843e+20, # H content
        elements.C:     2.6809428863754843e+20, # C content
        elements.N:     2.6809428863754843e+18, # N content
        'Msol':          0,                      # Msol
        'Mliq':          2.6809428863754845e+24, # Mliq
        'Sred':          52058019178382.14,      # Sred
        equilibria.eq_H:  0.29017611471080934,    # eq_H cst
        equilibria.eq_C:  0.007392229885041656,   # eq_C cst
        equilibria.eq_N1: 1.1618308408007343e-10},# eq_N1 cst
      }
x0s = {# 2H2+O2=2H2O    2CO+O2=2CO2
       frozenset([equilibria.eq_H.expression,equilibria.eq_C.expression]):\
       {molecules.H2:5.90125671e+05,
        molecules.H2O:2.60981138e+05,
        molecules.CO:1.06928553e+07,
        molecules.CO2:8.18063974e+05},
       
       # 2H2+O2=2H2O    2CO+O2=2CO2    CO+3H2=CH4+H2O
       frozenset([equilibria.eq_H.expression,equilibria.eq_C.expression,equilibria.eq_CH.expression]):\
       {molecules.H2:490983.1886947303,
        molecules.H2O:264482.8114673321,
        molecules.CO:10247915.412124762,
        molecules.CO2:881096.0699533084,
        molecules.CH4:20.209185651601615},
       
       # 2H2+O2=2H2O    2CO+O2=2CO2    N2+3H2=2NH3
       frozenset([equilibria.eq_H.expression,equilibria.eq_C.expression,equilibria.eq_N1.expression]):\
       {molecules.H2:5.90125671e+05,
        molecules.H2O:2.60981138e+05,
        molecules.CO:1.06928553e+07,
        molecules.CO2:8.18063974e+05,
        molecules.N2:4.83310908e+04,
        molecules.NH3:1.07424047e+01}
       }
def spec_iter_old(p_tar,**kw):                      
    
    if 'n_step' in kw:
        n_step = kw['n_step']
    else:
        n_step=20
        
    eqs   = []
    elems = []
    for k in p_tar:
        if isinstance(k,equilibria.equilibrium):
            eqs.append(k)
        elif isinstance(k,elements.element):
            elems.append(k)
        
    p = p0s[frozenset([eq.expression for eq in eqs])]
    if 'x0' in kw:
        x = kw['x0']
    else:
        x = x0s[frozenset([eq.expression for eq in eqs])]
            
    dp = utils.dicmulscal(utils.dicsub(p_tar,p),1/n_step)
    count = 0
    while count < n_step:
        p = utils.dicadd(p,dp)
        #p     += dp
        count +=1
        for eq in eqs:
            eq.constant_val = p[eq]
        x  = speciation_old({element:p[element] for element in elems},
                        p['Msol'],p['Mliq'],p['Sred'],
                        eqs,
                        x)
        
    return x
    
#### Factories
        
### CHEMICAL EQUILIBRIA AND MASS CONSERVATION

# equation factories

class chemical_equilibrium_res_factory:
    def __init__(self,species,eq):
        self.species = species
        self.eq      = eq
    def __call__(self,pp):
        p_ref = 1e5 # partial pressure [Pa] to fugacity [non-dim]
        return np.product([(pp[i]/p_ref)**self.eq.coefs[sp] for i,sp in enumerate(self.species)])/self.eq.constant_val - 1. ########

class d_chemical_equilibrium_res_dpi_factory:
    def __init__(self,species,eq,i):
        self.species = species
        self.eq      = eq
        self.i                    = i
    def __call__(self,pp):
        p_ref = 1e5 # partial pressure [Pa] to fugacity [non-dim]
        return self.eq.coefs[self.species[self.i]]/p_ref*(pp[self.i]/p_ref)**-1\
               *np.product([(pp[i]/p_ref)**self.eq.coefs[sp] for i,sp in enumerate(self.species)])/self.eq.constant_val  ########

class mass_conservation_res_factory:
    def __init__(self,M_bulk,M_sol,M_liq,S_red,species,elem):
        self.species          = species
        self.elem             = elem
        self.M_bulk           = M_bulk
        self.M_sol            = M_sol
        self.M_liq            = M_liq
        self.S_red            = S_red
    def __call__(self,pp):
        return sum([((self.M_liq+sp.part_coef*self.M_sol)*sp.Henry_law(pp[i],sum(pp[:-1]))+self.S_red*pp[i]*sp.molecular_mass/pp[-1])\
                    *sp.coefs[self.elem]/sp.molecular_mass*self.elem.atomic_mass for i,sp in enumerate(self.species)])/self.M_bulk - 1 ########


class d_mass_conservation_res_dpi_factory:
    def __init__(self,M_bulk,M_sol,M_liq,S_red,species,elem,i):
        self.species          = species
        self.elem             = elem
        self.M_bulk           = M_bulk
        self.M_sol            = M_sol
        self.M_liq            = M_liq
        self.S_red            =  S_red
        self.i                = i
    def __call__(self,pp):
        return (((self.M_liq+self.species[self.i].part_coef*self.M_sol)*self.species[self.i].dHenry_law_dpp(pp[self.i],sum(pp[:-1]))+self.S_red*self.species[self.i].molecular_mass/pp[-1])\
                 *self.species[self.i].coefs[self.elem]/self.species[self.i].molecular_mass*self.elem.atomic_mass\
               +sum([(self.M_liq+sp.part_coef*self.M_sol)*sp.dHenry_law_dp(pp[i],sum(pp[:-1]))*sp.coefs[self.elem]/sp.molecular_mass*self.elem.atomic_mass for i,sp in enumerate(self.species)]))/self.M_bulk

class d_mass_conservation_res_dmu_factory:
    def __init__(self,M_bulk,S_red,species,elem):
        self.M_bulk           = M_bulk
        self.S_red            = S_red
        self.species          = species
        self.elem             = elem
    def __call__(self,pp):
        return -sum([self.S_red*pp[i]*sp.molecular_mass/pp[-1]**2*sp.coefs[self.elem]/sp.molecular_mass*self.elem.atomic_mass for i,sp in enumerate(self.species)])/self.M_bulk ########
