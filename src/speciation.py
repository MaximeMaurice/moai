#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 17 11:30:22 2023

@author: maxime
"""

from chemistry.elements import *
from chemistry.molecules import *
from chemistry.equilibria import *
from chemistry.redox import *
from physics.constants import *
from physics.phase_change.volatiles import S_solubility_G22,N_solubility_D22
from scipy.optimize import root
import numpy as np


class SpeciationSolverFailed(Exception):
    pass

## SEED: solution for the reference state:
#    fO2   = IW-2
#    T     = 2000 K
#    M_MO  = 4e24 kg     (BSE mass)
#    M_sol = 0
#    R     = 6371000 m   (Earth radius)
#    g     = 9.798 kg/s² (Earth gravity)
#    X_H   = 100 ppm
#    X_C   = 100 ppm
#    X_N   = 100 ppm
#    X_S   = 100 ppm

x0 = {'H2': 2.09639794e+06,
      'H2O':2.15870140e+05,
      'CO': 1.47715515e+07,
      'CO2':3.33613767e+05,
      'S2': 6.18369814e-02,
      'SH2':9.86001302e+02,
      'SO2':3.02914773e-02,
      'CH4':2.83652820e+03,
      'N2': 6.36524802e+06,
      'NH3':1.57592315e+03,
      'HCN':9.00608564e+03}

state_ref = {'M_bulk':{'H':100e-6*BSE_mass,'C':100e-6*BSE_mass,'N':100e-6*BSE_mass,'S':100e-6*BSE_mass},
             'M_MO':BSE_mass,
             'M_sol':0,
             'R':Earth_surface_radius,
             'g':Earth_gravity,
             'DeltaIW':-2,
             'T':2000,
             'pp0':x0}

def speciation(M_bulk,
               M_liq,
               M_sol,
               R,
               g,
               pp0):
    
    elems = [H,C,N,S]
    gases = [H2,H2O,CO,CO2,S2,SH2,SO2,CH4,N2,NH3,HCN]
    eqs   = [eq_H,eq_C,eq_CH,eq_N1,eq_S1,eq_S2,eq_N2]
    
    ## Solve

    # chemical equilibria
    f = {}
    for eq in eqs:
        f[eq]=chemeqres_factory(eq,gases)
    
    # masses conservation
    k = {}
    for el in elems:
        k[el]=massconsres_factory(el,M_bulk[el.symbol],M_liq,M_sol,R,g,gases)
    
    # System
    F=lambda pp: np.append(np.array([f[eq](pp) for eq in eqs]),np.array([k[el](pp) for el in elems]))
    
    # Solve
    sol=root(F,x0=[pp0[gas.formula] for gas in gases])
    
    # Sanity check
    if not sol.success:
        raise SpeciationSolverFailed
     
    return {sp:sol.x[i] for i,sp in enumerate(pp0)}

def spec_iter(state_tar={},state_ini=state_ref,N_step=50):
    
    if 'M_bulk' not in state_tar:
        state_tar['M_bulk'] = state_ref['M_bulk']
    else:
        for el in ['H','C','N','S']:
            if not el in state_tar['M_bulk']:
                state_tar['M_bulk'][el] = state_ref['M_bulk'][el]
    if 'M_MO' not in state_tar:
        state_tar['M_MO'] = state_ref['M_MO']
    if 'M_sol' not in state_tar:
        state_tar['M_sol'] = state_ref['M_sol']
    if 'R' not in state_tar:
        state_tar['R'] = state_ref['R']
    if 'g' not in state_tar:
        state_tar['g'] = state_ref['g']
    if 'T' not in state_tar:
        state_tar['T'] = state_ref['T']
    if 'DeltaIW' not in state_tar:
        state_tar['DeltaIW'] = state_ref['DeltaIW']
        
    pp = state_ini['pp0']
    
    # iteration
    for M_H,M_C,M_N,M_S,M_MO,M_sol,R,g,T,DeltaIW in zip(np.logspace(np.log10(state_ini['M_bulk']['H']),np.log10(state_tar['M_bulk']['H']),N_step),
                                                        np.logspace(np.log10(state_ini['M_bulk']['C']),np.log10(state_tar['M_bulk']['C']),N_step),
                                                        np.logspace(np.log10(state_ini['M_bulk']['N']),np.log10(state_tar['M_bulk']['N']),N_step),
                                                        np.logspace(np.log10(state_ini['M_bulk']['S']),np.log10(state_tar['M_bulk']['S']),N_step),
                                                        np.linspace(state_ini['M_MO'],state_tar['M_MO'],N_step),
                                                        np.linspace(state_ini['M_sol'],state_tar['M_sol'],N_step),
                                                        np.linspace(state_ini['R'],state_tar['R'],N_step),
                                                        np.linspace(state_ini['g'],state_tar['g'],N_step),
                                                        np.linspace(state_ini['T'],state_tar['T'],N_step),
                                                        np.linspace(state_ini['DeltaIW'],state_tar['DeltaIW'],N_step)):
        
        ## update state
        fO2                = 10**(fO2_buffers_H22(sum([pp[gas] for gas in pp]),T)+DeltaIW) # p used here is the one calculated at the last step,
                                                                                           # but it is too small to have an influence anyway.        
        # equilibrium constants
        eq_H.constant_val  = eq_H.constant_fun(T)*fO2
        eq_C.constant_val  = eq_C.constant_fun(T)*fO2
        eq_CH.constant_val = eq_CH.constant_fun(T)
        eq_N1.constant_val = eq_N1.constant_fun(T)
        eq_S1.constant_val = eq_S1.constant_fun(T)
        eq_S2.constant_val = eq_S2.constant_fun(T)/fO2**2
        eq_N2.constant_val = eq_N2.constant_fun(T)
        
        # solubilities
        S2.Henry_law       = lambda pp_,p: S_solubility_G22(pp_,T,fO2)
        N2.Henry_law       = lambda pp_,p: N_solubility_D22(pp_,p,T,DeltaIW)
        
        # solve
        pp = speciation({'H':M_H,'C':M_C,'N':M_N,'S':M_S},
                        M_MO,
                        M_sol,
                        R,
                        g,
                        pp)
        
    return pp


class chemeqres_factory:
    
    def __init__(self,eq,gases):
        
        self.eq    = eq
        self.gases = gases
        
    def __call__(self,pp):
        
        f = pp*1e-5
        return np.prod([f[i]**self.eq.coefs[gas] for i,gas in enumerate(self.gases)])/self.eq.constant_val-1

class massconsres_factory:
    def __init__(self,el,M_bulk,M_MO,M_sol,R,g,gases,verbose=False):
        
        self.el        = el
        self.M_bulk    = M_bulk
        self.M_MO      = M_MO
        self.M_sol     = M_sol
        self.R         = R
        self.g         = g
        self.gases     = gases
        self.verbose   = verbose
        
    def __calc_M_gas__(self,pp):
        
        p      = sum(pp)
        mu_atm = sum([pp[i]*gas.molecular_mass for i,gas in enumerate(self.gases)])/p
        M_atm  = 4*np.pi*self.R**2/self.g*p
        X_atm  = np.array([gas.molecular_mass/mu_atm*pp[i]/p for i,gas in enumerate(self.gases)])
        
        return M_atm*sum([X_atm[i]*self.el.atomic_mass*gas.coefs[self.el]/gas.molecular_mass for i,gas in enumerate(self.gases)])
        
    def __calc_M_diss__(self,pp):
        
        p = sum(pp)
        X_MO   = [gas.Henry_law(pp[i],p) for i,gas in enumerate(self.gases)]
        
        return self.M_MO*sum([X_MO[i]*self.el.atomic_mass*gas.coefs[self.el]/gas.molecular_mass for i,gas in enumerate(self.gases)])
    
    def __calc_M_cum__(self,pp):
        
        p = sum(pp)
        X_MO   = [gas.Henry_law(pp[i],p) for i,gas in enumerate(self.gases)]
        
        return self.M_MO*sum([gas.part_coef*X_MO[i]*self.el.atomic_mass*gas.coefs[self.el]/gas.molecular_mass for i,gas in enumerate(self.gases)])
        
    def __call__(self,x):
        
        M_gas  = self.__calc_M_gas__(x)
        M_diss = self.__calc_M_diss__(x)
        M_cum  = self.__calc_M_cum__(x)
        if self.verbose:
            print(self.el.symbol,'mass conservation: M_gas=',M_gas,',M_diss=',M_diss,',M_bulk=',self.M_bulk)
            
        return (M_gas + M_diss + M_cum) / self.M_bulk - 1