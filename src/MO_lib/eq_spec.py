#!/usr/bin/env python
# coding: utf-8

# # Magma ocean thermal evolution model
# by Maxime Maurice (maxime.maurice@rice.edu)

# ### Dependencies and Packages

# In[ ]:


from physics import eos,viscosity
from physics.phase_change.refractories import mc_book
from chemistry.elements import *
from chemistry.molecules import *
from chemistry.equilibria import *
from chemistry.redox import *
from datapath import datapath
from magma_ocean import magma_ocean
from atmospheres import atmosphere, multi_species_single_condensible_convective_atmosphere
from utils import y2s

# ### case-specific settings

# In[ ]:


eq_C.coefs[O2]      = 0
eq_H.coefs[O2]      = 0

albedo              = 0.2
faint_young_sun     = 0.75
solar_const         = 1367.6 # [W/m^2]
stellar_irradiation = 1./4*solar_const*(1.-albedo)*faint_young_sun

R_planet            = 6371000 # [m]
g_planet            = 9.798 # [m/s^2]
BSE_mass            = 4e24 # [kg]
EO_mass             = 1.4e21  # [kg]

T_pot               = 3003 # [K] corresponds to nothing
p_CMB               = 135e9 # [Pa]

BSE                 = pd.read_excel(datapath+'/BSE_composition.ods',index_col=0,sheet_name='molar').T['Palme and Oneill 2003'] # molar fractions


# ## Magma Ocean

# ### Prepare EOS

# In[ ]:


from burnman.minerals.DKS_2013_liquids import MgSiO3_liquid
# Equations of state for MgSiO3 liquid [de Koker and Stixrude 2013]

rho   = lambda p,T:MgSiO3_liquid().evaluate(['density'],p,T)
alpha = lambda p,T:MgSiO3_liquid().evaluate(['thermal_expansivity'],p,T)
cp    = lambda p,T:MgSiO3_liquid().evaluate(['C_p'],p,T)/MgSiO3_liquid().molar_mass

# Need to vectorize: burnman struggles otherwise
rho_vec   = np.vectorize(rho)
alpha_vec = np.vectorize(alpha)
cp_vec    = np.vectorize(cp)


# ### Create Magma ocean

# In[ ]:


MO = magma_ocean(T_pot,
                 p_CMB, # for fractional crystallization cases, we have to make the MO believe that 
                 eos={'rho':rho_vec,'alpha':alpha_vec,'cp':cp_vec},
                 g=g_planet,
                 R=R_planet,
                 ConvCum=True)


# ### melting curves

# In[ ]:


MO.setMeltingCurves(mc_book['Earth'],file=datapath+'/mc_lookups_equi_RCMF=0.4_MgSiO3_liquid_EOS.dat',RCMF=0.4)
#MO.setMeltingCurves(mc_book['Earth'],RCMF=0.4,p_bot0=55e9)

# initialize potential temperature
MO.updateT_pot(MO.T_pot_lookup[np.argmin(abs(MO.p_bot_lookup-55e9))])

# ### Viscosity law

# In[ ]:


def eta_mix(p,T,phi):
    prefactor = 26
    if phi<MO.mc.RCMF:
        return viscosity.viscosity_book['Arrhenius'](p,T)*np.exp(prefactor*phi)
    else:
        return viscosity.viscosity_book['VFT'](T)/(1.-(1.-phi)/(1.-MO.mc.RCMF))**2.5
    
MO.setParametrization('viscosity',
                      lambda var: np.vectorize(eta_mix)(var['pressures'],var['temperatures'],var['phi']),
                      ['temperatures','pressures','phi'],
                      ptype='profile')


# ### boundary layer

# In[ ]:


MO.setParametrization('k',lambda var: 1e-6*var['rho']*var['cp'],['rho','cp'],ptype='profile')
#MO.addBoundaryLayer(1.) # 1 K temperature contrast accross the thermal boundary layer (will be overwritten when solving flux balance for T_sfc)


# In[ ]:


MO.updateT_pot(MO.T_pot_lookup[np.argmin(abs(MO.p_bot_lookup-55e9))])


# ## Oxygen Fugacity

# ### Core-mantle equilibration effective $p$-$T$ conditions

# In[ ]:


z_eq = 'sfc'
p_eq = MO.p_surf
T_eq = MO.adiabat.getT(p_eq)


# ### ferrous- ferric-iron oxides
# The partition coefficients are average values calculated for the crystallization sequence of the terrestrial MO from Elkins-Tanton 2008  
# The initial Fe$^{3+}/\Sigma$Fe is calculated so that the $f_{O_2}$ at the core-mantle equilibration $p,T$ conditions is IW-2. In the present case, equilibration occurs at the bottom of the MO and $p_{\rm eq}=55$ GPa and $T_{\rm eq}$ is on the adiabat at  $p_{\rm eq}$.

# #### Partition coefficients

# In[ ]:


from chemistry.partition_coefficients import get_part_coef
Fe_ferrous.part_coef     = 0.94
MO.setParametrization('D_Fe3+_profile',lambda var: get_part_coef(var['pressures'],'Fe3+','2000km'),['pressures'],ptype='profile')
# What about weighting the partition coefficient profile in the average by RCMF-phi?
def set_Fe_ferric_part_coef(MO):
    Fe_ferric.part_coef = MO.getAverage(MO.profiles['D_Fe3+_profile'],mass_weighted=True,domain=[MO.p_bot,MO.p_liq])
    return Fe_ferric.part_coef
MO.setParametrization('D_Fe3+_eff', lambda var: set_Fe_ferric_part_coef(var['MO']),[],ptype='scalar')


# #### Initial Fe$^{3+}/\Sigma$Fe in MO

# In[ ]:


# Fe3+/Fe2+ (molar) set by the initial condition (impose bz core-mantle equilibration): fO2(p_eq,T_eq,Fe3+/Fe2+) = IW-2
initial_molar_ferric_to_ferrous_iron_liq = ferric_to_ferrous_iron_H22(p_eq,                                # core-mantle equilibrium pressure
                                                                      T_eq,                                # core-mantle equilibrium temperature
                                                                      10**(fO2_buffers_H22(p_eq,T_eq)-2), # fO2(p_eq,T_eq) = IW-2
                                                                      BSE)                                # composition


# Molar ratio to mass fraction conversion

# In[ ]:


total_iron_oxide_mass_fraction_BSE = (pd.read_excel(datapath+'/BSE_composition.ods',index_col=0,sheet_name='mass').T['Palme and Oneill 2003'])['FeO']/100

def molar_ratio_to_mass_frac(molar_ratio, total_mass_frac=total_iron_oxide_mass_fraction_BSE):
    Fe3p_mass_frac = molar_ratio*Fe_ferric.molecular_mass/Fe_ferrous.molecular_mass/(1+molar_ratio*Fe_ferric.molecular_mass/Fe_ferrous.molecular_mass)*total_mass_frac
    return Fe3p_mass_frac, total_mass_frac-Fe3p_mass_frac


# In[ ]:


MO.addSpecies([Fe_ferric,Fe_ferrous],
              [(MO.M_liquid+Fe_ferric.part_coef*MO.M_solid)/MO.M_system*molar_ratio_to_mass_frac(initial_molar_ferric_to_ferrous_iron_liq)[0],
               (MO.M_liquid+Fe_ferrous.part_coef*MO.M_solid)/MO.M_system*molar_ratio_to_mass_frac(initial_molar_ferric_to_ferrous_iron_liq)[1]],
              False,False)
MO.to_frac=['Fe3+','Fe2+']
MO.fractionation(MO.to_frac)
MO.setParametrization('Fe3+/FeT',
                      lambda var:1/(1+1/(var['Fe3+_liquid']/var['Fe2+_liquid']*Fe_ferrous.molecular_mass/Fe_ferric.molecular_mass)),
                      ['Fe2+_liquid','Fe3+_liquid'],
                      ptype='scalar')


# ### Set $f_{O_2}$ parametrization from Hirschmann 2022

# In[ ]:


#MO.setParametrization('fO2_sfc',lambda var: fO2_sfc_H22(var['T_pot'],var['Fe3+_liquid']/(var['Fe3+_liquid']+var['Fe2+_liquid']),BSE),['T_pot','Fe3+_liquid','Fe2+_liquid'],is_profile=False)
def getfO2(p_bot,p,T,Fe_ratio,compo):
    if p<=p_bot:
        # Deng et al., 2020 parametrization does not work beyond 55 GPa and 4000 K
        return fO2_H22(p,T,Fe_ratio,compo)
    else:
        # We set the cumulates at IW-2, its value is not used anyway, this is
        # just to avoid spurious behaviors if trying to compute fO2 at  p >50 GPa
        return 10**(fO2_buffers_H22(p,T,compo)+2)
fO2_vec = np.vectorize(getfO2,excluded=[0,3,4])
MO.setParametrization('fO2',lambda var: fO2_vec(var['p_bot'],var['pressures'],
                                                var['temperatures'],var['Fe3+_liquid']/var['Fe2+_liquid']*Fe_ferrous.molecular_mass/Fe_ferric.molecular_mass,
                                                BSE),
                      ['p_bot','pressures','temperatures','Fe3+_liquid','Fe2+_liquid'],ptype='profile')

# Surface Delta IW (to feed in speciation solver)
MO.setParametrization('DeltaIW_sfc',
                      lambda var:fO2_buffers_H22(var['p_sfc'],var['T_pot'],BSE)-np.log10(var['fO2_sfc']),
                      ['p_sfc','T_pot','fO2_sfc'],
                      ptype='scalar')


# ## Volatiles

# ### Model parameters

# In[ ]:


EO_mass             = 1.4e21  # [kg]

# non dimensional study parameters
H_budet_EO  = 3   # for the BSE, not just the MO
C2H_by_mass = 0.5

# bulk masses of volatiles in the system
H_mass_in_MO = H_budet_EO*EO_mass/BSE_mass*H2O.coefs[H]*H.atomic_mass/H2O.molecular_mass*MO.M_system
C_mass_in_MO = H_mass_in_MO*C2H_by_mass

# ## Atmosphere

# ### Create atmosphere

# In[ ]:


#MO.atm = atmosphere([],[],MO.adiabat.T_pot-10,pToA=1)
MO.atm=multi_species_single_condensible_convective_atmosphere(np.array([H2,H2O,CO,CO2,CH4,N2,NH3,HCN,SH2,S2,SO2]),
                                                              np.array([1e5,1e5,1e5,1e5,1e5,1e5,1e5,1e5,1e5,1e5,1e5]),
                                                              MO.adiabat.T_pot-10,R=MO.R_out,g=MO.gravity,
                                                              pToA=1,cond=H2O,T_str=200.)
MO.addBoundaryLayer()

# ### Load reference composition

MO.setupChemistry({H2:H_budet_EO*EO_mass/MO.M_system,
                   CO:C2H_by_mass*H_budet_EO*EO_mass/MO.M_system*CO.molecular_mass/C.atomic_mass,
                   N2:1e-6,
                   S2:1e-6})
MO.speciationInit()


#def flux_residual(MO,T):
#    MO.updateBL(T)
#    MO.atm.updateTs(T)
#    MO.updateParam('OLR')
#    return MO.BL.getFlux()+stellar_irradiation-MO.scalar['OLR']
#MO.flux_residual = flux_residual
#
#
## ## Initialize
#
## In[ ]:
#
#
#MO.dEth_dTpot=8.34e+27
#MO.RK4_step(0.)
## Don't mind the warning


