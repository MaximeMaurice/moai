from physics.constants import Earth_gravity,Earth_surface_radius
from physics.eos import *
from physics.phase_change.refractories import mc_book
from magma_ocean import magma_ocean

# MO object
MO = magma_ocean(3000,
                 135e9,
                 eos={'rho':eos_book['rho']['MK19l'],'alpha':eos_book['alpha']['N19'],'cp':eos_book['cp']['cst']},
                 g=Earth_gravity,
                 R=Earth_surface_radius,
                 ConvCum=True)

# Melting curves
MO.setMeltingCurves(mc_book['Earth'],RCMF=0.4)

# Boundary layer
from physics import viscosity
MO.setParametrization('viscosity',lambda var: viscosity.viscosity_book['VFT'](var['pressures'],var['temperatures']),['temperatures','pressures'],ptype='profile')
from physics.constants import thermal_diffusivity
MO.setParametrization('k',lambda var: thermal_diffusivity*var['rho']*var['cp'],['rho','cp'],ptype='profile')

# Atmosphere
from atmospheres import radiative_grey_atmosphere
MO.atm = radiative_grey_atmosphere([],[],MO.adiabat.T_pot,g=Earth_gravity,R=Earth_surface_radius)
MO.addBoundaryLayer()

from chemistry.molecules import H2O
MO.addSpecies([H2O],[EO_mass/BSE_mass],volatile=True,reg_elems=False)
MO.fractionation(['H2O'])
MO.to_frac = ['H2O']

# Planetary heat flux
from scipy.constants import Stefan_Boltzmann
def flux_residual(MO,T):
    MO.updateBL(T)
    MO.atm.updateTs(T)
    return MO.BL.getFlux()-(MO.atm.getOLR()-MO.atm.getASR())
MO.flux_residual = flux_residual

# Secular cooling
MO.dEth_dTpot=8.34e+27

# Initialization
MO.RK4_step(0.)
