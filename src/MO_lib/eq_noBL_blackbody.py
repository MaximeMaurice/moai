from physics.constants import Earth_gravity,Earth_surface_radius
from physics.eos import *
from physics.phase_change.refractories import mc_book
from magma_ocean import magma_ocean

MO = magma_ocean(3000,
                 135e9,
                 eos={'rho':eos_book['rho']['MK19l'],'alpha':eos_book['alpha']['N19'],'cp':eos_book['cp']['cst']},
                 g=Earth_gravity,
                 R=Earth_surface_radius,
                 ConvCum=True)

MO.setMeltingCurves(mc_book['Earth'],RCMF=0.4)

from atmospheres import black_body
MO.atm = black_body([],[],MO.adiabat.T_pot,g=Earth_gravity,R=Earth_surface_radius)

def update_Ts(var):
    var['atm'].updateTs(var['T_pot'])

MO.setParametrization('update Ts',update_Ts,['atm','T_pot'],ptype='action')
