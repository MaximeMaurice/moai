from physics.constants import Earth_gravity,Earth_surface_radius, EO_mass, BSE_mass
from physics.eos import *
from physics.phase_change.refractories import mc_book
from magma_ocean import magma_ocean

MO = magma_ocean(3000,
                 135e9,
                 eos={'rho':eos_book['rho']['MK19l'],'alpha':eos_book['alpha']['N19'],'cp':eos_book['cp']['cst']},
                 g=Earth_gravity,
                 R=Earth_surface_radius,
                 ConvCum=True)

MO.setMeltingCurves(mc_book['Earth'],RCMF=0.4)

from atmospheres import radiative_grey_atmosphere
MO.atm = radiative_grey_atmosphere([],[],MO.adiabat.T_pot,g=Earth_gravity,R=Earth_surface_radius)

from chemistry.molecules import H2O
MO.addSpecies([H2O],[EO_mass/BSE_mass],volatile=True,reg_elems=False)
MO.fractionation(['H2O'])
MO.to_frac = ['H2O']

def update_Ts(var):
    var['atm'].updateTs(var['T_pot'])

MO.setParametrization('update Ts',update_Ts,['atm','T_pot'],ptype='action')
