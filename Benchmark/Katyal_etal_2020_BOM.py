#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  8 16:24:48 2022

@author: maxime
"""

from physics import *
from chemistry import *
from classes import *

Ts = 3000. # [K]
ps = 76.7e5

#### Scenario 1.1
p_boA={'IW-4':48e5,'IW':53.2e5,'IW+4':75.3e5}
x_CO2={'IW-4':0.0018,'IW':0.15,'IW+4':0.90}
x_H2O={'IW-4':0.0007,'IW':0.029,'IW+4':0.049}
x_H2 ={'IW-4':0.049,'IW':0.02,'IW+4':0.00}
x_CO ={'IW-4':0.94,'IW':0.79,'IW+4':0.046}

plt.figure()

for buffer in ['IW-4','IW','IW+4']:
    atm = convective_atmosphere(np.array([H2O,H2,CO2,CO]),\
                                np.array([x_H2O[buffer],x_H2[buffer],x_CO2[buffer],x_CO[buffer]])*p_boA[buffer],\
                                Ts,T_str=200.,compute_HE=True)
    plt.subplot(121)
    plt.semilogy(atm.profiles['temperature'],atm.profiles['pressure'],label=buffer)
    plt.subplot(122)
    plt.plot(atm.profiles['temperature'],atm.profiles['altitude']*1e-3,label=buffer)
    
plt.subplot(121)
plt.semilogy(H2O.T_dew(np.logspace(8,-1)),np.logspace(8,-1),'--',label=r'Psat')
plt.xlabel('Temperature [K]')
plt.ylabel('Pressure [Pa]')
plt.ylim(1e8,1e-1)
plt.legend()

plt.subplot(122)
plt.xlabel('Temperature [K]')
plt.ylabel('Altitude [km]')
plt.legend()
plt.title('Scenario 1.1')

#### Scenario 1.2
p_boA={'IW-4':27.1e5,'IW':51e5,'IW+4':75.8e5}
x_CO2={'IW-4':0.0,'IW':0.040,'IW+4':0.24}
x_H2O={'IW-4':0.01,'IW':0.44,'IW+4':0.74}
x_H2 ={'IW-4':0.74,'IW':0.30,'IW+4':0.005}
x_CO ={'IW-4':0.25,'IW':0.20,'IW+4':0.0122}

plt.figure()

for buffer in ['IW-4','IW','IW+4']:
    atm = convective_atmosphere(np.array([H2O,H2,CO2,CO]),\
                                np.array([x_H2O[buffer],x_H2[buffer],x_CO2[buffer],x_CO[buffer]])*p_boA[buffer],\
                                Ts,T_str=200.,compute_HE=True)
    plt.subplot(121)
    plt.semilogy(atm.profiles['temperature'],atm.profiles['pressure'],label=buffer)
    plt.subplot(122)
    plt.plot(atm.profiles['temperature'],atm.profiles['altitude']*1e-3,label=buffer)
    
plt.subplot(121)
plt.semilogy(H2O.T_dew(np.logspace(8,-1)),np.logspace(8,-1),'--',label=r'Psat')
plt.xlabel('Temperature [K]')
plt.ylabel('Pressure [Pa]')
plt.ylim(1e8,1e-1)
plt.legend()

plt.subplot(122)
plt.xlabel('Temperature [K]')
plt.ylabel('Altitude [km]')
plt.legend()
plt.title('Scenario 1.2')

#### Scenario 1.3
p_boA={'IW-4':9.5e5,'IW':49.1e5,'IW+4':76.2e5}
x_CO2={'IW-4':0.0,'IW':0.0,'IW+4':0.0}
x_H2O={'IW-4':0.014,'IW':0.60,'IW+4':0.99}
x_H2 ={'IW-4':0.98,'IW':0.40,'IW+4':0.0069}
x_CO ={'IW-4':0.0,'IW':0.0,'IW+4':0.0}

plt.figure()

for buffer in ['IW-4','IW','IW+4']:
    atm = convective_atmosphere(np.array([H2O,H2,CO2,CO]),\
                                np.array([x_H2O[buffer],x_H2[buffer],x_CO2[buffer],x_CO[buffer]])*p_boA[buffer],\
                                Ts,T_str=200.,compute_HE=True)
    plt.subplot(121)
    plt.semilogy(atm.profiles['temperature'],atm.profiles['pressure'],label=buffer)
    plt.subplot(122)
    plt.plot(atm.profiles['temperature'],atm.profiles['altitude']*1e-3,label=buffer)
    
plt.subplot(121)
plt.semilogy(H2O.T_dew(np.logspace(8,-1)),np.logspace(8,-1),'--',label=r'Psat')
plt.xlabel('Temperature [K]')
plt.ylabel('Pressure [Pa]')
plt.ylim(1e8,1e-1)
plt.legend()

plt.subplot(122)
plt.xlabel('Temperature [K]')
plt.ylabel('Altitude [km]')
plt.legend()
plt.title('Scenario 1.3')