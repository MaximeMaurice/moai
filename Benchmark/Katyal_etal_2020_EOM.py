#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  8 16:24:48 2022

@author: maxime
"""

from physics import *
from chemistry import *
from classes import *

Ts = 1650. # [K]
ps = 395e5

#### Scenario 1.1
p_boA={'IW-4':247.5e5,'IW':283.7e5,'IW+4':390.4e5}
x_CO2={'IW-4':0.003,'IW':0.22,'IW+4':0.92}
x_H2O={'IW-4':0.0005,'IW':0.025,'IW+4':0.05}
x_H2 ={'IW-4':0.049,'IW':0.025,'IW+4':0.00}
x_CO ={'IW-4':0.94,'IW':0.79,'IW+4':0.03}

plt.figure()

for buffer in ['IW-4','IW','IW+4']:
    atm = convective_atmosphere(np.array([H2O,H2,CO2,CO]),\
                                np.array([x_H2O[buffer],x_H2[buffer],x_CO2[buffer],x_CO[buffer]])*p_boA[buffer],\
                                Ts,T_str=200.,compute_HE=True)
    plt.subplot(121)
    plt.semilogy(atm.profiles['temperature'],atm.profiles['pressure'],label=buffer)
    plt.subplot(122)
    plt.plot(atm.profiles['temperature'],atm.profiles['altitude']*1e-3,label=buffer)
    
plt.subplot(121)
plt.semilogy(H2O.T_dew(np.logspace(8,-1)),np.logspace(8,-1),'--',label=r'Psat')
plt.xlabel('Temperature [K]')
plt.ylabel('Pressure [Pa]')
plt.ylim(1e8,1e-1)
plt.legend()

plt.subplot(122)
plt.xlabel('Temperature [K]')
plt.ylabel('Altitude [km]')
plt.legend()
plt.title('Scenario 1.1')

#### Scenario 1.2
p_boA={'IW-4':139e5,'IW':248e5,'IW+4':391e5}
x_CO2={'IW-4':0.0007,'IW':0.058,'IW+4':0.24}
x_H2O={'IW-4':0.007,'IW':0.37,'IW+4':0.74}
x_H2 ={'IW-4':0.74,'IW':0.37,'IW+4':0.007}
x_CO ={'IW-4':0.25,'IW':0.19,'IW+4':0.007}

plt.figure()

for buffer in ['IW-4','IW','IW+4']:
    atm = convective_atmosphere(np.array([H2O,H2,CO2,CO]),\
                                np.array([x_H2O[buffer],x_H2[buffer],x_CO2[buffer],x_CO[buffer]])*p_boA[buffer],\
                                Ts,T_str=200.,compute_HE=True)
    plt.subplot(121)
    plt.semilogy(atm.profiles['temperature'],atm.profiles['pressure'],label=buffer)
    plt.subplot(122)
    plt.plot(atm.profiles['temperature'],atm.profiles['altitude']*1e-3,label=buffer)
    
plt.subplot(121)
plt.semilogy(H2O.T_dew(np.logspace(8,-1)),np.logspace(8,-1),'--',label=r'Psat')
plt.xlabel('Temperature [K]')
plt.ylabel('Pressure [Pa]')
plt.ylim(1e8,1e-1)
plt.legend()

plt.subplot(122)
plt.xlabel('Temperature [K]')
plt.ylabel('Altitude [km]')
plt.legend()
plt.title('Scenario 1.2')

#### Scenario 1.3
p_boA={'IW-4':47.3e5,'IW':219e5,'IW+4':391.5e5}
x_CO2={'IW-4':0.0,'IW':0.0,'IW+4':0.0}
x_H2O={'IW-4':0.009,'IW':0.50,'IW+4':0.99}
x_H2 ={'IW-4':0.99,'IW':0.50,'IW+4':0.0099}
x_CO ={'IW-4':0.0,'IW':0.0,'IW+4':0.0}

plt.figure()

for buffer in ['IW-4','IW','IW+4']:
    atm = convective_atmosphere(np.array([H2O,H2,CO2,CO]),\
                                np.array([x_H2O[buffer],x_H2[buffer],x_CO2[buffer],x_CO[buffer]])*p_boA[buffer],\
                                Ts,T_str=200.,compute_HE=True)
    plt.subplot(121)
    plt.semilogy(atm.profiles['temperature'],atm.profiles['pressure'],label=buffer)
    plt.subplot(122)
    plt.plot(atm.profiles['temperature'],atm.profiles['altitude']*1e-3,label=buffer)
    
plt.subplot(121)
plt.semilogy(H2O.T_dew(np.logspace(8,-1)),np.logspace(8,-1),'--',label=r'Psat')
plt.xlabel('Temperature [K]')
plt.ylabel('Pressure [Pa]')
plt.ylim(1e8,1e-1)
plt.legend()

plt.subplot(122)
plt.xlabel('Temperature [K]')
plt.ylabel('Altitude [km]')
plt.legend()
plt.title('Scenario 1.3')