Installation
============

Obtaining MOAI
--------------

To get MOAI, simply clone the git repository:

.. code-block:: console

   git clone https://MaximeMaurice@bitbucket.org/MaximeMaurice/MOAI.git
   
Dependencies
------------

You will need some common python packages to run MOAI, like numpy, scipy, matplotlib or pandas.   

You can run MOAI as a stand-alone mode, but the real fun comes when you start using other tools, to build more sophisticated models.   
A couple of examples of python packages that are used to complement MOA (`petitRADTRANS <https://petitradtrans.readthedocs.io/en/latest/>`_ and `burnman <https://geodynamics.github.io/burnman/>`_) are introduced in the BONUS section: check it out!