Moai
====

While MOAI stands for Magma Ocean-Atmosphere Interface, the name has Moai is well known and has a long story. Moais (or mo'ais, or mōais) are statues erected on Easter island between the 15th and the 17th century CE, by the Rapa Nui people. They have been carves from the flanks of the Rano Raraku volcano, from tuff () More information on them can be found `here <https://www.easterisland.travel/easter-island-facts-and-info/moai-statues>`_.   

.. image:: moais.jpg
image from https://www.easterisland.travel/easter-island-facts-and-info/moai-statues