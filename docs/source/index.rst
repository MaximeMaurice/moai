.. MagmaOceanModel documentation master file, created by
   sphinx-quickstart on Mon Oct  3 14:50:21 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MOAI's documentation!
================================

MOAI (Magma Ocean-Atmosphere Interface) is a thermo-chemical evolution model for magma ocean crystallization and atmosphere outgassing, developed by Maxime Maurice and used in Maurice *et al*., 202X. As its name suggests, it is an interface. It is both an interface between magma oceans and atmospheres, where a lot of interesting physics happens, and a python interface, which lets you model this physics on your laptop.

Feel free to use it and extend it, and don't forget to cite the paper!

.. toctree::
   :maxdepth: 1
   :caption: Foreword

   foreword/MOAI
   foreword/installation

.. toctree::
   :maxdepth: 1
   :caption: Tutorial
   
   notebooks/My_first_MO.ipynb
   notebooks/Equilibrium_vs_Fractional.ipynb
   notebooks/Thermal_evolution.ipynb
   notebooks/Fractionation.ipynb
   notebooks/Speciation.ipynb
   notebooks/Parametrizations.ipynb
   notebooks/Atmospheres.ipynb

.. toctree::
   :maxdepth: 1
   :caption: Bonus
   
   notebooks/Burnman_EOS.ipynb
   notebooks/petitRADTRANS.ipynb

.. toctree::
   :maxdepth: 1
   :caption: Publications setups
   
   notebooks/Maurice_etal_PSJ_2023.ipynb
   
.. toctree::
   :maxdepth: 1
   :caption: Auto-generated stuff
