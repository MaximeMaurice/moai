:py:mod:`eq_full`
=================

.. py:module:: eq_full


Module Contents
---------------


Functions
~~~~~~~~~

.. autoapisummary::

   eq_full.eta_mix
   eq_full.set_Fe_ferric_part_coef
   eq_full.molar_ratio_to_mass_frac
   eq_full.getfO2
   eq_full.K_H
   eq_full.K_C
   eq_full.update_rad_pres
   eq_full.getOLR
   eq_full.flux_residual



Attributes
~~~~~~~~~~

.. autoapisummary::

   eq_full.albedo
   eq_full.faint_young_sun
   eq_full.solar_const
   eq_full.stellar_irradiation
   eq_full.R_planet
   eq_full.g_planet
   eq_full.BSE_mass
   eq_full.EO_mass
   eq_full.T_pot
   eq_full.p_CMB
   eq_full.BSE
   eq_full.rho
   eq_full.alpha
   eq_full.cp
   eq_full.rho_vec
   eq_full.alpha_vec
   eq_full.cp_vec
   eq_full.MO
   eq_full.z_eq
   eq_full.p_eq
   eq_full.T_eq
   eq_full.part_coef
   eq_full.initial_molar_ferric_to_ferrous_iron_liq
   eq_full.total_iron_oxide_mass_fraction_BSE
   eq_full.to_frac
   eq_full.fO2_vec
   eq_full.to_spec
   eq_full.EO_mass
   eq_full.H_budet_EO
   eq_full.C2H_by_mass
   eq_full.H_mass_in_MO
   eq_full.C_mass_in_MO
   eq_full.pp0
   eq_full.atm
   eq_full.atm
   eq_full.equilibria
   eq_full.species_order
   eq_full.radiation
   eq_full.getOLR
   eq_full.flux_residual
   eq_full.dEth_dTpot


.. py:data:: albedo
   :annotation: = 0.2

   

.. py:data:: faint_young_sun
   :annotation: = 0.75

   

.. py:data:: solar_const
   :annotation: = 1367.6

   

.. py:data:: stellar_irradiation
   

   

.. py:data:: R_planet
   :annotation: = 6371000

   

.. py:data:: g_planet
   :annotation: = 9.798

   

.. py:data:: BSE_mass
   :annotation: = 4e+24

   

.. py:data:: EO_mass
   :annotation: = 1.4e+21

   

.. py:data:: T_pot
   :annotation: = 3003

   

.. py:data:: p_CMB
   :annotation: = 135000000000.0

   

.. py:data:: BSE
   

   

.. py:data:: rho
   

   

.. py:data:: alpha
   

   

.. py:data:: cp
   

   

.. py:data:: rho_vec
   

   

.. py:data:: alpha_vec
   

   

.. py:data:: cp_vec
   

   

.. py:data:: MO
   

   

.. py:function:: eta_mix(p, T, phi)


.. py:data:: z_eq
   :annotation: = sfc

   

.. py:data:: p_eq
   

   

.. py:data:: T_eq
   

   

.. py:data:: part_coef
   :annotation: = 0.94

   

.. py:function:: set_Fe_ferric_part_coef(MO)


.. py:data:: initial_molar_ferric_to_ferrous_iron_liq
   

   

.. py:data:: total_iron_oxide_mass_fraction_BSE
   

   

.. py:function:: molar_ratio_to_mass_frac(molar_ratio, total_mass_frac=total_iron_oxide_mass_fraction_BSE)


.. py:data:: to_frac
   :annotation: = ['Fe3+', 'Fe2+']

   

.. py:function:: getfO2(p_bot, p, T, Fe_ratio, compo)


.. py:data:: fO2_vec
   

   

.. py:function:: K_H(variables)


.. py:function:: K_C(variables)


.. py:data:: to_spec
   :annotation: = ['H', 'C']

   

.. py:data:: EO_mass
   :annotation: = 1.4e+21

   

.. py:data:: H_budet_EO
   :annotation: = 3

   

.. py:data:: C2H_by_mass
   :annotation: = 0.5

   

.. py:data:: H_mass_in_MO
   

   

.. py:data:: C_mass_in_MO
   

   

.. py:data:: pp0
   

   

.. py:data:: atm
   

   

.. py:data:: atm
   

   

.. py:data:: equilibria
   

   

.. py:data:: species_order
   

   

.. py:data:: radiation
   

   

.. py:function:: update_rad_pres(var)


.. py:function:: getOLR()


.. py:data:: getOLR
   

   

.. py:function:: flux_residual(MO, T)


.. py:data:: flux_residual
   

   

.. py:data:: dEth_dTpot
   :annotation: = 8.34e+27

   

