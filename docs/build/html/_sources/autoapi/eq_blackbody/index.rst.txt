:py:mod:`eq_blackbody`
======================

.. py:module:: eq_blackbody


Module Contents
---------------


Functions
~~~~~~~~~

.. autoapisummary::

   eq_blackbody.flux_residual



Attributes
~~~~~~~~~~

.. autoapisummary::

   eq_blackbody.MO
   eq_blackbody.atm
   eq_blackbody.flux_residual
   eq_blackbody.dEth_dTpot


.. py:data:: MO
   

   

.. py:data:: atm
   

   

.. py:function:: flux_residual(MO, T)


.. py:data:: flux_residual
   

   

.. py:data:: dEth_dTpot
   :annotation: = 8.34e+27

   

