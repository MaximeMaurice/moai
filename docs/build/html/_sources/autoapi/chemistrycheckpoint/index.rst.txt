:py:mod:`chemistry-checkpoint`
==============================

.. py:module:: chemistry-checkpoint

.. autoapi-nested-parse::

   Created on Wed Jul 28 10:03:34 2021

   @author: mm167



Module Contents
---------------

Classes
~~~~~~~

.. autoapisummary::

   chemistry-checkpoint.element
   chemistry-checkpoint.species
   chemistry-checkpoint.equilibrium



Functions
~~~~~~~~~

.. autoapisummary::

   chemistry-checkpoint.cp_Shomate_single_val
   chemistry-checkpoint.cp_Wagner_Pruss_single_val
   chemistry-checkpoint.psat
   chemistry-checkpoint.T_dew



Attributes
~~~~~~~~~~

.. autoapisummary::

   chemistry-checkpoint.H
   chemistry-checkpoint.C
   chemistry-checkpoint.N
   chemistry-checkpoint.O
   chemistry-checkpoint.Cl
   chemistry-checkpoint.Fe
   chemistry-checkpoint.S
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.Fe3plus
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.volatile
   chemistry-checkpoint.symbol
   chemistry-checkpoint.name
   chemistry-checkpoint.FeO
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.volatile
   chemistry-checkpoint.symbol
   chemistry-checkpoint.name
   chemistry-checkpoint.elem_dict
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.JANAF
   chemistry-checkpoint.cp_NATAF
   chemistry-checkpoint.T_Shomate
   chemistry-checkpoint.A_Shomate
   chemistry-checkpoint.B_Shomate
   chemistry-checkpoint.C_Shomate
   chemistry-checkpoint.D_Shomate
   chemistry-checkpoint.E_Shomate
   chemistry-checkpoint.cp_Shomate
   chemistry-checkpoint.cp_Wagner_Pruss
   chemistry-checkpoint.H2
   chemistry-checkpoint.Henry_law
   chemistry-checkpoint.alpha
   chemistry-checkpoint.beta
   chemistry-checkpoint.psat
   chemistry-checkpoint.molecular_mass
   chemistry-checkpoint.cp
   chemistry-checkpoint.cp_mass
   chemistry-checkpoint.cp_mol
   chemistry-checkpoint.cp_cond
   chemistry-checkpoint.L_mass
   chemistry-checkpoint.L_mol
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.k0
   chemistry-checkpoint.p0
   chemistry-checkpoint.T_dew
   chemistry-checkpoint.fastchemfor
   chemistry-checkpoint.O2
   chemistry-checkpoint.gas
   chemistry-checkpoint.psat
   chemistry-checkpoint.T_dew
   chemistry-checkpoint.molecular_mass
   chemistry-checkpoint.cp
   chemistry-checkpoint.cp_mass
   chemistry-checkpoint.cp_mol
   chemistry-checkpoint.L_mass
   chemistry-checkpoint.L_mol
   chemistry-checkpoint.fastchemfor
   chemistry-checkpoint.H2O
   chemistry-checkpoint.Henry_law
   chemistry-checkpoint.alpha
   chemistry-checkpoint.beta
   chemistry-checkpoint.psat
   chemistry-checkpoint.molecular_mass
   chemistry-checkpoint.cp
   chemistry-checkpoint.cp_mass
   chemistry-checkpoint.cp_mol
   chemistry-checkpoint.cp_cond
   chemistry-checkpoint.L_mass
   chemistry-checkpoint.L_mol
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.k0
   chemistry-checkpoint.p0
   chemistry-checkpoint.T_dew
   chemistry-checkpoint.fastchemfor
   chemistry-checkpoint.CH4
   chemistry-checkpoint.Henry_law
   chemistry-checkpoint.alpha
   chemistry-checkpoint.beta
   chemistry-checkpoint.psat
   chemistry-checkpoint.cp_mol
   chemistry-checkpoint.molecular_mass
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.k0
   chemistry-checkpoint.p0
   chemistry-checkpoint.T_dew
   chemistry-checkpoint.fastchemfor
   chemistry-checkpoint.CO
   chemistry-checkpoint.Henry_law
   chemistry-checkpoint.alpha
   chemistry-checkpoint.beta
   chemistry-checkpoint.psat
   chemistry-checkpoint.molecular_mass
   chemistry-checkpoint.cp
   chemistry-checkpoint.cp_mass
   chemistry-checkpoint.cp_mol
   chemistry-checkpoint.cp_cond
   chemistry-checkpoint.L_mass
   chemistry-checkpoint.L_mol
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.k0
   chemistry-checkpoint.p0
   chemistry-checkpoint.T_dew
   chemistry-checkpoint.fastchemfor
   chemistry-checkpoint.CO2
   chemistry-checkpoint.Henry_law
   chemistry-checkpoint.alpha
   chemistry-checkpoint.beta
   chemistry-checkpoint.psat
   chemistry-checkpoint.molecular_mass
   chemistry-checkpoint.cp
   chemistry-checkpoint.cp_mass
   chemistry-checkpoint.cp_mol
   chemistry-checkpoint.cp_cond
   chemistry-checkpoint.L_mass
   chemistry-checkpoint.L_mol
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.k0
   chemistry-checkpoint.p0
   chemistry-checkpoint.T_dew
   chemistry-checkpoint.fastchemfor
   chemistry-checkpoint.N2
   chemistry-checkpoint.Henry_law
   chemistry-checkpoint.alpha
   chemistry-checkpoint.beta
   chemistry-checkpoint.psat
   chemistry-checkpoint.molecular_mass
   chemistry-checkpoint.cp
   chemistry-checkpoint.cp_mass
   chemistry-checkpoint.cp_mol
   chemistry-checkpoint.cp_cond
   chemistry-checkpoint.L_mass
   chemistry-checkpoint.L_mol
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.k0
   chemistry-checkpoint.p0
   chemistry-checkpoint.T_dew
   chemistry-checkpoint.fastchemfor
   chemistry-checkpoint.NH3
   chemistry-checkpoint.Henry_law
   chemistry-checkpoint.alpha
   chemistry-checkpoint.beta
   chemistry-checkpoint.psat
   chemistry-checkpoint.molecular_mass
   chemistry-checkpoint.cp
   chemistry-checkpoint.cp_mass
   chemistry-checkpoint.cp_mol
   chemistry-checkpoint.cp_cond
   chemistry-checkpoint.L_mass
   chemistry-checkpoint.L_mol
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.k0
   chemistry-checkpoint.p0
   chemistry-checkpoint.T_dew
   chemistry-checkpoint.fastchemfor
   chemistry-checkpoint.HCN
   chemistry-checkpoint.Henry_law
   chemistry-checkpoint.alpha
   chemistry-checkpoint.beta
   chemistry-checkpoint.cp
   chemistry-checkpoint.cp_mass
   chemistry-checkpoint.cp_mol
   chemistry-checkpoint.cp_cond
   chemistry-checkpoint.molecular_mass
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.k0
   chemistry-checkpoint.p0
   chemistry-checkpoint.fastchemfor
   chemistry-checkpoint.SH2
   chemistry-checkpoint.S2
   chemistry-checkpoint.SO2
   chemistry-checkpoint.HCl
   chemistry-checkpoint.species_dict
   chemistry-checkpoint.eq_H
   chemistry-checkpoint.eq_C
   chemistry-checkpoint.eq_CH
   chemistry-checkpoint.eq_N1
   chemistry-checkpoint.eq_N2
   chemistry-checkpoint.eq_C2
   chemistry-checkpoint.eq_S1
   chemistry-checkpoint.eq_S2
   chemistry-checkpoint.equilibria_dict
   chemistry-checkpoint.half_life_U235
   chemistry-checkpoint.heat_U235
   chemistry-checkpoint.conc_U235
   chemistry-checkpoint.U235
   chemistry-checkpoint.volatile
   chemistry-checkpoint.formula
   chemistry-checkpoint.name
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.radio
   chemistry-checkpoint.heat
   chemistry-checkpoint.half_life_U238
   chemistry-checkpoint.heat_U238
   chemistry-checkpoint.conc_U238
   chemistry-checkpoint.U238
   chemistry-checkpoint.volatile
   chemistry-checkpoint.formula
   chemistry-checkpoint.name
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.radio
   chemistry-checkpoint.heat
   chemistry-checkpoint.half_life_Th232
   chemistry-checkpoint.heat_Th232
   chemistry-checkpoint.conc_Th232
   chemistry-checkpoint.Th232
   chemistry-checkpoint.volatile
   chemistry-checkpoint.formula
   chemistry-checkpoint.name
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.radio
   chemistry-checkpoint.heat
   chemistry-checkpoint.half_life_K40
   chemistry-checkpoint.heat_K40
   chemistry-checkpoint.conc_K40
   chemistry-checkpoint.K40
   chemistry-checkpoint.volatile
   chemistry-checkpoint.formula
   chemistry-checkpoint.name
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.radio
   chemistry-checkpoint.heat
   chemistry-checkpoint.half_life_Al26
   chemistry-checkpoint.heat_Al26
   chemistry-checkpoint.conc_Al26
   chemistry-checkpoint.Al26
   chemistry-checkpoint.volatile
   chemistry-checkpoint.formula
   chemistry-checkpoint.name
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.radio
   chemistry-checkpoint.heat
   chemistry-checkpoint.half_life_Fe60
   chemistry-checkpoint.heat_Fe60
   chemistry-checkpoint.conc_Fe60
   chemistry-checkpoint.Fe60
   chemistry-checkpoint.volatile
   chemistry-checkpoint.formula
   chemistry-checkpoint.name
   chemistry-checkpoint.part_coef
   chemistry-checkpoint.radio
   chemistry-checkpoint.heat


.. py:class:: element(symbol, atomic_mass)


.. py:data:: H
   

   

.. py:data:: C
   

   

.. py:data:: N
   

   

.. py:data:: O
   

   

.. py:data:: Cl
   

   

.. py:data:: Fe
   

   

.. py:data:: S
   

   

.. py:data:: part_coef
   :annotation: = 0.01

   

.. py:data:: part_coef
   :annotation: = 0.001

   

.. py:data:: part_coef
   :annotation: = 0.0

   

.. py:data:: Fe3plus
   

   

.. py:data:: part_coef
   :annotation: = 0.2

   

.. py:data:: volatile
   :annotation: = False

   

.. py:data:: symbol
   :annotation: = Fe3+

   

.. py:data:: name
   :annotation: = Fe3+

   

.. py:data:: FeO
   

   

.. py:data:: part_coef
   :annotation: = 0.9

   

.. py:data:: volatile
   :annotation: = False

   

.. py:data:: symbol
   :annotation: = FeO

   

.. py:data:: name
   :annotation: = FeO

   

.. py:data:: elem_dict
   

   

.. py:data:: part_coef
   :annotation: = 0.0062085

   

.. py:data:: part_coef
   :annotation: = 0.0009301

   

.. py:data:: JANAF
   

   

.. py:data:: cp_NATAF
   

   

.. py:data:: T_Shomate
   

   

.. py:data:: A_Shomate
   

   

.. py:data:: B_Shomate
   

   

.. py:data:: C_Shomate
   

   

.. py:data:: D_Shomate
   

   

.. py:data:: E_Shomate
   

   

.. py:function:: cp_Shomate_single_val(T, **kw)


.. py:data:: cp_Shomate
   

   

.. py:function:: cp_Wagner_Pruss_single_val(T)


.. py:data:: cp_Wagner_Pruss
   

   

.. py:class:: species(formula, name='NA')


.. py:function:: psat(T, gaz)


.. py:function:: T_dew(p, gas)


.. py:data:: H2
   

   

.. py:data:: Henry_law
   

   

.. py:data:: alpha
   :annotation: = 5e-12

   

.. py:data:: beta
   :annotation: = 1.0

   

.. py:data:: psat
   

   

.. py:data:: molecular_mass
   

   

.. py:data:: cp
   

   

.. py:data:: cp_mass
   

   

.. py:data:: cp_mol
   

   

.. py:data:: cp_cond
   

   

.. py:data:: L_mass
   

   

.. py:data:: L_mol
   

   

.. py:data:: part_coef
   :annotation: = 1e-06

   

.. py:data:: k0
   :annotation: = 5e-05

   

.. py:data:: p0
   :annotation: = 101325.0

   

.. py:data:: T_dew
   

   

.. py:data:: fastchemfor
   :annotation: = H2

   

.. py:data:: O2
   

   

.. py:data:: gas
   

   

.. py:data:: psat
   

   

.. py:data:: T_dew
   

   

.. py:data:: molecular_mass
   :annotation: = 32.0

   

.. py:data:: cp
   

   

.. py:data:: cp_mass
   

   

.. py:data:: cp_mol
   

   

.. py:data:: L_mass
   

   

.. py:data:: L_mol
   

   

.. py:data:: fastchemfor
   :annotation: = O2

   

.. py:data:: H2O
   

   

.. py:data:: Henry_law
   

   

.. py:data:: alpha
   

   

.. py:data:: beta
   

   

.. py:data:: psat
   

   

.. py:data:: molecular_mass
   

   

.. py:data:: cp
   

   

.. py:data:: cp_mass
   

   

.. py:data:: cp_mol
   

   

.. py:data:: cp_cond
   

   

.. py:data:: L_mass
   

   

.. py:data:: L_mol
   

   

.. py:data:: part_coef
   :annotation: = 0.00025

   

.. py:data:: k0
   :annotation: = 0.01

   

.. py:data:: p0
   :annotation: = 101325.0

   

.. py:data:: T_dew
   

   

.. py:data:: fastchemfor
   :annotation: = H2O1

   

.. py:data:: CH4
   

   

.. py:data:: Henry_law
   

   

.. py:data:: alpha
   :annotation: = 2.2e-13

   

.. py:data:: beta
   :annotation: = 1.0

   

.. py:data:: psat
   

   

.. py:data:: cp_mol
   

   

.. py:data:: molecular_mass
   

   

.. py:data:: part_coef
   :annotation: = 0.00025

   

.. py:data:: k0
   :annotation: = 1.2e-05

   

.. py:data:: p0
   :annotation: = 101325.0

   

.. py:data:: T_dew
   

   

.. py:data:: fastchemfor
   :annotation: = C1H4

   

.. py:data:: CO
   

   

.. py:data:: Henry_law
   

   

.. py:data:: alpha
   :annotation: = 5.5e-13

   

.. py:data:: beta
   :annotation: = 1.0

   

.. py:data:: psat
   

   

.. py:data:: molecular_mass
   

   

.. py:data:: cp
   

   

.. py:data:: cp_mass
   

   

.. py:data:: cp_mol
   

   

.. py:data:: cp_cond
   

   

.. py:data:: L_mass
   

   

.. py:data:: L_mol
   

   

.. py:data:: part_coef
   :annotation: = 0.00025

   

.. py:data:: k0
   :annotation: = 1.2e-05

   

.. py:data:: p0
   :annotation: = 101325.0

   

.. py:data:: T_dew
   

   

.. py:data:: fastchemfor
   :annotation: = C1O1

   

.. py:data:: CO2
   

   

.. py:data:: Henry_law
   

   

.. py:data:: alpha
   :annotation: = 1.6e-12

   

.. py:data:: beta
   :annotation: = 1.0

   

.. py:data:: psat
   

   

.. py:data:: molecular_mass
   

   

.. py:data:: cp
   

   

.. py:data:: cp_mass
   

   

.. py:data:: cp_mol
   

   

.. py:data:: cp_cond
   

   

.. py:data:: L_mass
   

   

.. py:data:: L_mol
   

   

.. py:data:: part_coef
   :annotation: = 0.005

   

.. py:data:: k0
   :annotation: = 0.0001

   

.. py:data:: p0
   :annotation: = 101325.0

   

.. py:data:: T_dew
   

   

.. py:data:: fastchemfor
   :annotation: = C1O2

   

.. py:data:: N2
   

   

.. py:data:: Henry_law
   

   

.. py:data:: alpha
   :annotation: = 1e-12

   

.. py:data:: beta
   :annotation: = 1.0

   

.. py:data:: psat
   

   

.. py:data:: molecular_mass
   

   

.. py:data:: cp
   

   

.. py:data:: cp_mass
   

   

.. py:data:: cp_mol
   

   

.. py:data:: cp_cond
   

   

.. py:data:: L_mass
   

   

.. py:data:: L_mol
   

   

.. py:data:: part_coef
   :annotation: = 0.005

   

.. py:data:: k0
   :annotation: = 0.0

   

.. py:data:: p0
   :annotation: = 101325.0

   

.. py:data:: T_dew
   

   

.. py:data:: fastchemfor
   :annotation: = N2

   

.. py:data:: NH3
   

   

.. py:data:: Henry_law
   

   

.. py:data:: alpha
   :annotation: = 5e-12

   

.. py:data:: beta
   :annotation: = 1.0

   

.. py:data:: psat
   

   

.. py:data:: molecular_mass
   

   

.. py:data:: cp
   

   

.. py:data:: cp_mass
   

   

.. py:data:: cp_mol
   

   

.. py:data:: cp_cond
   

   

.. py:data:: L_mass
   

   

.. py:data:: L_mol
   

   

.. py:data:: part_coef
   :annotation: = 0.005

   

.. py:data:: k0
   :annotation: = 0.0

   

.. py:data:: p0
   :annotation: = 101325.0

   

.. py:data:: T_dew
   

   

.. py:data:: fastchemfor
   :annotation: = N1H3

   

.. py:data:: HCN
   

   

.. py:data:: Henry_law
   

   

.. py:data:: alpha
   :annotation: = 5e-11

   

.. py:data:: beta
   :annotation: = 1.0

   

.. py:data:: cp
   

   

.. py:data:: cp_mass
   

   

.. py:data:: cp_mol
   

   

.. py:data:: cp_cond
   

   

.. py:data:: molecular_mass
   :annotation: = 27.0

   

.. py:data:: part_coef
   :annotation: = 0.005

   

.. py:data:: k0
   :annotation: = 0.0

   

.. py:data:: p0
   :annotation: = 101325.0

   

.. py:data:: fastchemfor
   :annotation: = H1C1N1

   

.. py:data:: SH2
   

   

.. py:data:: S2
   

   

.. py:data:: SO2
   

   

.. py:data:: HCl
   

   

.. py:data:: species_dict
   

   

.. py:class:: equilibrium(expression)


.. py:data:: eq_H
   

   

.. py:data:: eq_C
   

   

.. py:data:: eq_CH
   

   

.. py:data:: eq_N1
   

   

.. py:data:: eq_N2
   

   

.. py:data:: eq_C2
   

   

.. py:data:: eq_S1
   

   

.. py:data:: eq_S2
   

   

.. py:data:: equilibria_dict
   

   

.. py:data:: half_life_U235
   :annotation: = 704100000.0

   

.. py:data:: heat_U235
   :annotation: = 0.000569

   

.. py:data:: conc_U235
   :annotation: = 2.2e-10

   

.. py:data:: U235
   

   

.. py:data:: volatile
   :annotation: = False

   

.. py:data:: formula
   :annotation: = 235U

   

.. py:data:: name
   :annotation: = 235U

   

.. py:data:: part_coef
   :annotation: = 0.01

   

.. py:data:: radio
   

   

.. py:data:: heat
   

   

.. py:data:: half_life_U238
   :annotation: = 4470000000.0

   

.. py:data:: heat_U238
   :annotation: = 9.46e-05

   

.. py:data:: conc_U238
   :annotation: = 3.08e-08

   

.. py:data:: U238
   

   

.. py:data:: volatile
   :annotation: = False

   

.. py:data:: formula
   :annotation: = 238U

   

.. py:data:: name
   :annotation: = 238U

   

.. py:data:: part_coef
   :annotation: = 0.01

   

.. py:data:: radio
   

   

.. py:data:: heat
   

   

.. py:data:: half_life_Th232
   :annotation: = 14500000000.0

   

.. py:data:: heat_Th232
   :annotation: = 2.54e-05

   

.. py:data:: conc_Th232
   :annotation: = 1.24e-07

   

.. py:data:: Th232
   

   

.. py:data:: volatile
   :annotation: = False

   

.. py:data:: formula
   :annotation: = 232Th

   

.. py:data:: name
   :annotation: = 232Th

   

.. py:data:: part_coef
   :annotation: = 0.01

   

.. py:data:: radio
   

   

.. py:data:: heat
   

   

.. py:data:: half_life_K40
   :annotation: = 1250000000.0

   

.. py:data:: heat_K40
   :annotation: = 2.94e-05

   

.. py:data:: conc_K40
   :annotation: = 3.69e-08

   

.. py:data:: K40
   

   

.. py:data:: volatile
   :annotation: = False

   

.. py:data:: formula
   :annotation: = 40K

   

.. py:data:: name
   :annotation: = 40K

   

.. py:data:: part_coef
   :annotation: = 0.01

   

.. py:data:: radio
   

   

.. py:data:: heat
   

   

.. py:data:: half_life_Al26
   :annotation: = 717000.0

   

.. py:data:: heat_Al26
   :annotation: = 0.455

   

.. py:data:: conc_Al26
   :annotation: = 1.23e-06

   

.. py:data:: Al26
   

   

.. py:data:: volatile
   :annotation: = False

   

.. py:data:: formula
   :annotation: = 26Al

   

.. py:data:: name
   :annotation: = 26Al

   

.. py:data:: part_coef
   :annotation: = 0.01

   

.. py:data:: radio
   

   

.. py:data:: heat
   

   

.. py:data:: half_life_Fe60
   :annotation: = 2620000.0

   

.. py:data:: heat_Fe60
   :annotation: = 0.0412

   

.. py:data:: conc_Fe60
   :annotation: = 7.2e-10

   

.. py:data:: Fe60
   

   

.. py:data:: volatile
   :annotation: = False

   

.. py:data:: formula
   :annotation: = 60Fe

   

.. py:data:: name
   :annotation: = 60Fe

   

.. py:data:: part_coef
   :annotation: = 0.01

   

.. py:data:: radio
   

   

.. py:data:: heat
   

   

