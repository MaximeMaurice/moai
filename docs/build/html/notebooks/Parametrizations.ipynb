{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "8d55dea1-dd63-4e84-a985-2d11a59c1bb4",
   "metadata": {},
   "source": [
    "# Parametrizations\n",
    "In this chapter, we will see how to use parametrizations in MOAI. Parametrizations are at the heart of MOAI's philosophy: they are what makes it modular. They can be used for pretty much anything. They are also recursive, i.e. a parametrization can call another one, which can itself call another one... etc. When it comes to parametrizations in MOAI, your imagination is the only limit!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4d203225-71c9-45b5-8f22-bc23009499ce",
   "metadata": {},
   "source": [
    "## What are parametrizations?\n",
    "We have already used parametrizations in this tutorial (every time we called the setParametrization method of the magma ocean object), although we haven't spent much time explaining in detail what we were doing. A parametrization is simply a quantity that you want to track which is not included in the built in traced quantities of the magma ocean (like the bottom pressure or the density). You simply need to tell the magma ocean object how to calculate this quantity (what's the formula), and which other quantities you need to do the calculation. Then, this new quantity will be made available to calculate yet other new ones. Let's take an example:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7a828d19-112c-4602-9e9e-e0bbc73718c2",
   "metadata": {},
   "source": [
    "In the previous chapter (on speciation), we needed to track the oxygen fugacity ($f_{{\\rm O}_2}$). We assumed that it was set by the IW buffer, so we used an expression (from Hirschmann 2021):   \n",
    "$$\\log f_{{\\rm O}_2}=a(p)+b(p)T+c(p)T\\ln T+d(p)/T$$\n",
    "where the parameters $a$, $b$, $c$ and $d$ are each proportional to a different 3$^{\\rm rd}$ order polynomial in $p$ plus a square root term. So the $f_{{\\rm O}_2}$ depends on the temperature and pressure, which are both built-in accessible quantities of the magma ocean object. You can calculate the $f_{{\\rm O}_2}$ at the surface of the MO, using the $p_{\\rm sfc}$ and $T_{\\rm pot}$ attributes, but you can also calculate the complete $f_{{\\rm O}_2}$ profile through the MO, using the temperature profile along the adiabat. Let's now see how to implement either of these possibilities."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "61887a95-1633-42fb-8da8-13691e474aa2",
   "metadata": {},
   "source": [
    "## Setting a new parametrization"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "07c3e4ea-74bb-413e-9e29-8fb050116137",
   "metadata": {},
   "source": [
    "As usual, we first need a MO:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "55424e72-9727-4a4a-9418-f74a93bc81cf",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "No file found: calculating lookups, be patient it can take some time!\n"
     ]
    }
   ],
   "source": [
    "from MO_lib.eq_basic import MO\n",
    "MO.updateT_pot(3000)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9e019103-0ea7-4232-8fc7-01b5fc5b4345",
   "metadata": {},
   "source": [
    "### Surface $f_{{\\rm O}_2}$\n",
    "The only method you need to call to set up a parametrization is setParametrization:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "37edfce9-82f2-499b-8c1d-3a704a231c97",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.0027389957633508484\n"
     ]
    }
   ],
   "source": [
    "from chemistry.redox import fO2_buffers_H22                                       # this is the function implementing equation (1)\n",
    "MO.setParametrization('fO2_surface',                                              # name of the parametrization\n",
    "                      lambda var:10**fO2_buffers_H22(var['p_sfc'],var['T_pot']),  # the function itself; notice that it needs to be defined as a function of a dictionnary (\"var\")\n",
    "                      ['p_sfc','T_pot'],                                          # the list of variables you need to include in the argument dictionnary var\n",
    "                      is_profile=False)                                           # does th eparametrization calculate a profile? No (by default yes)\n",
    "\n",
    "# The value is store in MO.averages (for scalar parametrizations):\n",
    "print(MO.averages['fO2_surface'])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0aad412b-dc1f-4446-b13c-6544c4669775",
   "metadata": {},
   "source": [
    "Each time you update the internal state of the MO, either by calling RK4_step or simply updateT_pot (or updateState if know what you're doing), the value of fO2_surface will be updated according to the new $T_{\\rm pot}$ and $p_{\\rm sfc}$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "65fbffc5-7a76-46f1-88e2-32fe5650e473",
   "metadata": {},
   "source": [
    "### $f_{{\\rm O}_2}$ profile\n",
    "If we want to know the whole profile of $f_{{\\rm O}_2}$, it is very similar: we just need to ask the parametrization to read the pressure and temperature profiles rather than their surface value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "4cc8aeee-0892-438b-84be-fd519d19897f",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Text(0, 0.5, 'pressure [GPa]')"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAjsAAAHACAYAAABEa6kcAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjUuMSwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy/YYfK9AAAACXBIWXMAAA9hAAAPYQGoP6dpAABPZklEQVR4nO3deVxU5eLH8c8MqyCguIAoKCquqCHuu7llppmWZVq2W1ZGZou3W9d+3aS8ZWWaZau2ml01r1kuLW5YLrjkvqGyiLixCLLO/P6gJklNRgfOMHzfr9f8wZlh+D5qw7dznvM8JqvVakVERETERZmNDiAiIiJSllR2RERExKWp7IiIiIhLU9kRERERl6ayIyIiIi5NZUdERERcmsqOiIiIuDSVHREREXFpKjsiIiLi0lR2RERExKW5TNl5++23CQ8Px9vbm+joaNasWWN0JBEREXECLlF25s2bR0xMDM8++yxbtmyhe/fuDBw4kKNHjxodTURERAxmcoWNQDt27Ejbtm2ZNWuW7Vjz5s0ZOnQosbGxBiYTERERo7kbHeBq5efns3nzZp555pkSx/v3709cXNxFvycvL4+8vDzb1xaLhdOnT1OjRg1MJlOZ5hURERHHsFqtZGVlERISgtl86YtVFb7snDx5kqKiIoKCgkocDwoKIjU19aLfExsbywsvvFAe8URERKSMJSYmUq9evUs+X+HLzh/+ekbGarVe8izNpEmTmDBhgu3rjIwMwsLCSExMxN/f32GZtiaeYfnO46zcfZyU9FzbcTezifYNqtOneW2ubRpEUIC3w36miIhIZZGZmUloaCh+fn5/+7oKX3Zq1qyJm5vbBWdx0tLSLjjb8wcvLy+8vLwuOO7v7+/QstOjpT89WtbnRauVXccyWbbzOMt3prInNYsNyblsSD5K7MqjtAmtxoCWQVzXMpiGtao67OeLiIhUBpebguIyE5Sjo6N5++23bcdatGjBjTfeWKoJypmZmQQEBJCRkeHQsnMph09ms2xnKst2phJ/NL3EcxG1qzKwVR0GtapDk6CqmkMkIiJyCaX9/e0SZWfevHnccccdvPPOO3Tu3JnZs2fz3nvvsXPnTurXr3/Z7y/vsnO+tMxclu86zrKdqaw/eIpCy59/HY1q+TKoVR0GtqpDs2A/FR8REZHzVKqyA8WLCk6dOpVjx44RGRnJ66+/To8ePUr1vUaWnfNl5BTww57jLP3tGKv3nSS/yGJ7rmFNX65vVYeBrYJpUcdfxUdERCq9Sld2roazlJ3zZeYW8OPuNL797Rir9p0gv/DP4tOgho/tUlfLEBUfERGpnFR27OCMZed8WbkF/LgnjaW/HePnvSfI+0vxGdwmhBtah9A0+O9no4uIiLgSlR07OHvZOV92XqGt+Py0N43cgj+LT5OgqgxuHcINbUIIr+lrYEoREZGyp7Jjh4pUds6XnVfIyt3H+d+2Y6zed6LEHJ/Iuv4Mbh3CoNZ1qFfdx8CUIiIiZUNlxw4VteycL+NcAct3prJk+zHWHjhJ0Xl3dbUNq8YNrUO4oU0davtpAUMREXENKjt2cIWyc77T2fl8t+MYS7Yd45eEU/zxN2w2QdfGNRnSJoTrIoPx8/YwNqiIiMhVUNmxg6uVnfOlZeay9LdjfLMthS3nLWDo6W6mb/Pa3HhNXXo1rYWXu5txIUVERK6Ayo4dXLnsnO/oqRy+2ZrMoq3JHDyRbTvu7+3O9a3qMOSaEDqG18DNrFvZRUTE+ans2KGylJ0/WK1WdqZksnhbCou3ppCa+ecmpcH+3gxuU4dhbevRvI7r/1mIiEjFpbJjh8pWds5XZLGyIeE032xNZulvx8jMLbQ91yzYj+Ft63HjNSHU9tfEZhERcS4qO3aozGXnfHmFRfy89wQL45P5Yc9xCoqK/2mYTdAtohbDourSv2UQPp7uBicVERFR2bGLys6F0nPyWbL9GAvik0rszO7r6cbAVnUYFlWXTg1rYNb8HhERMYjKjh1Udv5ewslsFm5JZuGWJBJPn7MdDwnwZljbegyPrqcVm0VEpNyp7NhBZad0rFYrm46cYUF8Mku2p5B13vye9g2qc3N0PQa1DqGqly5ziYhI2VPZsYPKjv1yC4pYufs4X29OYvW+E/yxYHMVDzcGRgZzc7t6dArXZS4RESk7Kjt2UNm5OqkZuSzcksz8zYkcOm/9nrrVqjA8uh63RNcjNFD7c4mIiGOp7NhBZccxrFYrWxLTmb8piSXbUsjK+/MyV5dGNbi1fSgDWgbj7aHVmkVE5Oqp7NhBZcfxcguKWLYzla83J7H2wEnb/lwBVTwYek0It7YPo0WI/qxFROTKqezYQWWnbCWdyWH+piS+3pxEcvqfd3O1qhvAiPahDGkTQkAVbUoqIiL2Udmxg8pO+SiyWFl34CTzNiayfFeqbdFCL3czg1rV4db2oXQID8Rk0qRmERG5PJUdO6jslL9TZ/NYuCWZrzYlsu/4WdvxRrV8GdkhjJuj61HNx9PAhCIi4uxUduygsmMcq9XK1sR05m1MZPG2FHLyiwDw/P1sz8gOYbRvUF1ne0RE5AIqO3ZQ2XEOWbkFfLM1hc9/PcquY5m2441rV2VkhzCGt62rsz0iImKjsmMHlR3nYrVa2Z6UwRcbjl70bM/oTmG0DdPZHhGRyk5lxw4qO87rUmd7mtfxZ3SnMIZeUxdfbU8hIlIpqezYQWXH+f1xtufTX46weFsKeYUWAKp6uTOsbV1Gd6pPkyA/g1OKiEh5Utmxg8pOxZKek8/Xm5P47NejJJz8c3uKDuGBjO5Un+taBuPpbjYwoYiIlAeVHTuo7FRMFouVuIOn+PSXI6zYfZyi33cjrVnVi9s7hDKqU32C/L0NTikiImVFZccOKjsVX2pGLl9sOMqXG49yPDMPAHeziQGRwdzVpQHt6mtCs4iIq1HZsYPKjusoKLKwfOdx5sQdZsPh07bjLer4M6ZLfW68pq42IhURcREqO3ZQ2XFNu1Iymbv+MIu2JpNbUDyhuZqPB7e2C2V0p/qEBvoYnFBERK6Gyo4dVHZcW3pOPl9tSmTu+iMknSneiNRsgv4tgrm7awPtxyUiUkGp7NhBZadyKLJY+WlPGnPWH2bN/pO24y1D/Lm7aziD29TBy12XuEREKgqVHTuo7FQ++45n8dG6wyzckmS7xFWzqiejOtZndKf61PLzMjihiIhcjsqOHVR2Kq8z2fl8sfEon6w/wrGMXAA83czc0KYO93YLp2VIgMEJRUTkUlR27KCyIwVFFr7fkcpH6xKIP5puO96lUQ3u796Qnk1qYTZrXo+IiDNR2bGDyo6cb2tiOh+sTWDpb8dsCxU2rl2Ve7uFc1OUbl0XEXEWKjt2UNmRi0k6k8PH6w7z5cZEzuYVAlDD15M7OhfP66lZVfN6RESMpLJjB5Ud+TtZuQXM25jIR+sOk5xefOu6p7uZ4W3rcX/3cBrWqmpwQhGRykllxw4qO1IahUUWlu5I5f01h9ielAGAyQQDWgQztmdDosKqG5xQRKRyUdmxg8qO2MNqtbIh4TSzVx/ihz1ptuMdwgN5sGdDejWprcnMIiLlQGXHDio7cqX2Hc9i9upDfLM1mYKi4v+UmgRV5YEejRjSJgRPd7PBCUVEXJfKjh1UduRqpWbk8tG6BD779ahtMnOwvzf3dQ/n9o5h+Hi6G5xQRMT1qOzYQWVHHCUzt4DPfz3Kh2sTSMvKA6C6jwd3dQlnTJf6VPPxNDihiIjrUNmxg8qOOFpeYREL4pN5Z9VBjpzKAcDX043bO4ZxX/eGBPl7G5xQRKTiK+3vb6eeUBAbG0v79u3x8/Ojdu3aDB06lL1795Z4jdVqZfLkyYSEhFClShV69erFzp07DUosUszL3Y2RHcL48YlevDUyiuZ1/MnOL+K9NQl0f+UnJi3YzuGT2UbHFBGpFJy67KxatYqHH36YX375hRUrVlBYWEj//v3Jzv7zl8TUqVOZNm0aM2bMYOPGjQQHB9OvXz+ysrIMTC5SzM1sYnCbEJaO78ZHd7enQ4NA8ossfLEhkWtf+5mYL7ew/7j+rYqIlKUKdRnrxIkT1K5dm1WrVtGjRw+sVishISHExMTw9NNPA5CXl0dQUBCvvPIKY8eOLdX76jKWlKeNh0/z9k8H+GnvCaB4rZ7rWgbzyLWNtfGoiIgdXOIy1l9lZBQv5BYYGAhAQkICqamp9O/f3/YaLy8vevbsSVxc3CXfJy8vj8zMzBIPkfLSvkEgH93dgSWPduO6lsFYrfDdjlQGTV/LvR9vZMvRM0ZHFBFxKRWm7FitViZMmEC3bt2IjIwEIDU1FYCgoKASrw0KCrI9dzGxsbEEBATYHqGhoWUXXOQSIusG8M4d0SyL6cGQNiGYTfDDnjRuejuO0e//yoaE00ZHFBFxCRWm7DzyyCNs376dL7744oLnTKaSq9VardYLjp1v0qRJZGRk2B6JiYkOzytSWk2D/Zg+MoqVE3pyS3Q93M0m1h44yYh31zNy9i/8euiU0RFFRCq0ClF2Hn30URYvXsxPP/1EvXr1bMeDg4MBLjiLk5aWdsHZnvN5eXnh7+9f4iFitIa1qvKfW9rw08RejOwQhoebifWHTnHr7F+4bfZ6flHpERG5Ik5ddqxWK4888ggLFizgxx9/JDw8vMTz4eHhBAcHs2LFCtux/Px8Vq1aRZcuXco7rohDhAb6EDusFT8/2ZtRHYtLzy+HTnPb7F+49d31rD+o0iMiYg+nvhtr3LhxfP7553zzzTc0bdrUdjwgIIAqVaoA8MorrxAbG8tHH31EREQEU6ZM4eeff2bv3r34+fmV6ufobixxZsnp55j18wG+2phEfpEFgI7hgUzo14SODWsYnE5ExDgusYLypebdfPTRR9x1111A8dmfF154gXfffZczZ87QsWNHZs6caZvEXBoqO1IRpKSfY9bPB5m3MdFWero1rsnj/ZoQXb+6welERMqfS5Sd8qKyIxXJsYxzzPjxAF9tSrTttN6raS0m9GtC63rVjA0nIlKOVHbsoLIjFVHi6Rxm/HiAr+OTKLIU/2fct3kQE/o1oUWI/h2LiOtT2bGDyo5UZIdPZjP9h/0s2prM752HQa3rMKFfExrVqmpsOBGRMqSyYweVHXEFB9LO8uYP+1myPQWrFcwmuDm6HuP7RFCvuo/R8UREHE5lxw4qO+JKdh/L5LXle1m5Ow0ATzczt3cM4+Hejanl52VwOhERx1HZsYPKjrii+KNn+M/3e1n/+2KEVTzcuKdbAx7o0YiAKh4GpxMRuXoqO3ZQ2RFXtu7ASaYu28u2xHQAAqp4MK5XI8Z0aYC3h5ux4UREroLKjh1UdsTVWa1Wlu86zqvL9rI/7SwAdQK8ebxvE4a1rYu7m1Mvpi4iclEqO3ZQ2ZHKoshiZUF8Eq+v2EdKRi4AjWtX5ckBTenfIuhvN9AVEXE2Kjt2UNmRyia3oIhP1h9h5s8HSM8pAKBtWDUmXd+c9g0CDU4nIlI6Kjt2UNmRyirjXAGzVx/kg7UJ5BYUb0HRv0UQTw9spjV6RMTpqezYQWVHKrvjmbm8sXI/8zYexWIFN7OJkR1CeaxPE92uLiJOS2XHDio7IsX2H8/ile/32Nbo8fV0Y2zPRtzXPRwfT3eD04mIlKSyYweVHZGSfjl0itilu9mWlAFAbT8vJg5oyvC29XAzaxKziDgHlR07qOyIXMhisfLtb8eYumwPiafPAdCijj//HNScLo1rGpxORERlxy4qOyKXlldYfOfWmz/sJyu3ECjeXX3S9ZrELCLGUtmxg8qOyOWdzs5n+g/7+eSXIxRZrLibTYzuVJ/H+kRQ3dfT6HgiUgmp7NhBZUek9A6kneXl73bbJjH7e7sT07cJd3Suj4dWYhaRcqSyYweVHRH7xR04yYvf7mb3sUwAGtXy5Z83tKB309oGJxORykJlxw4qOyJXpshi5atNiby6bC+nsvMB6NW0Fv8c1ILGtTWfR0TKlsqOHVR2RK5OZm4BM348wEfrEigoKp7Pc0fn+sT0aUKAj4fR8UTERans2EFlR8QxEk5m89K3u2zzeQJ9PXlqQFNuaReq9XlExOFUduygsiPiWGv2n+CF/+3iQNpZAFrVDWDykJZE169ucDIRcSUqO3ZQ2RFxvIIiC3PXH+GNFfvIyiten2dY27o8c10zavt7G5xORFyByo4dVHZEys6JrDz+s2wPX21KAqCqlzuP9Yngrq4NdKu6iFwVlR07qOyIlL2tien8a/FOtiWmAxBRuyov3NiSLo209YSIXBmVHTuo7IiUD4vFytebk3j5+z2c/v1W9SFtQnh2UHOCdGlLROxU2t/fOocsIuXGbDYxon0oPz3Rizs61cdsgsXbUrj21Z95b/UhCoosRkcUERekMzvozI6IUXYkZ/DcNzvYcjQdgCZBVfn30FZ0CA80NpiIVAg6syMiTi+ybgD/fbALU4e3JtDXk33HzzLi3fVMnL+NU2fzjI4nIi5CZUdEDPXHpa0fJvRkZIdQAL7enMS1r63iiw1HsVgq/clnEblKuoyFLmOJOJPNR87wz0U7bBuMtg2rxr+HtqJFiP7bFJGSdBlLRCqk6PrV+d8jXfnnoOb4eroRfzSdwTPWErt0Nzn5hUbHE5EKSGVHRJyOu5uZ+7o35IcnenF9q2CKLFbeXX2IftNW89OeNKPjiUgFo7IjIk4rOMCbt0dF88GYdtStVoXk9HPc/fFGHv4snrTMXKPjiUgFobIjIk6vT/MgVkzowf3dw3Ezm/j2t2P0eW0Vn/xyRBOYReSyVHZEpELw8XTn2UEt+ObhrrSpF0BWXiHPLdrBiHfXcyAty+h4IuLEVHZEpEKJrBvAgnFdmTy4Bb6ebmw6cobr31zLmyv3k1+oFZhF5EIqOyJS4biZTdzVNZzlE3rSu2kt8ossvL5yHze8tYbNR84YHU9EnIzKjohUWHWrVeHDu9ozfWQUNX5fgfnmd+L41zc7yM7TbeoiUkxlR0QqNJPJxJA2Iayc0JObo+thtcKc9Ufo//pqVu87YXQ8EXECKjsi4hKq+3ry6i1t+PTejtSrXnyb+p0fbuDJ+dvIyCkwOp6IGEhlR0RcSreImiyL6cFdXRpgMsH8zUn0fX0Vy3amGh1NRAyisiMiLsfXy53JQ1oyf2xnGtby5URWHmM/2cwjn8drN3WRSkhlR0RcVrsGgSwd351xvRrhZjaxZPsx+r++mqW/HTM6moiUowpVdmJjYzGZTMTExNiOWa1WJk+eTEhICFWqVKFXr17s3LnTuJAi4lS8Pdx46rpmLBrXlaZBfpzKzmfcZ/E8/JnO8ohUFhWm7GzcuJHZs2fTunXrEsenTp3KtGnTmDFjBhs3biQ4OJh+/fqRlaUVVUXkT63qBbD40a48em1j25YT/V5fzbfbdZZHxNVViLJz9uxZRo0axXvvvUf16tVtx61WK2+88QbPPvssw4YNIzIykjlz5pCTk8Pnn39uYGIRcUZe7m480b8p3zzclWbBfpzOzufhz+N5+PN4TmfnGx1PRMpIhSg7Dz/8MIMGDaJv374ljickJJCamkr//v1tx7y8vOjZsydxcXGXfL+8vDwyMzNLPESk8oisG8DiR7ox/o+zPL/P5Vmx67jR0USkDDh92fnyyy+Jj48nNjb2gudSU4tvJQ0KCipxPCgoyPbcxcTGxhIQEGB7hIaGOja0iDg9T3czE/o3ZeG4LkTUrsrJs3ncP3cTE77aSsY5rcsj4kqcuuwkJiby2GOP8emnn+Lt7X3J15lMphJfW63WC46db9KkSWRkZNgeiYmJDsssIhVL63rV+N+j3RjbsyEmEyyIT2aAVl8WcSlOXXY2b95MWloa0dHRuLu74+7uzqpVq5g+fTru7u62Mzp/PYuTlpZ2wdme83l5eeHv71/iISKVl7eHG5MGNufrBzvToIYPqZm53PnhBp7/Zgc5+dpjS6Sic+qy06dPH3777Te2bt1qe7Rr145Ro0axdetWGjZsSHBwMCtWrLB9T35+PqtWraJLly4GJheRiii6fiDfPdaDMZ3rAzB3/REGTV/LlqPaSV2kInM3OsDf8fPzIzIyssQxX19fatSoYTseExPDlClTiIiIICIigilTpuDj48Ptt99uRGQRqeCqeLrxwo2R9G0RxJPzt5NwMpvhs+J4uHdjxveJwMPNqf8fUUQuosL/V/vUU08RExPDuHHjaNeuHcnJySxfvhw/Pz+jo4lIBdY9ohbLYnpw4zUhWKzw1o8HuOntdRxIO2t0NBGxk8lqtVqNDmG0zMxMAgICyMjI0PwdEbnAku0p/HPRDtJzCvD2MPOP65tzR6f6f3sjhIiUvdL+/q7wZ3ZERMraDa1DWBbTg+4RNcktsPD8Nzu5++ONpGXlGh1NREpBZUdEpBSC/L2Zc3cHJg9ugae7mZ/3nuC6N9awbOel1/QSEeegsiMiUkpms4m7uoaz5NFutKjjz+nsfMZ+spln/rtdt6iLODGVHREROzUJ8mPhw11sCxF+uTGRG6av5bekDKOjichFqOyIiFwBL/fihQg/u68jwf7eHDqZzbBZ63hn1UEslkp/34eIU1HZERG5Cl0a1eS7x7pzXctgCoqsvPzdHkZ/8CupGZq8LOIsVHZERK5SdV9PZo1uy8vDWlHFw424g6e47k3toi7iLFR2REQcwGQycVuHML4d343Iuv6k5xRw/9xN/OubHeQWFBkdT6RSU9kREXGghrWq8t+HunB/93AA5qw/wtCZ69h/PMvgZCKVl8qOiIiDebm78eygFnx8d3tqVvVkT2oWg2es5YsNR9Gi9SLlT2VHRKSM9Gpam6WPdbetvDxpwW88+sUWsnILjI4mUqmo7IiIlKHafsUrL08a2Ax3s4kl248xaPpatielGx1NpNJQ2RERKWNms4mxPRvx1YOdqVutCkdP5zB8Vhzvrzmky1oi5UBlR0SknLQNq87S8X+uyfPvb3dz35xNnMnONzqaiEtT2RERKUcBPh7MGt2WF4dG4ulu5oc9aQyavobNR84YHU3EZansiIiUM5PJxB2d6rNwXBfCa/qSkpHLre+uZ/ZqbTUhUhZUdkREDNIyJIDFj3TlhtZ1KLRYmbJ0D/fP1WUtEUdT2RERMZCftwdvjYzipZtKXtaKP6rLWiKOorIjImIwk8nEqI4XXtb6cG2C7tYScQCTtRT/JS1evNjuN+7Xrx9VqlS5olDlLTMzk4CAADIyMvD39zc6johUYlm5BTz93+0s/S0VgOtbBfPK8Nb4eXsYnEzE+ZT293epyo7ZbN8JIJPJxP79+2nYsKFd32cUlR0RcSZWq5U5cYd5aeluCoqsNKjhw9ujomkRos8nkfOV9vd3qVtMamoqFoulVA8fHx+HDEJEpDIymUzc1TWcr8YWL0J4+FQON729jvmbEo2OJlIhlarsjBkzxq5LUqNHj9YZEhGRqxQVVp0lj3ajd9Na5BVaePLr7Tzz3+3kFhQZHU2kQinVZSxXp8tYIuLMLBYrM386wLSV+7BaoWWIP7NGRRNWQ2fRpXJz+GUsERExhtls4tE+Ecy9pwPVfTzYmZLJDW+t4cc9x42OJlIhXNGZnY0bNzJ//nyOHj1Kfn7Jxa8WLFjgsHDlRWd2RKSiSEk/x7jP4tmamA7A+D4RPNYnAjezydhgIgYoszM7X375JV27dmXXrl0sXLiQgoICdu3axY8//khAQMBVhRYRkb8XUq0KX43tzJ2d6wMw/Yf93PPxRtJztOqyyKXYXXamTJnC66+/zpIlS/D09OTNN99k9+7djBgxgrCwsLLIKCIi5/F0N/N/N0YybUQbvD3MrNp3ghveWsuO5Ayjo4k4JbvLzsGDBxk0aBAAXl5eZGdnYzKZePzxx5k9e7bDA4qIyMUNa1uPBQ91JSzQh6Qz5xg+K46vNycZHUvE6dhddgIDA8nKygKgbt267NixA4D09HRycnIcm05ERP5WixB//vdIN65tVpu8QgsT52/j+W92kF9oMTqaiNOwu+x0796dFStWADBixAgee+wx7r//fkaOHEmfPn0cHlBERP5egI8H79/Zjsf6RAAwd/0Rbn/vF9Iycw1OJuIc7L4b6/Tp0+Tm5hISEoLFYuHVV19l7dq1NG7cmOeee47q1auXVdYyo7uxRMRV/LD7ODFfbiUrr5Dafl7MGt2W6PqBRscSKRMO3RvrD0eOHGH58uUUFBTQs2dPWrZs6ZCwRlPZERFXknAymwfmbmJ/2lk83ExMHtKSUR3rGx1LxOEcXnZWr17N9ddfb5uX4+7uzpw5cxg5cqRjEhtIZUdEXE12XiFPfr3Ntnv6yA5hTB7SAi93N4OTiTiOw9fZee655+jduzdJSUmcOnWKe+65h6eeesohYUVExLF8vdyZeXtbnhzQFJMJvthwlJGzNY9HKqdSn9kJDAxk9erVREZGApCdnY2/vz8nT56skPN0zqczOyLiyn7am8b4L7aQlVs8j+edO6JpG1axP7dFoAzO7KSnp1O7dm3b176+vvj4+JCenn5VQUVEpGz1blqbxY90I6J2VdKy8rjt3V/4alOi0bFEyo27PS/etWsXqamptq+tViu7d++2rbsD0Lp1a8elExERhwiv6cvCh7syYd5Wlu86zlNfb2dXSibPDmqOh5v2hBbXVurLWGazGZPJxMVe/sdxk8lEUVGRw0OWNV3GEpHKwmKxMv3H/byxcj8AnRvWYOaotgT6ehqcTMR+Dr8b68iRI6X6wfXrV7zbG1V2RKSy+X5HKhO+2kpOfhH1qlfhvTvb0byOPv+kYimTdXZclcqOiFRGe1OzuH/uJo6ezsHH043Xb72GAS2DjY4lUmplUnYyMzNtb7Z06VIKCwttz7m5udk2CK1oVHZEpLJKz8nn4c/jWXfgFABP9GvCI9c2xmQyGZxM5PIcXnaWLFnCc889x5YtWwDw8/MjOzv7zzcymZg3bx4333zzVUYvfyo7IlKZFRRZeOnb3XwcdxiAQa3q8J9bWuPjadc9LCLlzuG3ns+ePZtHHnmkxLEDBw5gsViwWCzExsby4YcfXnniS0hOTmb06NHUqFEDHx8frrnmGjZv3mx73mq1MnnyZEJCQqhSpQq9evVi586dDs8hIuKqPNzMTB7SkpeHtcLDzcS3vx3jlnfWk5J+zuhoIg5R6rKzfft22rRpc8nnBw4cyKZNmxwS6g9nzpyha9eueHh48N1337Fr1y5ee+01qlWrZnvN1KlTmTZtGjNmzGDjxo0EBwfTr1+/ErfDi4jI5d3WIYzP7+9EDV9PdqZkMmTGOuKPnjE6lshVK/VlLG9vb3bv3k14eDgAmzZtok2bNnh4eACQkJBAs2bNyMvLc1i4Z555hnXr1rFmzZqLPm+1WgkJCSEmJoann34agLy8PIKCgnjllVcYO3ZsqX6OLmOJiPwp6UwO983ZxJ7ULDzdzbw8rBXD2tYzOpbIBRx+GSswMJCDBw/avm7Xrp2t6ADs37+fwMDAK4x7cYsXL6Zdu3bccsst1K5dm6ioKN577z3b8wkJCaSmptK/f3/bMS8vL3r27ElcXJxDs4iIVBb1qvvw34e60L9FEPmFFiZ8tY2Xv9tDkaXS37wrFVSpy06PHj2YPn36JZ+fPn06PXr0cEioPxw6dIhZs2YRERHBsmXLePDBBxk/fjxz584FsK3mHBQUVOL7goKCSqz0/Fd5eXlkZmaWeIiIyJ98vdx5Z3Q0j/RuDMA7qw4y9pPNZOcVXuY7RZxPqcvO008/zfLly7nlllvYuHEjGRkZZGRksGHDBoYPH87KlSttl5IcxWKx0LZtW6ZMmUJUVBRjx47l/vvvZ9asWSVe99dbJP9YzflSYmNjCQgIsD1CQ0MdmltExBWYzSYmDmjKm7ddg6e7mZW7jzN8VhzJmrgsFUypy05UVBTz5s3j559/plOnTgQGBhIYGEjnzp1ZtWoVX375JW3btnVouDp16tCiRYsSx5o3b87Ro0cBCA4uXvzqr2dx0tLSLjjbc75JkybZylpGRgaJidoQT0TkUm68pi7zHuhEzape7EnN4sYZa9l8RBOXpeKwaxGFG2+8kX79+rFs2TL27y/eVyUiIoL+/fvj6+vr8HBdu3Zl7969JY7t27fPtiVFeHg4wcHBrFixgqioKADy8/NZtWoVr7zyyiXf18vLCy8vL4fnFRFxVVFh1fnmka7cN2cTu49lMvK9X5g6vDVDo+oaHU3ksuxeMcrHx4ebbrqpLLJc4PHHH6dLly5MmTKFESNGsGHDBmbPns3s2bOB4stXMTExTJkyhYiICCIiIpgyZQo+Pj7cfvvt5ZJRRKSyqFutCl8/2JnHf985PWbeVg6dOEtM3yaYzVpxWZxXqS5jTZ8+ndzc3FK/6TvvvOOQdW7at2/PwoUL+eKLL4iMjOTFF1/kjTfeYNSoUbbXPPXUU8TExDBu3DjatWtHcnIyy5cvx8/P76p/voiIlPTHxOUHezYCYPqPB3j0yy3kFhQZnEzk0kq1zo6bmxupqanUqlWrVG/q7+/P1q1badiw4VUHLA9aZ0dExH7zNyXyj4W/UVBkpU1oNd67I5ra/t5Gx5JKpLS/v0t1GctqtdKnTx/c3Ut31evcOc3UFxFxdbe0CyUs0Iexn25mW2I6Q2eu4/0x7WkRov9pFOdSqjM7L7zwgt1v/Nhjj5XY1sGZ6cyOiMiVO3wym3vmbOTQiWx8Pd146/Yorm126TtiRRzF4bueuzKVHRGRq5ORU8BDn20m7uApzCZ4/oYW3NU13OhY4uIcvl2EiIjIpQT4eDDnng7c1j4UixUm/28Xz3+zg8Iii9HRRFR2RETEMTzczMQOa8U/rm+GyQRz1x/h3jmbyMotMDqaVHIqOyIi4jAmk4kHejRi1qhovD3MrNp3glveWU+KtpgQA6nsiIiIw10XGcxXYztTy694i4mhM9fxW1KG0bGkkrrispOfn8/evXspLNQOuCIicqHW9aqx6OGuNA3yIy0rjxHvrmf5ztTLf6OIg9lddnJycrj33nvx8fGhZcuWtk05x48fz8svv+zwgCIiUnHVrVaFrx/qTPeImpwrKGLsp5t5f80hdCOwlCe7y86kSZPYtm0bP//8M97ef66U2bdvX+bNm+fQcCIiUvH5eXvw0V3tub1jGFYr/Pvb3fxr8U7dqSXlxu6ys2jRImbMmEG3bt0wmf7c+K1FixYcPHjQoeFERMQ1uLuZeWloJP+4vhlQfKfWA59sJjtPUyGk7Nlddk6cOEHt2rUvOJ6dnV2i/IiIiJzvzzu12uLlbubHPWmMeHc9qRml32ha5ErYXXbat2/Pt99+a/v6j4Lz3nvv0blzZ8clExERlzSwVR2+eKATNXw92ZmSyU1vr2P3sUyjY4kLK93OnueJjY3luuuuY9euXRQWFvLmm2+yc+dO1q9fz6pVq8oio4iIuJi2YdVZ9HBXxny0gUMnsrnlnfW8PaotPZrUMjqauCC7z+x06dKFuLg4cnJyaNSoEcuXLycoKIj169cTHR1dFhlFRMQFhQb6sOChLnQMD+RsXiF3f7yReRuPGh1LXJBdG4EWFBTwwAMP8Nxzz9GwYcOyzFWutBGoiIhx8gqLeOa/v7FwSzIAD/duxMT+TTUPVC6rTDYC9fDwYOHChVcdTkRE5A9e7m5MG9GG8dc2BmDmTweJmbeVvMIig5OJq7D7MtZNN93EokWLyiCKiIhUViaTiQn9mzL15ta4m018szWFOz/YQEaONhGVq2f3BOXGjRvz4osvEhcXR3R0NL6+viWeHz9+vMPCiYhI5TKiXSghAVV46NPN/JpwmuHvxPHRXe0JDfQxOppUYHbN2QEIDw+/9JuZTBw6dOiqQ5U3zdkREXEue1IzufujjRzLyKVmVS8+vKsdretVMzqWOJnS/v62u+y4IpUdERHnczwzl7s/2siuY5lU8XBjxu1R9GkeZHQscSJlMkFZRESkvAT5e/PVg53p0aQW5wqKuH/uJj795YjRsaQCsvvMzj333PO3z3/44YdXFcgIOrMjIuK8Coos/HPhDuZtSgRgbM+GPD2gGWazbk2v7Er7+9vuCcpnzpwp8XVBQQE7duwgPT2da6+91v6kIiIif8PDzczLw1tRr3oVXluxj3dXHSIlPZdXb2mNl7ub0fGkArC77FxsnR2LxcK4ceNcaqFBERFxHiaTiUf7RBBSrQpP/3c7/9uWwvHMXN67ox0BPh5GxxMn57AJynv37qVXr14cO3bMEW9XrnQZS0Sk4lh34CQPfrKZrLxCGteuqlvTK7Fyn6B88OBBCgsLHfV2IiIiF9W1cU3mP9SZYH9vDqSdZdisOHYkZxgdS5yY3ZexJkyYUOJrq9XKsWPH+PbbbxkzZozDgomIiFxKs2B/Fj7chbs/2sie1CxGvLuemaPa0rtpbaOjiROy+zJW7969S3xtNpupVasW1157Lffccw/u7nb3J8PpMpaISMWUlVvAQ5/Gs/bASdzMJl4aGsltHcKMjiXlRIsK2kFlR0Sk4sovtPDMgu0siC/eNX18nwge7xuhXdMrgTKbs3Pu3DlycnJsXx85coQ33niD5cuXX1lSERGRq+Dpbua1W/7cNX36D/t5b03F27pIyo7dZefGG29k7ty5AKSnp9OhQwdee+01brzxRmbNmuXwgCIiIpdjsUJeocX29dm8IgPTiLOxu+zEx8fTvXt3AL7++muCg4M5cuQIc+fOZfr06Q4PKCIi8nfSc/K566MNvLu6+GzOuF6NeKxPhMGpxJnYPZs4JycHPz8/AJYvX86wYcMwm8106tSJI0e0Z4mIiJSfvalZ3D93E0dP51DFw43/3NKaG1qHGB1LnIzdZ3YaN27MokWLSExMZNmyZfTv3x+AtLQ0Te4VEZFy9dCnmzl6Oodgf2/++1AXFR25KLvLzvPPP8/EiRNp0KABHTt2pHPnzkDxWZ6oqCiHBxQREbmUqLDqAJzJyedUdp7BacRZXdGt56mpqRw7dow2bdpgNhf3pQ0bNuDv70+zZs0cHrKs6dZzEZGKKb/QwrjP4lm5+zhe7mY+GNOebhE1jY4l5aRMt4sIDg4mKioKs9lMZmYmixYtws/Pr0IWHRERqbg83c28PaotfZvXJq/Qwr1zNrLuwEmjY4mTsbvsjBgxghkzZgDFa+60a9eOESNG0Lp1a/773/86PKCIiMjf8XQ3M3NUW65t9mfh+fXQKaNjiROxu+ysXr3aduv5woULsVqtpKenM336dP797387PKCIiMjleLm7MWt0W3o2qUVugYV7Pt7I5iNnjI4lTsLuspORkUFgYCAA33//PcOHD8fHx4dBgwaxf/9+hwcUEREpDS93N969I5pujWuSnV/EXR9uYFtiutGxxAnYXXZCQ0NZv3492dnZfP/997Zbz8+cOYO3t7fDA4qIiJSWt4cb793Zjo7hgWTlFXLHB7+yMyXD6FhiMLvLTkxMDKNGjaJevXrUqVOHXr16AcWXt1q1auXofCIiInap4unGh3e1p1396mTmFnLHBxvYfzzL6FhioCu69XzTpk0kJibSr18/qlatCsC3335LtWrV6Nq1q8NDljXdei4i4noycwsY/f6vbE/KoLafF1+N7UyDmr5GxxIHKtNbz9u1a8egQYNITk6msLAQgEGDBjm86BQWFvLPf/6T8PBwqlSpQsOGDfm///s/LJY/N3uzWq1MnjyZkJAQqlSpQq9evdi5c6dDc4iISMXj7+3BnLs70DTIj7SsPEa9/yvJ6eeMjiUGsLvs5OTkcO+99+Lj40PLli05evQoAOPHj+fll192aLhXXnmFd955hxkzZrB7926mTp3Kf/7zH9566y3ba6ZOncq0adOYMWMGGzduJDg4mH79+pGVpVOWIiKVXXVfTz69ryMNa/qSnH6OUe/9QlpWrtGxpJzZXXYmTZrEtm3b+Pnnn0tMSO7bty/z5s1zaLj169dz4403MmjQIBo0aMDNN99M//792bRpE1B8VueNN97g2WefZdiwYURGRjJnzhxycnL4/PPPHZpFREQqplp+Xnx2f0fqVa/C4VM53PnBBtJz8o2OJeXI7rKzaNEiZsyYQbdu3TCZTLbjLVq04ODBgw4N161bN3744Qf27dsHwLZt21i7di3XX389AAkJCaSmptruCAPw8vKiZ8+exMXFXfJ98/LyyMzMLPEQERHXVSegCp/d15Hafl7sSc3iro82cjav0OhYUk7sLjsnTpygdu3aFxzPzs4uUX4c4emnn2bkyJE0a9YMDw8PoqKiiImJYeTIkUDxHl0AQUFBJb4vKCjI9tzFxMbGEhAQYHuEhoY6NLeIiDif+jV8+fS+jlTz8WBrYjoPzN1EbkGR0bGkHNhddtq3b8+3335r+/qPgvPee+/ZdkB3lHnz5vHpp5/y+eefEx8fz5w5c3j11VeZM2dOidf9tWRZrda/LV6TJk0iIyPD9khMTHRobhERcU5NgvyYc3cHqnq5E3fwFI9+sYXCIsvlv1EqNHd7vyE2NpbrrruOXbt2UVhYyJtvvsnOnTtZv349q1atcmi4J598kmeeeYbbbrsNgFatWnHkyBFiY2MZM2YMwcHBQPEZnjp16ti+Ly0t7YKzPefz8vLCy8vLoVlFRKRiaBNajffHtOPODzewYtdxnlnwG1OHt8ZsduzVCXEedp/Z6dKlC3FxceTk5NCoUSOWL19OUFAQ69evJzo62qHhcnJyMJtLRnRzc7Pdeh4eHk5wcDArVqywPZ+fn8+qVavo0qWLQ7OIiIjr6NSwBjNvb4ub2cTXm5OYsnQ3V7DsnFQQdp3ZKSgo4IEHHuC555674FJSWRg8eDAvvfQSYWFhtGzZki1btjBt2jTuueceoPjyVUxMDFOmTCEiIoKIiAimTJmCj48Pt99+e5nnExGRiqtfiyCmDm/NE/O38f7aBKr7evJw78ZGx5IyYPcKytWqVSM+Pp6GDRuWVSabrKwsnnvuORYuXEhaWhohISGMHDmS559/Hk9PT6B4fs4LL7zAu+++y5kzZ+jYsSMzZ84kMjKy1D9HKyiLiFReH6xN4MUluwB4eVgrbusQZnAiKa3S/v62u+zcfffdtGrVigkTJlx1SGehsiMiUrn9Z9keZv50ELMJZo2OZkDLYKMjSSmU9ve33ROUGzduzIsvvkhcXBzR0dH4+pbcZ2T8+PH2pxURETHQxP5NOZmVz7xNiTz6xRY+uacDHRvWMDqWOIjdZ3bCw8Mv/WYmE4cOHbrqUOVNZ3ZERKSwyMJDn8WzYtdx/Lzdmf9gZ5oF63eCMyuzy1iuSGVHREQAcguKuPODDWw4fJogfy8WjOtK3WpVjI4ll1Cmu57/wWq16lY9ERFxGd4ebrx3ZzuaBFXleGYeYz7UPlqu4IrKzgcffEBkZCTe3t54e3sTGRnJ+++/7+hsIiIi5S7Ax4OP7+5AsL83B9LOcr+2lajw7C47zz33HI899hiDBw9m/vz5zJ8/n8GDB/P444/zz3/+sywyioiIlKuQalWYc08H/Lzd2Xj4DDFfbqXIoisZFZXdc3Zq1qzJW2+9ZduM8w9ffPEFjz76KCdPnnRowPKgOTsiInIxvxw6xZ0fbCC/yMI9XcN5fnALoyPJecpszk5RURHt2rW74Hh0dDSFhYX2vp2IiIjT6tSwBq+OaAPAh+sS+GBtgsGJ5ErYXXZGjx7NrFmzLjg+e/ZsRo0a5ZBQIiIizmJImxCeGdgMgH9/u4ulvx0zOJHYy+5FBaF4gvLy5cvp1KkTAL/88guJiYnceeedJVZWnjZtmmNSioiIGGhsj4YknznHJ78cIWbeVoL8vYmuX93oWFJKds/Z6d27d+ne2GTixx9/vKJQ5U1zdkRE5HKKLFbGfrKJlbvTqOHrycJxXQmr4WN0rEpNiwraQWVHRERKIzuvkFtnr2dHciYNa/my8KGuBPh4GB2r0iqXRQVFREQqE18vdz4Y056QAG8Onchm7KebyC+0GB1LLkNlR0RExA5B/t58eHd7qnq588uh0/xj4W/aTcDJqeyIiIjYqVmwPzNHtcXNbOLrzUnMWnXQ6EjyN1R2RERErkDPJrWY/Psig1O/38t3uiXdaansiIiIXKE7Ojfgri4NAHj8q61sT0o3NI9cnMqOiIjIVXjuhhb0blqL3AIL98/dRGpGrtGR5C9UdkRERK6Cm9nE9JFRNAmqyvHMPB74ZBPn8rVLujNR2REREblKft4evH9ne6r7eLA9KYMnv96mO7SciMqOiIiIA4TV8OGd0dF4uJlYsv0Yb/14wOhI8juVHREREQfp2LAG/x4aCcC0Ffv4fkeqwYkEVHZEREQc6tb2YbY7tCZ8tZU9qZnGBhKVHREREUf756DmdG1cg5z8Iu6fu4nT2flGR6rUVHZEREQczN3NzIyRbQkL9CHx9Dke+TyewiLtoWUUlR0REZEyUN3Xk/fHtMPX0424g6eI/W6P0ZEqLZUdERGRMtIkyI/XRrQB4IO1CSzckmRwospJZUdERKQMXRdZh0evbQzAM//9jR3JGQYnqnxUdkRERMrY432b0KdZbfIKLYz9ZLMmLJczlR0REZEyZjabmHbrNTSo4UNy+jnGf7GFIotWWC4vKjsiIiLlIKCKB+/e0Y4qHm6sPXCSV5fvNTpSpaGyIyIiUk6aBvsx9ebWAMz6+aBWWC4nKjsiIiLlaHCbEO7rFg7AxPnbOHTirMGJXJ/KjoiISDl7emAzOjQI5GxeIQ99Gk9OfqHRkVyayo6IiEg583AzM+P2KGpW9WLv8SyeXbgDq1UTlsuKyo6IiIgBavt7M/P2KNzMJhZuSeazX48aHcllqeyIiIgYpGPDGjw1oCkA//e/XVpwsIyo7IiIiBjogR4N6ds8iPwiC+M+iyfjXIHRkVyOyo6IiIiBTCYTr93ShnrVq3D0dA5Pfb1N83ccTGVHRETEYAE+Hrw9qi2ebmaW7TzOx3GHjY7kUlR2REREnEDretV4dlBzAKYs3c32pHRjA7kQlR0REREncWfn+lzXMpiCIiuPfL6FzFzN33EElR0REREnYTKZeOXm1rb5O5MW/Kb5Ow6gsiMiIuJEAqp48NbIKNzNJr7dfox5GxONjlThGVp2Vq9ezeDBgwkJCcFkMrFo0aISz1utViZPnkxISAhVqlShV69e7Ny5s8Rr8vLyePTRR6lZsya+vr4MGTKEpKSkchyFiIiIY0WFVefJ39ffmfy/new/nmVwoorN0LKTnZ1NmzZtmDFjxkWfnzp1KtOmTWPGjBls3LiR4OBg+vXrR1bWn3/pMTExLFy4kC+//JK1a9dy9uxZbrjhBoqKisprGCIiIg53f/eGdI+oSW6BhUe/2EJugX6vXSmT1UkuBppMJhYuXMjQoUOB4rM6ISEhxMTE8PTTTwPFZ3GCgoJ45ZVXGDt2LBkZGdSqVYtPPvmEW2+9FYCUlBRCQ0NZunQpAwYMKNXPzszMJCAggIyMDPz9/ctkfCIiIvZKy8rl+jfXcPJsPnd2rs//3RhpdCSnUtrf3047ZychIYHU1FT69+9vO+bl5UXPnj2Ji4sDYPPmzRQUFJR4TUhICJGRkbbXXExeXh6ZmZklHiIiIs6mtp83r424BoC564/ww+7jxgaqoJy27KSmpgIQFBRU4nhQUJDtudTUVDw9PalevfolX3MxsbGxBAQE2B6hoaEOTi8iIuIYPZvU4p6u4QA89fV20rJyDU5U8Tht2fmDyWQq8bXVar3g2F9d7jWTJk0iIyPD9khM1Ex3ERFxXk9d15RmwX6cys7nyfnbsVicYgZKheG0ZSc4OBjggjM0aWlptrM9wcHB5Ofnc+bMmUu+5mK8vLzw9/cv8RAREXFW3h5uvDUyCi93M6v2nWDu+sNGR6pQnLbshIeHExwczIoVK2zH8vPzWbVqFV26dAEgOjoaDw+PEq85duwYO3bssL1GRETEFUQE+dm2k4j9bo9uR7eDu5E//OzZsxw4cMD2dUJCAlu3biUwMJCwsDBiYmKYMmUKERERREREMGXKFHx8fLj99tsBCAgI4N577+WJJ56gRo0aBAYGMnHiRFq1akXfvn2NGpaIiEiZuKNTfX7YncaqfSeImbeVheO64unutOctnIahZWfTpk307t3b9vWECRMAGDNmDB9//DFPPfUU586dY9y4cZw5c4aOHTuyfPly/Pz8bN/z+uuv4+7uzogRIzh37hx9+vTh448/xs3NrdzHIyIiUpZMJhP/ubk1A95Yzc6UTN5YuY+nrmtmdCyn5zTr7BhJ6+yIiEhF8v2OYzz4aTxmE8x/sDPR9QONjmSICr/OjoiIiFzcdZF1GBZVF4sVnvhqGzn5hUZHcmoqOyIiIhXQv4a0pE6AN4dP5fDyd3uMjuPUVHZEREQqoIAqHky9uTVQvLry2v0nDU7kvFR2REREKqjuEbW4o1N9AJ7+73aycgsMTuScVHZEREQqsGcGNiM0sArJ6eeYslSXsy5GZUdERKQC8/VyZ+rwNgB8seEoa/afMDiR81HZERERqeA6N6rBmM6/X876ejtn83R31vlUdkRERFzA0wObERboQ0pGLq/o7qwSVHZERERcgI+nOy8PawXAJ78c4ddDpwxO5DxUdkRERFxEl8Y1GdkhFIBnFvxGbkGRwYmcg8qOiIiIC5l0fXOC/L1IOJnNGyv3Gx3HKajsiIiIuBB/bw/+PbT4ctZ7aw6xKyXT4ETGU9kRERFxMf1aBDEwMpgii5VJC7ZTZKnce36r7IiIiLigF4a0xM/bnW1JGXwcd9joOIZS2REREXFBtf29eWZgMwBeW76XlPRzBicyjsqOiIiIixrZPozo+tXJyS/ihf/tNDqOYVR2REREXJTZbOKlmyJxN5tYtvM4K3cdNzqSIVR2REREXFizYH/u7R4OwL8W7+RcfuVbe0dlR0RExMU91ieCutWKd0af8VPlW3tHZUdERMTF+Xi68/zgFgDMXn2IQyfOGpyofKnsiIiIVAL9WwTRq2ktCoqs/GvxTqzWyrP2jsqOiIhIJWAymZg8uCWe7mbW7D/JdztSjY5UblR2REREKokGNX15sEdDAF76dnelmayssiMiIlKJPNSrMSEB3iSnn+Pd1QeNjlMuVHZEREQqkSqebvxjUHMAZv18kKQzOQYnKnsqOyIiIpXMoFZ16BgeSF6hhdjv9hgdp8yp7IiIiFQyJpOJfw1uickE324/xqbDp42OVKZUdkRERCqhFiH+3NouFIAXl+zCYnHdW9FVdkRERCqpJ/o3paqXO9uSMvhmW7LRccqMyo6IiEglVcvPi3G9GwHwn+/3klvgmreiq+yIiIhUYvd0DSckwJuUjFw+WnfY6DhlQmVHRESkEvP2cGPigKYAvP3TAU5n5xucyPFUdkRERCq5odfUpWWIP1l5hUz/wfV2RVfZERERqeTMZhOTBhYvNPjZr0dIPO1aCw2q7IiIiAjdImrSrXFNCoqsvL5yn9FxHEplR0RERAB46rriuTsLtySzJzXT4DSOo7IjIiIiALSuV41BrepgtcKry/YaHcdhVHZERETEZkL/JphNsHJ3GlsT042O4xAqOyIiImLTqFZVboqqB8C0Fa4xd0dlR0REREp4rE8E7mYTq/edYKMLbBKqsiMiIiIlhNXw4ZbfNwmdtrzin91R2REREZELPNy7ER5uJtYfOlXhz+6o7IiIiMgF6lX34ebo4rk7FX1VZZUdERERuahxvRrjbjaxZv9JNh85Y3ScK2Zo2Vm9ejWDBw8mJCQEk8nEokWLbM8VFBTw9NNP06pVK3x9fQkJCeHOO+8kJSWlxHvk5eXx6KOPUrNmTXx9fRkyZAhJSUnlPBIRERHXExrow7C2dYHiTUIrKkPLTnZ2Nm3atGHGjBkXPJeTk0N8fDzPPfcc8fHxLFiwgH379jFkyJASr4uJiWHhwoV8+eWXrF27lrNnz3LDDTdQVFRUXsMQERFxWQ/1aozZBD/sSWNXSsVcVdlktVqtRocAMJlMLFy4kKFDh17yNRs3bqRDhw4cOXKEsLAwMjIyqFWrFp988gm33norACkpKYSGhrJ06VIGDBhQqp+dmZlJQEAAGRkZ+Pv7O2I4IiIiLuORz+NZsv0Yg9uE8NbIKKPj2JT293eFmrOTkZGByWSiWrVqAGzevJmCggL69+9ve01ISAiRkZHExcVd8n3y8vLIzMws8RAREZGLe6hXIwC+3Z7C4ZPZBqexX4UpO7m5uTzzzDPcfvvttvaWmpqKp6cn1atXL/HaoKAgUlNTL/lesbGxBAQE2B6hoaFlml1ERKQiaxkSQK+mtbBY4YO1CUbHsVuFKDsFBQXcdtttWCwW3n777cu+3mq1YjKZLvn8pEmTyMjIsD0SExMdGVdERMTlPNCjIQDzNydyOjvf4DT2cfqyU1BQwIgRI0hISGDFihUlrskFBweTn5/PmTMlb4dLS0sjKCjoku/p5eWFv79/iYeIiIhcWueGNYis609ugYVPfzlidBy7OHXZ+aPo7N+/n5UrV1KjRo0Sz0dHR+Ph4cGKFStsx44dO8aOHTvo0qVLeccVERFxWSaTiQd6FM/dmRN3mNyCinPXs7uRP/zs2bMcOPDnffsJCQls3bqVwMBAQkJCuPnmm4mPj2fJkiUUFRXZ5uEEBgbi6elJQEAA9957L0888QQ1atQgMDCQiRMn0qpVK/r27WvUsERERFzS9ZHBvBzgTUpGLv/blmLbP8vZGXpmZ9OmTURFRREVVXwb24QJE4iKiuL5558nKSmJxYsXk5SUxDXXXEOdOnVsj/PvtHr99dcZOnQoI0aMoGvXrvj4+PC///0PNzc3o4YlIiLiktzdzNzZpQEAH647jJOsXnNZTrPOjpG0zo6IiEjppOfk0yn2B3ILLHz5QCc6Naxx+W8qIy65zo6IiIgYq5qPJ8PaFm8Q+vG6w8aGKSWVHREREbHLmM4NAFix+zipGbnGhikFlR0RERGxS9NgPzo0CKTIYuWLDUeNjnNZKjsiIiJit9Gd6wPwxYajFBRZDE7z91R2RERExG7XtQymZlVP0rLy+GlPmtFx/pbKjoiIiNjN093M8N8nKs/b6NzbLqnsiIiIyBUZ0b54UcGf9qZxLOOcwWkuTWVHRERErkijWlXpEB6IxQpfb0oyOs4lqeyIiIjIFbv19y0jFmxJdtoVlVV2RERE5IpdFxmMj6cbCSeziT96xug4F6WyIyIiIlfM18udgZF1APh6c7LBaS5OZUdERESuyvC2dQFYsj2FvMIig9NcSGVHRERErkqnhjUI9vcmK7eQn/eeMDrOBVR2RERE5KqYzSZuaF18KWvxthSD01xIZUdERESu2o3XFF/K+mH3cbLzCg1OU5LKjoiIiFy1yLr+hNf0JbfAwg9Otn2Eyo6IiIhcNZPJxMDIYAC+33HM4DQlqeyIiIiIQ/xxC/pPe05wLt957spS2RERERGHiKzrT73qVThXUMSqfc5zV5bKjoiIiDiEyWRiQMviS1krdh03OM2fVHZERETEYfo0rw0U74ReZHGOvbJUdkRERMRh2jcIxM/bndPZ+WxNdI69slR2RERExGE83Mz0alp8dueH3c5xC7rKjoiIiDjUtc1qATjNJGWVHREREXGobo2Ly87OlExOZOUZnEZlR0RERByslp8XLer4A7D2gPFnd1R2RERExOF6NCk+u7Nm/0mDk6jsiIiISBno2rgGAL8eOo3Vauwt6Co7IiIi4nDR9avj4WYiOf0ciafPGZpFZUdEREQczsfTnTb1qgHwy6FThmZR2REREZEy0bFhIAC/Jpw2NIfKjoiIiJSJdg2Ky86Wo8aupKyyIyIiImWibWh1AA6dzOZ0dr5hOdwN+8lO5I9Z4pmZmQYnERERcR0mINzfxMET2azZeZTezWo79P3/+L19ubu9TFaj7wdzAklJSYSGhhodQ0RERK5AYmIi9erVu+TzKjuAxWIhJSUFPz8/TCaT0XEukJmZSWhoKImJifj7+xsdp1xp7Bp7ZRp7ZR03aOwa+5WN3Wq1kpWVRUhICGbzpWfm6DIWYDab/7YROgt/f/9K9x/CHzR2jb0yqazjBo1dY7dfQEDAZV+jCcoiIiLi0lR2RERExKWp7FQAXl5e/Otf/8LLy8voKOVOY9fYK5PKOm7Q2DX2sh27JiiLiIiIS9OZHREREXFpKjsiIiLi0lR2RERExKWp7IiIiIhLU9lxMYmJifTq1YsWLVrQunVr5s+fb3SkMrNkyRKaNm1KREQE77//vtFxyk1l+ju+lJycHOrXr8/EiRONjlKuEhIS6N27Ny1atKBVq1ZkZ2cbHancvP7667Rs2ZIWLVowfvz4y+6FVJHddNNNVK9enZtvvrnE8crwmXexsTvkM88qLiUlJcW6ZcsWq9VqtR4/ftxat25d69mzZ40NVQYKCgqsERER1qSkJGtmZqa1cePG1lOnThkdq1xUlr/jv/OPf/zDesstt1ifeOIJo6OUqx49elhXr15ttVqt1lOnTlkLCgoMTlQ+0tLSrA0bNrSeO3fOWlhYaO3SpYs1Li7O6Fhl5scff7QuXrzYOnz4cNuxyvKZd7GxO+IzT2d2XEydOnW45pprAKhduzaBgYGcPn3a2FBlYMOGDbRs2ZK6devi5+fH9ddfz7Jly4yOVS4qy9/xpezfv589e/Zw/fXXGx2lXO3cuRMPDw+6d+8OQGBgIO7ulWfHn8LCQnJzcykoKKCgoIDatR27e7Yz6d27N35+fiWOVZbPvIuN3RGfeSo75Wz16tUMHjyYkJAQTCYTixYtuuA1b7/9NuHh4Xh7exMdHc2aNWuu6Gdt2rQJi8XilDu6X+2fQ0pKCnXr1rV9Xa9ePZKTk8sj+lVz5L8BZ/47vhhHjH3ixInExsaWU2LHudqx79+/n6pVqzJkyBDatm3LlClTyjH91bnasdeqVYuJEycSFhZGSEgIffv2pVGjRuU4gtIrq8/4ivCZVx6/3670M09lp5xlZ2fTpk0bZsyYcdHn582bR0xMDM8++yxbtmyhe/fuDBw4kKNHj9peEx0dTWRk5AWPlJQU22tOnTrFnXfeyezZs8t8TFfiav8crBe5Xu+MO9ZfjCP+DYDz/x1fzNWO/ZtvvqFJkyY0adKkPGM7xNWOvaCggDVr1jBz5kzWr1/PihUrWLFiRXkO4Ypd7djPnDnDkiVLOHz4MMnJycTFxbF69eryHEKpOeq/77+qCJ95ZTX2P1zVZ57jrrSJvQDrwoULSxzr0KGD9cEHHyxxrFmzZtZnnnmm1O+bm5tr7d69u3Xu3LmOiFnmruTPYd26ddahQ4fanhs/frz1s88+K/Osjnal/wYq2t/xxVzJ2J955hlrvXr1rPXr17fWqFHD6u/vb33hhRfKK7LDXMnY4+LirAMGDLA9N3XqVOvUqVPLPKujXcnYv/rqK+u4ceNsz02dOtX6yiuvlHnWq3U1n/E//fRTiXkrFe0zz5Fjt1qv/jNPZ3acSH5+Pps3b6Z///4ljvfv35+4uLhSvYfVauWuu+7i2muv5Y477iiLmGWuNH8OHTp0YMeOHSQnJ5OVlcXSpUsZMGCAEXEdqjRjd4W/44spzdhjY2NJTEzk8OHDvPrqq9x///08//zzRsR1qNKMvX379hw/fpwzZ85gsVhYvXo1zZs3NyKuQ5Vm7KGhocTFxZGbm0tRURE///wzTZs2NSLuVbmaz/iK/pl3NWN3xGde5ZndVgGcPHmSoqIigoKCShwPCgoiNTW1VO+xbt065s2bR+vWrW3XSz/55BNatWrl6LhlpjR/Du7u7rz22mv07t0bi8XCU089RY0aNYyI61ClGbsr/B1fjCP+/VdUpf03P2XKFHr06IHVaqV///7ccMMNRsR1qNKMvVOnTlx//fVERUVhNpvp06cPQ4YMMSLuVSntv/EBAwYQHx9PdnY29erVY+HChbRv375Cf+Zdzdjz8vKu+jNPZccJ/fU6rNVqLfW12W7dumGxWMoiVrm73J/DkCFDKuQHXmn83dhd6e/4Ykr77/+uu+4qp0Tl53JjHzhwIAMHDizvWOXicmN/6aWXeOmll8o7Vpm43FgvdZeVK3zmXenYr/YzT5exnEjNmjVxc3O74P9i09LSLmjDrqwy/zlo7Br7+TR21xp7ZRrrXxk9dpUdJ+Lp6Ul0dPQFd1isWLGCLl26GJSq/FXmPweNXWM/n8buWmOvTGP9K6PHrstY5ezs2bMcOHDA9nVCQgJbt24lMDCQsLAwJkyYwB133EG7du3o3Lkzs2fP5ujRozz44IMGpna8yvznoLFr7KCxu+rYK9NY/8qpx35F93DJFfvpp5+swAWPMWPG2F4zc+ZMa/369a2enp7Wtm3bWletWmVc4DJSmf8cNHaNXWN33bFXprH+lTOP3WS1uvBuaiIiIlLpac6OiIiIuDSVHREREXFpKjsiIiLi0lR2RERExKWp7IiIiIhLU9kRERERl6ayIyIiIi5NZUdERERcmsqOiIiIuDSVHREREXFpKjsi4lJmzpxJgwYNcHd358knnzQ6jog4Ae2NJSIuY8eOHURFRbFo0SLatm1LQEAAPj4+RscSEYO5Gx1ARMRRFi9eTHR0NIMGDTI6iog4EZUdEXEJjRo14tChQwCYTCZGjx7NJ598YnAqEXEGuowlIi4hLS2Nzp0789BDDzF69Gh8fX3x8/MzOpaIOAFNUBYRl1C1alUOHz5Mt27dCA4O5tSpU1x33XU0adKEJk2a8MYbbxgdUUQMorIjIi5h+/btALRq1Qqr1cpNN93EPffcw759+9i0aRPz5s1jwYIFBqcUESOo7IiIS9i6dSuNGzfG19eXlStXUq1aNUaMGAGAv78/sbGxvPbaawanFBEjqOyIiEvYunUrbdq0AWDXrl1ERUWVeD4qKordu3cbEU1EDKayIyIuYevWrVxzzTWXfN5kMpVfGBFxKio7IlLhWSwWfvvtN9uZnebNmxMfH1/iNfHx8TRv3hyAH374gfvuu4+hQ4fy448/lnteESlfuvVcRFyOxWIhKiqKZ599lhEjRpCZmcmAAQOYOHEiw4cPt70uPT2df/zjH7z99tsGphWRsqayIyIu6dChQzz44IMcPnwYq9XKQw89xIQJE0q85umnn+bWW2+lbdu2BqUUkfKgsiMildLkyZPp0qUL/fv3NzqKiJQxbRchIpXOZ599xsKFC0lNTeXw4cM88MADRkcSkTKkMzsiIiLi0nQ3loiIiLg0lR0RERFxaSo7IiIi4tJUdkRERMSlqeyIiIiIS1PZEREREZemsiMiIiIuTWVHREREXJrKjoiIiLg0lR0RERFxaSo7IiIi4tJUdkRERMSl/T+0HrnY2zeorwAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 640x480 with 1 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# we just need to vectorize fO2_buffers_H22 so that it can read p and T as vector rather than scalars\n",
    "import numpy as np\n",
    "fO2_buffers_H22_vec = np.vectorize(fO2_buffers_H22)\n",
    "\n",
    "MO.setParametrization('fO2_profile',\n",
    "                      lambda var:10**fO2_buffers_H22_vec(var['pressures'],var['temperatures']),\n",
    "                      ['pressures','temperatures'], # The pressures and temperature profiles are simply called... \"pressures\" and \"temperatures\"\n",
    "                      is_profile=True)              # This time it is a profile.\n",
    "\n",
    "# The value is store in MO.profiles (for profile parametrizations):\n",
    "import matplotlib.pyplot as plt\n",
    "plt.semilogx(MO.profiles['fO2_profile'],MO.profiles['pressures']*1e-9)\n",
    "plt.ylim(135,0)\n",
    "plt.xlabel(r'$f_{{\\rm O}_2}$')\n",
    "plt.ylabel('pressure [GPa]')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6a0ca8a9-fbfb-493d-a0cb-4a067f76f76a",
   "metadata": {},
   "source": [
    "The small jump around 75 GPa corresponds to the transition between coefficients calibrated for fcc and hcp iron.    \n",
    "Notice that for profile parametrizations, we also have access to the (arithmetic) average under MO.averages:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "1b725f99-c708-4d48-9f6f-ae5d391909a9",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "22484932081.460506\n"
     ]
    }
   ],
   "source": [
    "print(MO.averages['fO2_profile'])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b463a6c9-5f1f-4e7f-b4df-18c152075c14",
   "metadata": {},
   "source": [
    "Of course, for the $F_{{\\rm O}_2}$, it is not very insightful, but for other things, it might be useful!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ff65fb3d-d289-482c-bba7-8139422c7928",
   "metadata": {},
   "source": [
    "## The MO address book\n",
    "As \"T_pot\" for $T_{\\rm pot}$, p_sfc for $p_{\\rm sfc}$, \"pressures\" for th epressure profile and \"temperatures\" for the temperature profile, the MO has a list of built-in values that can be accessed by parametrizations. Every time you set up a new parametrization, it gets added to this list, and can be accessed by the name you gave it. For profile parametrizations, you can even call their average or surface value only, if that's what you need!     \n",
    "Remember that we added the $f_{{\\rm O}_2}$ because we wanted to calculate the effective equilibrium constant $K_H\\sqrt{f_{{\\rm O}_2}}$, for the redox reaction H$_2$+1/2O$_2$=H$_2$O. To do that, we added another parametrization, which called $T_{\\rm pot}$, but also $f_{{\\rm O}_2,{\\rm sfc}}$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "16028d8f-bc4b-478f-8137-374abe9e18f1",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "25.46519933449377\n"
     ]
    }
   ],
   "source": [
    "from chemistry.equilibria import eq_H\n",
    "from chemistry.redox import equilibrium_constant_NATAF\n",
    "def set_K_H(T,fO2):\n",
    "    eq_H.constant = equilibrium_constant_NATAF[eq_H.expression](T) * np.sqrt(fO2)\n",
    "    return eq_H.constant\n",
    "MO.setParametrization('K_H',lambda var:set_K_H(var['T_pot'],var['fO2_surface']),['T_pot','fO2_surface'],is_profile=False) # here we call fO2_surface, but we could also use fO2_profile_sfc\n",
    "                                                                                                                          # since we have two different parametrizations for fO2\n",
    "print(MO.averages['K_H'])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4beea112-78ed-4674-8fc0-050afbb61918",
   "metadata": {},
   "source": [
    "## Parametrizations channels"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7f3b2085-20f4-40c4-a7af-52f4503a6187",
   "metadata": {},
   "source": [
    "You may want to have some control on when, during the update of the internal state of the MO, a parametrization is going to be updated. For example, if one of your parametrization depends on the partial pressures in an atmosphere where speciation is solved, like in the previous chapter, you want the update to be made after the speciation has been solved for, while the effective equilibrium constant needs to be updated before. This can be done by specifying th eparametrization channel, which decides when the parametrization will be updated. There are 7 built-in parametrization channels:   \n",
    "\n",
    " 1. eos: for the equatoins of state (in general only for density, thermal expansivity and heat capacity, used to calculate the adiabat)\n",
    " 2. pre-update: parametrizations in this channel are updated before the internal state is updated (i.e. before even the temperature is updated)\n",
    " 3. pre-frac: parametrizations in this channel are updated just before calling the fractionation function\n",
    " 4. pre-spec: parametrizations in this channel are updated just before calling the sepeciation function\n",
    " 5. post-spec: parametrizations in this channel are updated just after calling the sepeciation function\n",
    " 6. time-dep: parametrizations in this channel are updated during RK4_step, and are integrated to the RK4 time-stepping\n",
    " 7. internal heating: parametrizations in this channel are updated when calculating the heat-conservation equation      \n",
    " \n",
    "The default parametrization channel is \"pre-spec\". Another parametrization channel can be specified using the keyword \"param_channel\" in the setParametrization method."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
