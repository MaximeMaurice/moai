��do      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]�(h �highlightlang���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��lang��none��force���linenothreshold���������u�tagname�h	�parent�h�	_document�h�source��I/home/maxime/Models/MO_Outgassing/docs/source/notebooks/My_first_MO.ipynb��line�Kub�docutils.nodes��comment���)��}�(hhh]�h}�(h]�h]�h]�h]�h]��	xml:space��preserve�uhh%hhh hh!h"h#Kubh$�section���)��}�(hhh]�(h$�title���)��}�(h�My first magma ocean�h]�h$�Text����My first magma ocean�����}�(hh9h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhh7hh4h hh!h"h#K	ubh$�	paragraph���)��}�(h�BLet’s get started with MOAI and implement our first magma ocean!�h]�h>�BLet’s get started with MOAI and implement our first magma ocean!�����}�(hhKh hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhhIh!h"h#Khh4h hubh&)��}�(hhh]�h}�(h]�h]�h]�h]�h]�h0h1uhh%hh4h hh!h"h#Kubh3)��}�(hhh]�(h8)��}�(h�Create a magma ocean�h]�h>�Create a magma ocean�����}�(hheh hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhh7hhbh hh!h"h#KubhJ)��}�(h��We need to specify a few parameters when creating a magma ocean object: - the potential temperature - the CMB pressure - the equations of state�h]�h>��We need to specify a few parameters when creating a magma ocean object: - the potential temperature - the CMB pressure - the equations of state�����}�(hhsh hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhhIh!h"h#Khhbh hubhJ)��}�(hX�  An there are a few other optional parameters (like the planetary radius and gravity) than can also be passed as keyword arguments. The native radial coordinate is pressure. The magma ocean has a built-in solver for lithostatic equilibrium, that calculates the radii/depths based on the pressure and the equations of state. While this is done during the initialization of the MO object, it is not automatically called every time we update it subsequently. For the sake of simplicity, we consider that
the pressure/depth relationship does not evolve (which is not the case if the equations of state are temperature-dependent). If you really want, you can enforce it, but it needs a bit of tweaking MOAI and is for more advanced users.�h]�h>X�  An there are a few other optional parameters (like the planetary radius and gravity) than can also be passed as keyword arguments. The native radial coordinate is pressure. The magma ocean has a built-in solver for lithostatic equilibrium, that calculates the radii/depths based on the pressure and the equations of state. While this is done during the initialization of the MO object, it is not automatically called every time we update it subsequently. For the sake of simplicity, we consider that
the pressure/depth relationship does not evolve (which is not the case if the equations of state are temperature-dependent). If you really want, you can enforce it, but it needs a bit of tweaking MOAI and is for more advanced users.�����}�(hh�h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhhIh!h"h#Khhbh hubh&)��}�(hhh]�h}�(h]�h]�h]�h]�h]�h0h1uhh%hhbh hh!h"h#K"ubh$�	container���)��}�(hhh]�(h �only���)��}�(hhh]�h$�literal_block���)��}�(h�[3]:�h]�h>�[3]:�����}�hh�sbah}�(h]�h]��prompt�ah]�h]�h]��language��none�h0h1uhh�hh�h!h"ubah}�(h]�h]�h]�h]�h]��expr��html�uhh�hh�ub�nbsphinx��CodeAreaNode���)��}�(hhh]�h�)��}�(hX.  from magma_ocean import magma_ocean
from physics.constants import Earth_gravity,Earth_surface_radius

# define MO potential temperature and mantle depth
T_pot = 2500. # [K]
p_CMB = 135e9 # [Pa]

# create "constant" equations of state
rho   = lambda p,T: 3500. # [kg/m³]
alpha = lambda p,T: 3e-5  # [K⁻¹]
cp    = lambda p,T: 1200. # [J/K/kg]

# create magma ocean object
basic_MO = magma_ocean(T_pot,
                       p_CMB,
                       eos={'rho':rho,'alpha':alpha,'cp':cp},
                       g=Earth_gravity,
                       R=Earth_surface_radius,
                       ConvCum=True)

# plot
import matplotlib.pyplot as plt
plt.plot(basic_MO.profiles['temperatures'],basic_MO.profiles['pressures']*1e-9)
plt.ylim(135,0)
plt.xlabel('temperature [K]')
plt.ylabel('pressure [GPa]')�h]�h>X.  from magma_ocean import magma_ocean
from physics.constants import Earth_gravity,Earth_surface_radius

# define MO potential temperature and mantle depth
T_pot = 2500. # [K]
p_CMB = 135e9 # [Pa]

# create "constant" equations of state
rho   = lambda p,T: 3500. # [kg/m³]
alpha = lambda p,T: 3e-5  # [K⁻¹]
cp    = lambda p,T: 1200. # [J/K/kg]

# create magma ocean object
basic_MO = magma_ocean(T_pot,
                       p_CMB,
                       eos={'rho':rho,'alpha':alpha,'cp':cp},
                       g=Earth_gravity,
                       R=Earth_surface_radius,
                       ConvCum=True)

# plot
import matplotlib.pyplot as plt
plt.plot(basic_MO.profiles['temperatures'],basic_MO.profiles['pressures']*1e-9)
plt.ylim(135,0)
plt.xlabel('temperature [K]')
plt.ylabel('pressure [GPa]')�����}�hh�sbah}�(h]�h]��
input_area�ah]�h]�h]��language��ipython3�h0h1uhh�hh�h!h"ubah}�(h]�h]�h]�h]�h]��prompt�h��stderr��uhh�hh�ubeh}�(h]�h]��nbinput�ah]�h]�h]�uhh�hhbh hh!h"h#Nubh&)��}�(hhh]�h}�(h]�h]�h]�h]�h]�h0h1uhh%hhbh hh!h"h#KDubh�)��}�(hhh]�(h�)��}�(hhh]�h�)��}�(h�[3]:�h]�h>�[3]:�����}�hh�sbah}�(h]�h]�h�ah]�h]�h]��language�h�h0h1uhh�hh�h!h"ubah}�(h]�h]�h]�h]�h]��expr�h�uhh�hh�ubh�)��}�(hhh]�h�)��}�(hhh]�(h$�raw���)��}�(h�3<pre>
Text(0, 0.5, &#39;pressure [GPa]&#39;)
</pre>�h]�h>�3<pre>
Text(0, 0.5, &#39;pressure [GPa]&#39;)
</pre>�����}�hj  sbah}�(h]�h]��	highlight�ah]�h]�h]��format��html�h0h1uhj  h!h"h#KLhj  ubj  )��}�(h�_\begin{sphinxVerbatim}[commandchars=\\\{\}]
Text(0, 0.5, 'pressure [GPa]')
\end{sphinxVerbatim}�h]�h>�_\begin{sphinxVerbatim}[commandchars=\\\{\}]
Text(0, 0.5, 'pressure [GPa]')
\end{sphinxVerbatim}�����}�hj!  sbah}�(h]�h]�h]�h]�h]��format��latex�h0h1uhj  h!h"h#KRhj  ubj  )��}�(h�Text(0, 0.5, 'pressure [GPa]')�h]�h>�Text(0, 0.5, 'pressure [GPa]')�����}�hj1  sbah}�(h]�h]�h]�h]�h]��format��text�h0h1uhj  h!h"h#KYhj  ubeh}�(h]�h]�(�output_area�heh]�h]�h]�uhh�hj  ubah}�(h]�h]�h]�h]�h]��prompt�h�stderr��uhh�hh�ubeh}�(h]�h]��nboutput�ah]�h]�h]�uhh�hhbh hh!Nh#Nubh&)��}�(hhh]�h}�(h]�h]�h]�h]�h]�h0h1uhh%hhbh hh!h"h#K^ubh�)��}�(hhh]�(h�)��}�(hhh]�h�)��}�(hhh]�h}�(h]�h]�(h��empty�eh]�h]�h]�uhh�hjc  ubah}�(h]�h]�h]�h]�h]��expr�h�uhh�hj`  ubh��FancyOutputNode���)��}�(hhh]�h�)��}�(hhh]�h$�image���)��}�(h�F.. image:: ../../build/doctrees/nbsphinx/notebooks_My_first_MO_2_1.png�h]�h}�(h]�h]�h]�h]�h]��uri��8../build/doctrees/nbsphinx/notebooks_My_first_MO_2_1.png��
candidates�}��*�j�  suhj  hj|  h!h"h#K ubah}�(h]�h]�(jD  heh]�h]�h]�uhh�hjy  ubah}�(h]�h]�h]�h]�h]��prompt�huhjw  hj`  ubeh}�(h]�h]�(jS  �nblast�eh]�h]�h]�uhh�hhbh hh!Nh#Nubh&)��}�(hhh]�h}�(h]�h]�h]�h]�h]�h0h1uhh%hhbh hh!h"h#Kgubh$�
line_block���)��}�(hhh]�h$h#��)��}�(h�nWe just plotted the temperature profile through our magma ocean. It follows an adiabat, given by the equation:�h]�h>�nWe just plotted the temperature profile through our magma ocean. It follows an adiabat, given by the equation:�����}�(hj�  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhh#�indent�K hj�  h hh!h"h#Klubah}�(h]�h]�h]�h]�h]�uhj�  hhbh hh!h"h#Klubh$�block_quote���)��}�(hX�  .. math:: \frac{dT}{dp}=\frac{\alpha T}{\rho c_p}

where :math:`T` is the tempature, :math:`p` the pressure, :math:`\alpha` the thermal expansivity, :math:`\rho` the density and :math:`c_p` the heat capacity. This equation is integrated given the boundary condition :math:`T(p=p_{\rm sfc})=T_{\rm pot}`. In general we can consider :math:`p_{\rm sfc}=0` Pa, since any variation below :math:`\sim100` MPa will not affect the adiabt significantly.�h]�(h$�
math_block���)��}�(h�)\frac{dT}{dp}=\frac{\alpha T}{\rho c_p}

�h]�h>�)\frac{dT}{dp}=\frac{\alpha T}{\rho c_p}

�����}�hj�  sbah}�(h]�h]�h]�h]�h]��docname��notebooks/My_first_MO��number�N�label�N�nowrap��h0h1uhj�  h!h"h#Knhj�  ubhJ)��}�(hX�  where :math:`T` is the tempature, :math:`p` the pressure, :math:`\alpha` the thermal expansivity, :math:`\rho` the density and :math:`c_p` the heat capacity. This equation is integrated given the boundary condition :math:`T(p=p_{\rm sfc})=T_{\rm pot}`. In general we can consider :math:`p_{\rm sfc}=0` Pa, since any variation below :math:`\sim100` MPa will not affect the adiabt significantly.�h]�(h>�where �����}�(hj�  h hh!Nh#Nubh$�math���)��}�(h�	:math:`T`�h]�h>�T�����}�(hj�  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh>� is the tempature, �����}�(hj�  h hh!Nh#Nubj�  )��}�(h�	:math:`p`�h]�h>�p�����}�(hj�  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh>� the pressure, �����}�(hj�  h hh!Nh#Nubj�  )��}�(h�:math:`\alpha`�h]�h>�\alpha�����}�(hj  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh>� the thermal expansivity, �����}�(hj�  h hh!Nh#Nubj�  )��}�(h�:math:`\rho`�h]�h>�\rho�����}�(hj#  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh>� the density and �����}�(hj�  h hh!Nh#Nubj�  )��}�(h�:math:`c_p`�h]�h>�c_p�����}�(hj5  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh>�M the heat capacity. This equation is integrated given the boundary condition �����}�(hj�  h hh!Nh#Nubj�  )��}�(h�$:math:`T(p=p_{\rm sfc})=T_{\rm pot}`�h]�h>�T(p=p_{\rm sfc})=T_{\rm pot}�����}�(hjG  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh>�. In general we can consider �����}�(hj�  h hh!Nh#Nubj�  )��}�(h�:math:`p_{\rm sfc}=0`�h]�h>�p_{\rm sfc}=0�����}�(hjY  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh>� Pa, since any variation below �����}�(hj�  h hh!Nh#Nubj�  )��}�(h�:math:`\sim100`�h]�h>�\sim100�����}�(hjk  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh>�. MPa will not affect the adiabt significantly.�����}�(hj�  h hh!Nh#Nubeh}�(h]�h]�h]�h]�h]�uhhIh!h"h#Kphj�  ubeh}�(h]�h]�h]�h]�h]�uhj�  h!h"h#Knhhbh hubj�  )��}�(hhh]�j�  )��}�(hX  Thats great, but the adiabatic temperature increase is actually a bit too high. And the curvature is strange as well… This is because we have used way too simple equations of state (we have simply considered :math:`\alpha`, :math:`\rho` and :math:`c_p` constant!�h]�(h>��Thats great, but the adiabatic temperature increase is actually a bit too high. And the curvature is strange as well… This is because we have used way too simple equations of state (we have simply considered �����}�(hj�  h hh!Nh#Nubj�  )��}�(h�:math:`\alpha`�h]�h>�\alpha�����}�(hj�  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh>�, �����}�(hj�  h hh!Nh#Nubj�  )��}�(h�:math:`\rho`�h]�h>�\rho�����}�(hj�  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh>� and �����}�(hj�  h hh!Nh#Nubj�  )��}�(h�:math:`c_p`�h]�h>�c_p�����}�(hj�  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh>�
 constant!�����}�(hj�  h hh!Nh#Nubeh}�(h]�h]�h]�h]�h]�uhh#j�  K hj�  h hh!h"h#Kqubah}�(h]�h]�h]�h]�h]�uhj�  hhbh hh!h"h#Kqubh&)��}�(hhh]�h}�(h]�h]�h]�h]�h]�h0h1uhh%hhbh hh!h"h#Kwubeh}�(h]��Create-a-magma-ocean�ah]�h]��create a magma ocean�ah]�h]�uhh2hh4h hh!h"h#Kubh3)��}�(hhh]�(h8)��}�(h�Use better equations of state�h]�h>�Use better equations of state�����}�(hj�  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhh7hj�  h hh!h"h#K}ubhJ)��}�(h��For silicate melt, we can use the following expression, derived by Abe 1997, from a Murnaghan equation of state and a :math:`T`-independent Grüneisen parameter�h]�(h>�vFor silicate melt, we can use the following expression, derived by Abe 1997, from a Murnaghan equation of state and a �����}�(hj�  h hh!Nh#Nubj�  )��}�(h�	:math:`T`�h]�h>�T�����}�(hj   h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh>�!-independent Grüneisen parameter�����}�(hj�  h hh!Nh#Nubeh}�(h]�h]�h]�h]�h]�uhhIh!h"h#Khj�  h hubj�  )��}�(h�F\alpha(p)=\alpha_0\left(\frac{K'}{K_0}-1\right)^{-\frac{m-1+K'}{K'}}

�h]�h>�F\alpha(p)=\alpha_0\left(\frac{K'}{K_0}-1\right)^{-\frac{m-1+K'}{K'}}

�����}�hj  sbah}�(h]�h]�h]�h]�h]��docname�j�  �number�N�label�N�nowrap��h0h1uhj�  h!h"h#K�hj�  h hubhJ)��}�(hXN  For density, a simple linear function of pressure, fitted from Myiazaki and Korenaga 2019 will do. Finally, :math:`c_p` can stay constant. All these functions are already included in MOAI, in the eos subpackage. If you are really picky about equations of state, you can check he chapter on how to use burnman to have much better ones.�h]�(h>�lFor density, a simple linear function of pressure, fitted from Myiazaki and Korenaga 2019 will do. Finally, �����}�(hj*  h hh!Nh#Nubj�  )��}�(h�:math:`c_p`�h]�h>�c_p�����}�(hj2  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhj�  hj*  ubh>�� can stay constant. All these functions are already included in MOAI, in the eos subpackage. If you are really picky about equations of state, you can check he chapter on how to use burnman to have much better ones.�����}�(hj*  h hh!Nh#Nubeh}�(h]�h]�h]�h]�h]�uhhIh!h"h#K�hj�  h hubh&)��}�(hhh]�h}�(h]�h]�h]�h]�h]�h0h1uhh%hj�  h hh!h"h#K�ubh�)��}�(hhh]�(h�)��}�(hhh]�h�)��}�(h�[5]:�h]�h>�[5]:�����}�hjY  sbah}�(h]�h]�h�ah]�h]�h]��language�h�h0h1uhh�hjV  h!h"ubah}�(h]�h]�h]�h]�h]��expr�h�uhh�hjS  ubh�)��}�(hhh]�h�)��}�(hX�  from physics.eos import *
better_MO = magma_ocean(T_pot,
                        p_CMB,
                        eos={'rho':eos_book['rho']['MK19l'],'alpha':eos_book['alpha']['N19'],'cp':cp},
                        g=Earth_gravity,
                        R=Earth_surface_radius,
                        ConvCum=True)
# plot
plt.plot(better_MO.profiles['temperatures'],better_MO.profiles['pressures']*1e-9)
plt.ylim(135,0)
plt.xlabel('temperature [K]')
plt.ylabel('pressure [GPa]')�h]�h>X�  from physics.eos import *
better_MO = magma_ocean(T_pot,
                        p_CMB,
                        eos={'rho':eos_book['rho']['MK19l'],'alpha':eos_book['alpha']['N19'],'cp':cp},
                        g=Earth_gravity,
                        R=Earth_surface_radius,
                        ConvCum=True)
# plot
plt.plot(better_MO.profiles['temperatures'],better_MO.profiles['pressures']*1e-9)
plt.ylim(135,0)
plt.xlabel('temperature [K]')
plt.ylabel('pressure [GPa]')�����}�hjr  sbah}�(h]�h]�h�ah]�h]�h]��language��ipython3�h0h1uhh�hjo  h!h"ubah}�(h]�h]�h]�h]�h]��prompt�j[  �stderr��uhh�hjS  ubeh}�(h]�h]�h�ah]�h]�h]�uhh�hj�  h hh!h"h#Nubh&)��}�(hhh]�h}�(h]�h]�h]�h]�h]�h0h1uhh%hj�  h hh!h"h#K�ubh�)��}�(hhh]�(h�)��}�(hhh]�h�)��}�(h�[5]:�h]�h>�[5]:�����}�hj�  sbah}�(h]�h]�h�ah]�h]�h]��language�h�h0h1uhh�hj�  h!h"ubah}�(h]�h]�h]�h]�h]��expr�h�uhh�hj�  ubh�)��}�(hhh]�h�)��}�(hhh]�(j  )��}�(h�3<pre>
Text(0, 0.5, &#39;pressure [GPa]&#39;)
</pre>�h]�h>�3<pre>
Text(0, 0.5, &#39;pressure [GPa]&#39;)
</pre>�����}�hj�  sbah}�(h]�h]��	highlight�ah]�h]�h]��format��html�h0h1uhj  h!h"h#K�hj�  ubj  )��}�(h�_\begin{sphinxVerbatim}[commandchars=\\\{\}]
Text(0, 0.5, 'pressure [GPa]')
\end{sphinxVerbatim}�h]�h>�_\begin{sphinxVerbatim}[commandchars=\\\{\}]
Text(0, 0.5, 'pressure [GPa]')
\end{sphinxVerbatim}�����}�hj�  sbah}�(h]�h]�h]�h]�h]��format��latex�h0h1uhj  h!h"h#K�hj�  ubj  )��}�(h�Text(0, 0.5, 'pressure [GPa]')�h]�h>�Text(0, 0.5, 'pressure [GPa]')�����}�hj�  sbah}�(h]�h]�h]�h]�h]��format��text�h0h1uhj  h!h"h#K�hj�  ubeh}�(h]�h]�(jD  heh]�h]�h]�uhh�hj�  ubah}�(h]�h]�h]�h]�h]��prompt�j�  �stderr��uhh�hj�  ubeh}�(h]�h]�jS  ah]�h]�h]�uhh�hj�  h hh!Nh#Nubh&)��}�(hhh]�h}�(h]�h]�h]�h]�h]�h0h1uhh%hj�  h hh!h"h#K�ubh�)��}�(hhh]�(h�)��}�(hhh]�h�)��}�(hhh]�h}�(h]�h]�(h�jl  eh]�h]�h]�uhh�hj  ubah}�(h]�h]�h]�h]�h]��expr�h�uhh�hj	  ubjx  )��}�(hhh]�h�)��}�(hhh]�j�  )��}�(h�F.. image:: ../../build/doctrees/nbsphinx/notebooks_My_first_MO_5_1.png�h]�h}�(h]�h]�h]�h]�h]��uri��8../build/doctrees/nbsphinx/notebooks_My_first_MO_5_1.png�j�  }�j�  j0  suhj  hj"  h!h"h#K ubah}�(h]�h]�(jD  heh]�h]�h]�uhh�hj  ubah}�(h]�h]�h]�h]�h]��prompt�huhjw  hj	  ubeh}�(h]�h]�(jS  j�  eh]�h]�h]�uhh�hj�  h hh!Nh#Nubh&)��}�(hhh]�h}�(h]�h]�h]�h]�h]�h0h1uhh%hj�  h hh!h"h#K�ubhJ)��}�(h��That’s a much nicer adiabat! But actually, we don’t really expect silicate at 4000 K and 120 GPa to be molten. Let’s now gear our magma ocean with melting curves:�h]�h>��That’s a much nicer adiabat! But actually, we don’t really expect silicate at 4000 K and 120 GPa to be molten. Let’s now gear our magma ocean with melting curves:�����}�(hjN  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhhIh!h"h#K�hj�  h hubh&)��}�(hhh]�h}�(h]�h]�h]�h]�h]�h0h1uhh%hj�  h hh!h"h#K�ubeh}�(h]��Use-better-equations-of-state�ah]�h]��use better equations of state�ah]�h]�uhh2hh4h hh!h"h#K}ubh3)��}�(hhh]�(h8)��}�(h�Add melting curves�h]�h>�Add melting curves�����}�(hjp  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhh7hjm  h hh!h"h#K�ubhJ)��}�(hX�  You can chose your favorite set of melting curves from the catalog in the phase_change.refractories subpackage, or implement your own (using the melting_curves class). When adding melting curves to a magma ocean, you also need to give the RCMF (rheologically critical melt fraction), which is the melt fraction at below which the magma ocean becomes the solid cumulates. By assumption, the melt fraction (:math:`\phi`) evolves linearly with temperature between the liquids (where :math:`\phi=1`) and
the solidus (where :math:`\phi=0`). The bottom of the magma ocean will be somewhere in between, where :math:`\phi=\phi_{\rm RCMF}`. Here we consider :math:`\phi_{\rm RCMF}=0.4`�h]�(h>X�  You can chose your favorite set of melting curves from the catalog in the phase_change.refractories subpackage, or implement your own (using the melting_curves class). When adding melting curves to a magma ocean, you also need to give the RCMF (rheologically critical melt fraction), which is the melt fraction at below which the magma ocean becomes the solid cumulates. By assumption, the melt fraction (�����}�(hj~  h hh!Nh#Nubj�  )��}�(h�:math:`\phi`�h]�h>�\phi�����}�(hj�  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhj�  hj~  ubh>�?) evolves linearly with temperature between the liquids (where �����}�(hj~  h hh!Nh#Nubj�  )��}�(h�:math:`\phi=1`�h]�h>�\phi=1�����}�(hj�  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhj�  hj~  ubh>�) and
the solidus (where �����}�(hj~  h hh!Nh#Nubj�  )��}�(h�:math:`\phi=0`�h]�h>�\phi=0�����}�(hj�  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhj�  hj~  ubh>�E). The bottom of the magma ocean will be somewhere in between, where �����}�(hj~  h hh!Nh#Nubj�  )��}�(h�:math:`\phi=\phi_{\rm RCMF}`�h]�h>�\phi=\phi_{\rm RCMF}�����}�(hj�  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhj�  hj~  ubh>�. Here we consider �����}�(hj~  h hh!Nh#Nubj�  )��}�(h�:math:`\phi_{\rm RCMF}=0.4`�h]�h>�\phi_{\rm RCMF}=0.4�����}�(hj�  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhj�  hj~  ubeh}�(h]�h]�h]�h]�h]�uhhIh!h"h#K�hjm  h hubh&)��}�(hhh]�h}�(h]�h]�h]�h]�h]�h0h1uhh%hjm  h hh!h"h#K�ubh�)��}�(hhh]�(h�)��}�(hhh]�h�)��}�(h�[17]:�h]�h>�[17]:�����}�hj�  sbah}�(h]�h]�h�ah]�h]�h]��language�h�h0h1uhh�hj�  h!h"ubah}�(h]�h]�h]�h]�h]��expr�h�uhh�hj�  ubh�)��}�(hhh]�h�)��}�(hX"  from physics.phase_change.refractories import mc_book
mc = mc_book['Earth'] # melting curves from Fiquet et al., 2010
better_MO.setMeltingCurves(mc,RCMF=0.4)

# adiabat
plt.plot(better_MO.profiles['temperatures'],better_MO.profiles['pressures']*1e-9,label='adiabat')

# melting curves
plt.plot(mc.liquidus,mc.p_lookup*1e-9,label='liquidus')
plt.plot(mc.solidus,mc.p_lookup*1e-9,label='solidus')
plt.plot(mc.solidus*(1-better_MO.mc.RCMF)+better_MO.mc.RCMF*mc.liquidus,mc.p_lookup*1e-9,label='RCMF')

# MO bottom
plt.scatter([better_MO.adiabat.getT(better_MO.p_bot)],[better_MO.p_bot*1e-9])

plt.legend(loc=7)
plt.ylim(135,0)
plt.xlabel('temperature [K]')
plt.ylabel('pressure [GPa]')

plt.twiny()
plt.plot(better_MO.profiles['phi'],better_MO.profiles['pressures']*1e-9,'k--')
plt.xlabel('melt fraction')�h]�h>X"  from physics.phase_change.refractories import mc_book
mc = mc_book['Earth'] # melting curves from Fiquet et al., 2010
better_MO.setMeltingCurves(mc,RCMF=0.4)

# adiabat
plt.plot(better_MO.profiles['temperatures'],better_MO.profiles['pressures']*1e-9,label='adiabat')

# melting curves
plt.plot(mc.liquidus,mc.p_lookup*1e-9,label='liquidus')
plt.plot(mc.solidus,mc.p_lookup*1e-9,label='solidus')
plt.plot(mc.solidus*(1-better_MO.mc.RCMF)+better_MO.mc.RCMF*mc.liquidus,mc.p_lookup*1e-9,label='RCMF')

# MO bottom
plt.scatter([better_MO.adiabat.getT(better_MO.p_bot)],[better_MO.p_bot*1e-9])

plt.legend(loc=7)
plt.ylim(135,0)
plt.xlabel('temperature [K]')
plt.ylabel('pressure [GPa]')

plt.twiny()
plt.plot(better_MO.profiles['phi'],better_MO.profiles['pressures']*1e-9,'k--')
plt.xlabel('melt fraction')�����}�hj
  sbah}�(h]�h]�h�ah]�h]�h]��language��ipython3�h0h1uhh�hj  h!h"ubah}�(h]�h]�h]�h]�h]��prompt�j�  �stderr��uhh�hj�  ubeh}�(h]�h]�h�ah]�h]�h]�uhh�hjm  h hh!h"h#Nubh&)��}�(hhh]�h}�(h]�h]�h]�h]�h]�h0h1uhh%hjm  h hh!h"h#K�ubh�)��}�(hhh]�(h�)��}�(hhh]�h�)��}�(hhh]�h}�(h]�h]�(h�jl  eh]�h]�h]�uhh�hj4  ubah}�(h]�h]�h]�h]�h]��expr�h�uhh�hj1  ubh�)��}�(hhh]�h�)��}�(hhh]�(j  )��}�(h�R<pre>
No file found: calculating lookups, be patient it can take some time!
</pre>�h]�h>�R<pre>
No file found: calculating lookups, be patient it can take some time!
</pre>�����}�hjM  sbah}�(h]�h]��	highlight�ah]�h]�h]��format��html�h0h1uhj  h!h"h#M hjJ  ubj  )��}�(h��\begin{sphinxVerbatim}[commandchars=\\\{\}]
No file found: calculating lookups, be patient it can take some time!
\end{sphinxVerbatim}�h]�h>��\begin{sphinxVerbatim}[commandchars=\\\{\}]
No file found: calculating lookups, be patient it can take some time!
\end{sphinxVerbatim}�����}�hj^  sbah}�(h]�h]�h]�h]�h]��format��latex�h0h1uhj  h!h"h#MhjJ  ubj  )��}�(h�ENo file found: calculating lookups, be patient it can take some time!�h]�h>�ENo file found: calculating lookups, be patient it can take some time!�����}�hjn  sbah}�(h]�h]�h]�h]�h]��format��text�h0h1uhj  h!h"h#MhjJ  ubeh}�(h]�h]�(jD  heh]�h]�h]�uhh�hjG  ubah}�(h]�h]�h]�h]�h]��prompt�h�stderr��uhh�hj1  ubeh}�(h]�h]�jS  ah]�h]�h]�uhh�hjm  h hh!Nh#Nubh&)��}�(hhh]�h}�(h]�h]�h]�h]�h]�h0h1uhh%hjm  h hh!h"h#Mubh�)��}�(hhh]�(h�)��}�(hhh]�h�)��}�(h�[17]:�h]�h>�[17]:�����}�hj�  sbah}�(h]�h]�h�ah]�h]�h]��language�h�h0h1uhh�hj�  h!h"ubah}�(h]�h]�h]�h]�h]��expr�h�uhh�hj�  ubh�)��}�(hhh]�h�)��}�(hhh]�(j  )��}�(h�2<pre>
Text(0.5, 0, &#39;melt fraction&#39;)
</pre>�h]�h>�2<pre>
Text(0.5, 0, &#39;melt fraction&#39;)
</pre>�����}�hj�  sbah}�(h]�h]��	highlight�ah]�h]�h]��format��html�h0h1uhj  h!h"h#Mhj�  ubj  )��}�(h�^\begin{sphinxVerbatim}[commandchars=\\\{\}]
Text(0.5, 0, 'melt fraction')
\end{sphinxVerbatim}�h]�h>�^\begin{sphinxVerbatim}[commandchars=\\\{\}]
Text(0.5, 0, 'melt fraction')
\end{sphinxVerbatim}�����}�hj�  sbah}�(h]�h]�h]�h]�h]��format��latex�h0h1uhj  h!h"h#M hj�  ubj  )��}�(h�Text(0.5, 0, 'melt fraction')�h]�h>�Text(0.5, 0, 'melt fraction')�����}�hj�  sbah}�(h]�h]�h]�h]�h]��format��text�h0h1uhj  h!h"h#M'hj�  ubeh}�(h]�h]�(jD  heh]�h]�h]�uhh�hj�  ubah}�(h]�h]�h]�h]�h]��prompt�j�  �stderr��uhh�hj�  ubeh}�(h]�h]�jS  ah]�h]�h]�uhh�hjm  h hh!Nh#Nubh&)��}�(hhh]�h}�(h]�h]�h]�h]�h]�h0h1uhh%hjm  h hh!h"h#M,ubh�)��}�(hhh]�(h�)��}�(hhh]�h�)��}�(hhh]�h}�(h]�h]�(h�jl  eh]�h]�h]�uhh�hj  ubah}�(h]�h]�h]�h]�h]��expr�h�uhh�hj  ubjx  )��}�(hhh]�h�)��}�(hhh]�j�  )��}�(h�F.. image:: ../../build/doctrees/nbsphinx/notebooks_My_first_MO_8_2.png�h]�h}�(h]�h]�h]�h]�h]��uri��8../build/doctrees/nbsphinx/notebooks_My_first_MO_8_2.png�j�  }�j�  j2  suhj  hj$  h!h"h#K ubah}�(h]�h]�(jD  heh]�h]�h]�uhh�hj!  ubah}�(h]�h]�h]�h]�h]��prompt�huhjw  hj  ubeh}�(h]�h]�(jS  j�  eh]�h]�h]�uhh�hjm  h hh!Nh#Nubh&)��}�(hhh]�h}�(h]�h]�h]�h]�h]�h0h1uhh%hjm  h hh!h"h#M5ubhJ)��}�(hX  The bottom of the magma ocean (materialized by the blue marker) lies at about 35 GPa, where the adiabat intercepts the RCMF. That’s also (by definition) where the melt fraction (black dashed line, read off the top :math:`x`-axis) reaches the RCMF value of 0.4.�h]�(h>��The bottom of the magma ocean (materialized by the blue marker) lies at about 35 GPa, where the adiabat intercepts the RCMF. That’s also (by definition) where the melt fraction (black dashed line, read off the top �����}�(hjP  h hh!Nh#Nubj�  )��}�(h�	:math:`x`�h]�h>�x�����}�(hjX  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhj�  hjP  ubh>�%-axis) reaches the RCMF value of 0.4.�����}�(hjP  h hh!Nh#Nubeh}�(h]�h]�h]�h]�h]�uhhIh!h"h#M:hjm  h hubh&)��}�(hhh]�h}�(h]�h]�h]�h]�h]�h0h1uhh%hjm  h hh!h"h#M@ubeh}�(h]��Add-melting-curves�ah]�h]��add melting curves�ah]�h]�uhh2hh4h hh!h"h#K�ubeh}�(h]��My-first-magma-ocean�ah]�h]��my first magma ocean�ah]�h]�uhh2hhh hh!h"h#K	ubeh}�(h]�h]�h]�h]�h]��source�h"�nbsphinx_include_css��uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h7N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h"�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�J ���pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j�  �my-first-magma-ocean�j�  �create-a-magma-ocean�jj  �use-better-equations-of-state�j~  �add-melting-curves�u�	nametypes�}�(j�  �j�  �jj  �j~  �uh}�(j�  h4j�  hbj�  j�  j�  jm  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]�h$�system_message���)��}�(hhh]�hJ)��}�(h�;Block quote ends without a blank line; unexpected unindent.�h]�h>�;Block quote ends without a blank line; unexpected unindent.�����}�(hj  h hh!Nh#Nubah}�(h]�h]�h]�h]�h]�uhhIhj  ubah}�(h]�h]�h]�h]�h]��level�K�type��WARNING��line�Kq�source�h"uhj  hhbh hh!h"h#Nuba�transform_messages�]��transformer�N�include_log�]��
decoration�Nh hub.